"""Test suite for grid and rate grid operations
"""
import os
import pytest
import numpy as np
from shapely import geometry
from openquake.baselib.node import Node
from openquake.hazardlib import nrml, mfd
from openquake.hazardlib.geo import Point, Polygon
from pshatest.seismicity_rate import Grid2D, RateGrid2D, convert_mfdist


DATA_DIR = os.path.join(os.path.dirname(__file__), "data")


class TestConvertMFDist:
    """Tests the conversions of the MFDs from nodes
    """

    def test_mfdist_incremental_mfd(self):
        input_mfd = Node(
            "incrementalMFD", {"binWidth": 1.0, "minMag": 5.05},
            nodes=[Node("occurRates", text=[1.0, 0.5, 0.2])])
        input_src = Node("source", nodes=[input_mfd])
        output_mfd = convert_mfdist(input_src)
        assert str(output_mfd) == "<EvenlyDiscretizedMFD>"
        assert output_mfd.min_mag == pytest.approx(5.05)
        assert output_mfd.bin_width == pytest.approx(1.0)
        np.testing.assert_array_almost_equal(np.array(output_mfd.occurrence_rates),
                                             np.array([1.0, 0.5, 0.2]))


class TestGrid2D:
    """Basic tests of the 2D grid function
    """

    def test_grid2d_instantiation(self):
        """Tests basic instantiation of a 2D grid
        """
        bbox = (30.0, 40.0, 32.0, 44.0)
        spcx = 0.5
        spcy = 1.0
        grid = Grid2D(bbox, spcx, spcy)
        assert str(grid) == "GRID:30.000/40.000/44.000/32.000 (0.500, 1.000)"
        assert grid.nx == 4
        assert grid.ny == 4
        exp_lons = np.array([30.0, 30.5, 31.0, 31.5, 32.0])
        exp_lats = np.array([40.0, 41.0, 42.0, 43.0, 44.0])
        np.testing.assert_array_almost_equal(grid.lons, exp_lons)
        np.testing.assert_array_almost_equal(grid.lats, exp_lats)
        gx, gy = np.meshgrid(exp_lons, exp_lats)
        np.testing.assert_array_almost_equal(grid.glons, gx)
        np.testing.assert_array_almost_equal(grid.glats, gy)
        assert len(grid.polygons) == (grid.nx * grid.ny)

    def test_grid2d_areas(self):
        """Tests the functions to calculate areas and total area
        """
        bbox = (30.0, 40.0, 32.0, 44.0)
        spcx = 0.5
        spcy = 1.0
        grid = Grid2D(bbox, spcx, spcy)
        exp_areas = np.array([
           [4440.43266075, 4425.41098957, 4410.04056418, 4394.32282718],
           [4371.12244032, 4356.10679702, 4340.7433445 , 4325.03358387],
           [4300.58528003, 4285.58653896, 4270.24118524, 4254.55077724],
           [4228.84284457, 4213.87197636, 4198.5559444 , 4182.89636299]
           ])
        np.testing.assert_array_almost_equal(grid.areas, exp_areas)
        assert grid.area == pytest.approx(68998.344117)


@pytest.fixture(scope="class")
def src_model1():
    src_model = nrml.read(os.path.join(DATA_DIR, "dummy_source_model1.xml"))
    return src_model


class TestRateGrid2D:

    def test_rate_grid_instantiation(self):
        """Tests the instantiation of the rate grid
        """
        bbox = (30.0, 40.0, 32.0, 42.0)
        spcx = 1.0
        spcy = 1.0
        mmin = 5.0
        mmax = 8.0
        dm = 1.0
        grid = RateGrid2D(bbox, spcx, spcy, mmin, mmax, dm)
        assert str(grid) ==\
            "RATE GRID:30.000/40.000/42.000/32.000/5.00/8.00 (1.000, 1.000, 1.00)"
        exp_lons = np.array([30.0, 31.0, 32.0])
        exp_lats = np.array([40.0, 41.0, 42.0])
        exp_mbins = np.array([5.0, 6.0, 7.0, 8.0])
        np.testing.assert_array_almost_equal(grid.lons, exp_lons)
        np.testing.assert_array_almost_equal(grid.lats, exp_lats)
        np.testing.assert_array_almost_equal(grid.mbins, exp_mbins)
        assert grid.rates.shape == (2, 2, 3)

    def test_rate_grid_add_area_source(self, src_model1):
        asrc = src_model1[0][0][0]
        bbox = (30.0, 40.0, 32.0, 42.0)
        spcx = 1.0
        spcy = 1.0
        mmin = 5.0
        mmax = 8.0
        dm = 1.0
        grid = RateGrid2D(bbox, spcx, spcy, mmin, mmax, dm)
        grid.add_area_source(asrc)
        assert grid.rates.sum() == pytest.approx(1.7)
        for i, val in enumerate([1.0, 0.5, 0.2]):
            assert grid.rates[:, :, i].sum() == pytest.approx(val)

    def test_rate_grid_add_simple_fault_source(self, src_model1):
        flt_src = src_model1[0][0][1]
        bbox = (30.0, 40.0, 32.0, 42.0)
        spcx = 1.0
        spcy = 1.0
        mmin = 5.0
        mmax = 8.0
        dm = 1.0
        grid = RateGrid2D(bbox, spcx, spcy, mmin, mmax, dm)
        grid.add_simple_fault_source(flt_src)
        assert grid.rates.sum() == pytest.approx(0.3)
        for i, val in enumerate([0.0, 0.2, 0.1]):
            assert grid.rates[:, :, i].sum() == pytest.approx(val)

