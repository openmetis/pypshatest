# pyPSHATest

(c) Graeme Weatherill, GFZ German Research Centre for Geosciences, Potsdam, Germany

Email: [graeme.weatherill@gfz-potsdam.de](graeme.weatherill@gfz-potsdam.de) 

## About the Project

The toolkit <b>pyPSHATest</b> is a set of Python tools for testing probabilistic seismic hazard models and their components against observed data

This toolkit is developed in the context of the Horizon 2020 METIS Project (Grant Agreement No. 945121)


## Getting started


### Installation

Use virtualenv

```
pyenv virtualenv haztools
pyenv activate haztools
pip install --upgrade pip
git clone https://github.com/gem/oq-engine.git --depth=1
pip install -r oq-engine/requirements ...
```

Follow instructions for OQ
Install extra dependencies


### Dependencies

```
openquake >= 3.15.0
RTree >= 1.0.0
geopandas >= 0.12.1
rasterio >= 1.3.0

```


## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.



## Contributors

Graeme Weatherill (GFZ Potsdam)


## License
The project is licensed with a GNU General Public Licence v3.0)

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
