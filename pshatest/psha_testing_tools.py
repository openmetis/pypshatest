"""
Tools for Testing PSHA
"""


import logging
from typing import List, Dict, Union, Tuple, Optional
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.stats import norm, binom
from pshatest.utils import best_subplot_dimensions

logging.basicConfig(level=logging.INFO)


def wilson_score_method(
        m: Union[np.ndarray, float],
        p: Union[np.ndarray, float],
        z: Union[np.ndarray, float]
        ) -> Union[np.ndarray, float]:
    """Returns the confidence interval of the Poisson distribution using
    the Wilson score method

    Args:
        m: no.of sites
        p: probability
        z: confidence interval
    """
    alpha = (1.0 - z) / 2.
    zb = np.array([norm.ppf(alpha), norm.ppf(1.0 - alpha)])

    ci = p + ((zb ** 2.) / (2.0 * m)) +\
        zb * np.sqrt(((p * (1.0 - p)) / m) + ((zb ** 2.) / (4.0 * (m ** 2.))))
    return ci / (1.0 + ((zb ** 2.) / m))


class PSHATests():
    """Implements functionality to conduct the PSHA tests comparing models to
    observations including the following:

    Implements the statistical testing methods of Albarello & D'Amico (2008):
    1) Counting method (Albarello & D'Amico, 2008)
    2) Likelihood method (Albrarello & D'Amico, 2008)
    3) Number of sites exceeding fixed GM levels (Mak & Schorlemmer, 2016)
    4) Number of sites exceeding GMs for fixed PoE levels (Mak & Schorlemmer, 2016)

    Attributes:
        observations: Ground motion observations
        models: Seismic hazard model
        investigation_time: The investigation time to which the PoEs in the PSHA model refer
        model_imts: Model intensity measure types
        station_list: The list of stations being considered here (from the observations)
        nsta: The number of stations
        neq: Number of earthquakes considered here
        nrlzs: Number of logic tree branches in the model
        quantiles: The number of quantiles in the model
        nyrs: Total duration of the testing period
    """

    def __init__(self, observations, models,
                 start_date: str = "2000-01-01T00:00:00.00",
                 end_date: str = "2021-01-01T00:00:00.00",
                 as_prob: bool = False,
                 investigation_time: float = 1.0,
                 use_stats: bool = False):
        """
        Args:
            observartions: The ground motiom observations
            models: The seismic haard values for the site
            start_date: Earliest date of the observation period as string formatted with
                        YYYY-mm-ddTHH:MM:ss.ss
            end_date: Last date of the observation period as string formatted with
                      YYYY-mm-ddTHH:MM:ss.ss
            as_prob: ?
            investigation_time: Investigation time to which the model PoEs refer
        """
        self.observations = observations
        self.models = models
        self.investigation_time = investigation_time
        self.model_imts = list(self.models["IMTLs"])
        self.station_list = self.observations.stations.index.to_list()
        for stn in self.station_list:
            assert stn in self.models["hcurves"], "No model curves found for %s" % stn
            assert stn in self.models["hstats"], "No stats curves found for %s" % stn
        self.nsta = len(self.station_list)
        assert self.nsta == self.observations.accelerations.shape[1]
        self.neq = self.observations.accelerations.shape[0]
        self.nrlzs = len(self.models["rlzs"])
        self.nqntl = len(self.models["quantiles"])
        self.nyrs = (
            np.datetime64(end_date) - np.datetime64(start_date)
        ).astype("timedelta64[D]").astype(float) / 365.25

    def counting_method_imls(self, target_imls: np.ndarray, imt: str,
                             nobs_samp: int = 0, use_stats: bool = False) -> np.ndarray:
        """
        Applies the counting method of PSHA testing described in equations
        2 - 9 of Albarello & D'Amico (2008) with respect to fixed intensity
        measure levels.

        Args:
            target_imls: Set of ground motion levels for counting exceedances
            imt: Get results for a the selected intensity measure type
            nobs_samp: Number of samples for the observed ground motions
            use_stats: Calculate for just the mean and quantiles (True) or
                       calculate for all branches (False)
        Returns:
            residual_matrix: A 3D matrix [nimls, nrlzs, nsamp] containing a
                             the normalized residual between the observed and expected number
                             of exceedances for each realisation and intensity measure level
        """
        nimls = len(target_imls)
        logging.info("Running Counting Method Test on IMLs for %s with %g IMLs"
                     % (imt, nimls))
        # Get the mu and sigma estimates based on the probabilities
        # of exceedance of each IML at each station from the PSHA curves
        mu_si = self.get_prob_exceedance_model(
            {imt: target_imls},
            self.nyrs,
            use_stats)[imt]  # Equation 3
        # Observed exceedance has dimensions [nsta, nimls, nsamp]
        observed_exceedance = self.get_observed_exceedance(target_imls,
                                                           imt,
                                                           nobs_samp)
        # For a given IMT, model exceedance has dimensions [nsta, nimls, nrlzs]
        nrlzs = mu_si.shape[2]
        sigma_si = np.sqrt(mu_si * (1.0 - mu_si))  # Equation 4
        # Count the number of stations exceeding the IML
        nstar = np.sum(observed_exceedance > 0, axis=0)  # Equation 5
        # Assuming stations are independent, sum over nstations to give the
        # total forecast for the region
        mu_i = np.sum(mu_si, axis=0)  # Equation 6
        sigma_i = np.sum(sigma_si, axis=0)  # Equation 8
        # According to Albarello & D'Amico (2008), when S is large
        # P(|N* - mu_i(N)| >= 2 * sigma_i(N)) ~== 0.05
        # nstar has dim. [nimls, nsamp]
        # mu_i has dim. [nimls, nrlzs]
        # sigma_i has dim. [nimls, nrlzs]
        residual_matrix = np.zeros([nimls, nrlzs, nobs_samp], dtype=float)
        for i in range(nobs_samp):
            for j in range(nrlzs):
                residual_matrix[:, j, i] = (nstar[:, i] - mu_i[:, j]) / sigma_i[:, j]
        return residual_matrix

    def counting_method_probs(self, target_probs: np.ndarray, imt: str,
                              nobs_samp: int = 0, use_stats=False) -> np.ndarray:
        """
        Runs the counting method in the manner presented in the application
        of Albarello & D'Amico, i.e. with a fixed PoE, meaning that exceedences
        are with respect to the IML corresponding to the PoE at each site

        Args:
            target_probs: Array of target probabilities of exceedance
            imt: Intensity measure type
            nobs_samp: Number of samples
            use_stats: Return results only for the quantiles (True) or for all curves (False)

        Returns:
            prob_sta: Number of stations exceeding their corresponding IML for the given
                      probabilities of exceedance for each probability, branch/quantile and
                      realization [Num. Probs., Num. Branches., Num. Samples]
        """
        nprobs = len(target_probs)
        logging.info("Running Counting Method Test on PoEs for %s with %g PoEs"
                     % (imt, nprobs))
        # Get the accelerations corresponding to the target intensity measure
        target_accelerations = self.get_target_accelerations_model(
            target_probs, [imt], self.nyrs, use_stats)[imt]
        nrlzs = target_accelerations.shape[2]
        eff_nsamp = max(nobs_samp, 1)
        prob_nsta = np.zeros([nprobs, nrlzs, eff_nsamp])
        for i in range(nrlzs):
            # Get observed exceedances from the target accelerations
            obs_exceedances = self.get_observed_exceedance(
                target_accelerations[:, :, i], imt,
                nobs_samp, as_prob=False)
            # At this point obs_exceedances should have for each IMT the dimension
            # [nsta, nprobs, nsamp]
            # Retrieve number of stations exceeding their target acceleration
            # for the given probabilities
            nstar = np.sum(obs_exceedances > 0, axis=0)
            for j, prob in enumerate(target_probs):
                prob_nsta[j, i, :] = binom.sf(nstar[j, :], self.nsta, prob)
        return prob_nsta

    def get_likelihoods_imls(self, target_imls: np.ndarray, imt: str,
                             nobs_samp: int, nsamp: int,
                             use_stats: bool = False) -> Tuple[np.ndarray, np.ndarray]:
        """Applies the log-likelihood approach for testing PSHA described in
        Albarerllo & D'Amico (2008) section 2.2

        Args:
            target_imls: Vector of target IMLs for calculating the exceedance
            imt: Intensity measure type (string)
            nobs_samp: Number of samples to sample uncertainty on observation
            nsamp: Numver of samples to sample uncertainty on the model
            use_stats: Use the mean and quantiles (True) or all curves (False)

        Returns:
            model_loglikelihoods: Array of log-likelihoods for the model [nimls, nsamp, nrlzs]
                                  where nrlzs is the number of model hazard curves or number
                                  of quantiles plus the mean
            obs_loglikelihoods: Array of log-likelihoods for the observations
                                [nimls, nobs_samp, nrlzs]

        """
        logging.info("Running IML likelihood test for %s with %g IMLs"
                     % (imt, len(target_imls)))
        # Get the modelled and observed probabilities of exceedance
        prob = self.get_prob_exceedance_model(
            {imt: target_imls},
            self.nyrs, use_stats)[imt]
        # Probability has dimensions [nsta, niml, nrlzs] for each imt
        nsta, nimls, nrlzs = prob.shape
        # Observations have dimensions [nsta, niml, nobs_samp]
        obs = self.get_observed_exceedance(target_imls, imt, nobs_samp)
        # Pre-allocate the likelihood arrays
        # For the model we have one likelihood value per iml, sample and realization
        model_loglikelihoods = np.zeros([nimls, nsamp, nrlzs])
        # For the observations we have one likelihood value per iml, observation sample
        # and realization
        obs_loglikelihoods = np.zeros([nimls, nobs_samp, nrlzs])
        for i, iml in enumerate(target_imls):
            # Monte Carlo sample the numbers of stations exceeding the ground
            # motion level, as predicted by the model
            prob_iml = prob[:, i, :]  # [nsta, nrlzs]
            model_samples = np.random.uniform(0.0, 1.0, [nsta, nrlzs, nsamp])
            for j in range(nsamp):
                # Exceedance occurs when the uniformly distributed variate is
                # less than the probability of exceedance
                model_sample = (model_samples[:, :, j] < prob_iml).astype(int)
                for k in range(nrlzs):
                    # For each sample and each realization of the LT,
                    # get the likelihood function based on number of
                    # stations exceeding the target acceleration
                    model_loglikelihoods[i, j, k] =\
                        self._loglikelihood(model_sample[:, k],
                                            prob_iml[:, k])
            # Now to the observations ...
            # Get the observed exceedances of the given intensty level
            obs_iml = obs[:, i, :]  # [nsta, nsamp]
            for j in range(nobs_samp):
                for k in range(nrlzs):
                    obs_loglikelihoods[i, j, k] = self._loglikelihood(
                        obs_iml[:, j], prob_iml[:, k]
                    )
        return model_loglikelihoods, obs_loglikelihoods

    def get_likelihoods_poes(self, target_poes: np.ndarray, imt: str,
                             nobs_samp: int, nsamp: int,
                             use_stats: bool = False) -> Tuple[np.ndarray, np.ndarray]:
        """Implements the likelihood method described in Albarello & D'Amico (2008) in terms
        of target probabilities of exceedance rather than ground motion levels

        Args:
            target_poes: Vector of target probabilities of exceedance
            imt: Intensity measure type (string)
            nobs_samp: Number of samples to sample uncertainty on observation
            nsamp: Numver of samples to sample uncertainty on the model
            use_stats: Use the mean and quantiles (True) or all curves (False)

        Returns:
            expected_loglikelihoods: Array of log-likelihoods from sampling the PoEs
            obs_loglikelihoods: Array of log-likelihoods for the observations
                                [nimls, nobs_samp, nrlzs]

        """
        nprobs = len(target_poes)
        logging.info("Running PoE likelihood test for %s with %g PoEs" % (imt, nprobs))

        # Get the accelerations corresponding to the target intensity measure
        # Target accelerations have dimension [nsta, nprobs, nrlz]
        target_accelerations = self.get_target_accelerations_model(
            target_poes, [imt], self.nyrs, use_stats)[imt]
        nrlzs = target_accelerations.shape[2]

        # Sample the number of stations expected to exceed their
        # target acceleration for each probability level.
        # Given that the probability is fixed, we can generated simulations
        # of numbers of stations exceeding the acceleration as the target probability
        # without actually know the target accelerations
        expected_loglikelihoods = np.zeros([nprobs, nsamp, nrlzs])
        model_samples = np.random.uniform(0.0, 1.0, size=[self.nsta, nsamp, nrlzs])
        logging.info("Executing model log-likelihoods using %g samples" % nsamp)
        for i in range(nprobs):
            model_realization = (model_samples < target_poes[i]).astype(int)
            for j in range(nsamp):
                for k in range(nrlzs):
                    expected_loglikelihoods[i, j, k] = self._loglikelihood(
                        model_realization[:, j, k], target_poes[i])
        # Now count the observed exceedences of the ground motion for each probability level
        logging.info("Executing observation log-likelihoods based on %g samples"
                     % nobs_samp)
        obs_loglikelihoods = np.zeros([nprobs, nobs_samp, nrlzs])
        for j in range(nrlzs):
            obs = self.get_observed_exceedance(target_accelerations[:, :, j],
                                               imt,
                                               nobs_samp)
            for i in range(nprobs):
                exceedance = (obs[:, i, :] > 0).astype(int)
                for k in range(nobs_samp):
                    obs_loglikelihoods[i, k, j] = self._loglikelihood(
                        exceedance[:, k],
                        target_poes[i])
        return expected_loglikelihoods, obs_loglikelihoods

    @staticmethod
    def _loglikelihood(obs: np.ndarray, prob: Union[np.ndarray, float]) -> np.ndarray:
        """
        Returns the loglikelihood function expressed by equation 13
        of Albarello & D'Amico (2008):

        l_i = sum_{s=1}^{N*} log(p(es|H)) + sum_{N* + 1}^{S} log(1 - p(es|H))

        Args:
            obs: Vector of observations of exceedance (one per station)
            prob: Probabilities of exceedance as either vector corresponding to
                  each station or a single probability value
        Returns:
            log-likelihood for the corresponding observation and probability
        """
        idx = obs > 0
        nidx = np.logical_not(idx)
        nstar = np.sum(idx)
        s_minus_n_star = np.sum(nidx)
        if isinstance(prob, float):
            is_prob = prob * np.ones(nstar, dtype=float)
            not_prob = (1.0 - prob) * np.ones(s_minus_n_star)
        else:
            is_prob = prob[idx]
            not_prob = (1.0 - prob[nidx])
        return np.sum(np.log10(is_prob)) + np.sum(np.log10(not_prob))

    def get_observed_exceedance(
            self,
            target_imls: np.ndarray,
            imt: str,
            nsamp: int = 0,
            as_prob: bool = False
            ) -> np.ndarray:
        """
        Returns array for each IMT indicating whether or not the target accelerations
        are exceeded at each station. It nsamp > 0 then sampling of the uncertainty
        for each event will take place and an exceedance will be counted on each sample.

        For the given IMT, the output array will take the shape [nsta, niml, nsamp] where nsta
        is the number of stations, niml the number of target intensity levels for the IMT and
        nsamp is the number of sample

        Args:
            nsamp: It nsamp > 0 then nsamp samples of the acceleration uncertainty for each
                   event will be drawn, otherwise just the expected/observed accelerations
            as_prob: If True rather than drawing samples this will return a simple
                     [nsta, niml, 1] array with each value taking a probability that the
                     acceleration level is exceeded

        Returns:
            observed_exceedance: Array indicating the exceedance (or not) of the IMLs for
                                 each station: [nsta, nimls, nsamp]
        """
        if len(target_imls.shape) == 1:
            # Single vector of target imls - tile to match number of stations,
            # dim: [nsta, nimls]
            target_imls = np.tile(target_imls, [self.nsta, 1])
        nimls = target_imls.shape[1]
        max_t, min_t = np.max(self.observations.periods), np.min(self.observations.periods)
        obs_periods = self.observations.periods

        # First issue is that we need to retrieve the observations for the
        # specific IMT
        if imt == "PGA":
            period = 0.0
        else:
            period = float(imt.replace("SA(", "").replace(")", ""))
        # See if period is in the list of those calculated
        if (period < min_t) or (period > max_t):
            logging.info(
                "Requested imt %s outside the observed period range %.3f - %.3f - skipping"
                % (imt, min_t, max_t)
            )
            return None
        # See if there is an exect value for the period
        idx = np.where(np.isclose(period, obs_periods))[0]
        if len(idx):
            # Exact period found
            obs_accels = self.observations.accelerations[:, :, idx[0]]
            obs_sigma = self.observations.sigmas[:, :, idx[0]]
        else:
            # Need to interpolate acceleration and sigma from neighbouring periods
            iloc = np.searchsorted(obs_periods, period)
            t_low, t_high = np.log10(obs_periods[iloc - 1]), np.log10(obs_periods[iloc])
            dt = t_high - t_low
            dx0 = np.log10(period) - t_low
            obs_accels = self.observations.accelerations[:, :, iloc - 1] + dx0 * (
                (self.observations.accelerations[:, :, iloc] -
                 self.observations.accelerations[:, :, iloc - 1]) / dt
            )
            obs_sigma = self.observations.sigmas[:, :, iloc - 1] + dx0 * (
                (self.observations.sigmas[:, :, iloc] -
                 self.observations.sigmas[:, :, iloc - 1]) / dt
            )

        #  Now have the observed accelerations and sigmas for the given period
        if as_prob:
            # Sum up the probabilities of IML > IML0 at each station for each
            # earthquake
            observed_exceedance = np.zeros([self.nsta, nimls, 1])
            for j in range(nimls):
                liml = np.log(target_imls[:, j])

                probs = norm.cdf(liml, loc=np.log(obs_accels), scale=obs_sigma)
                # Ensure that when the uncertainty is 0 and the observation
                # is both non-zero and less than the target iml then the
                # probability is 0
                probs[np.logical_and(obs_accels < np.exp(liml),
                                     np.isclose(obs_sigma, 0.0))] = 1.0
                observed_exceedance[:, j, 0] = 1.0 - np.nanprod(probs, axis=0)
        else:
            if nsamp == 0:
                # No sampling, just take accelerations as given
                observed_exceedance = np.zeros([self.nsta, nimls, 1])
                for j in range(nimls):
                    for k in range(self.neq):
                        exceed = obs_accels[k, :] >= target_imls[:, j]
                        observed_exceedance[exceed, j, 0] += 1
            else:
                # Generate random samples from a Gaussian distribution
                epsilons = np.random.normal(size=[self.neq, self.nsta, nsamp])
                observed_exceedance = np.zeros([self.nsta, nimls, nsamp], dtype=int)
                for i in range(nsamp):
                    sample_accelerations = np.exp(
                        np.log(obs_accels) + epsilons[:, :, i] * obs_sigma)
                    for j in range(nimls):
                        for k in range(self.neq):
                            exceed = sample_accelerations[k, :] >= target_imls[:, j]
                            observed_exceedance[exceed, j, i] += 1
        return observed_exceedance

    def get_rate_exceedance_model(
            self,
            target_imtls: Dict,
            target_investigation_time: float,
            use_stats: bool = False
            ) -> Dict:
        """Hazard curves from the model given in terms of rates of exceedance within the
        specified time period

        Args:
            target_imtls: Dictionary of target intenstify measure levels for the exceedance
                          calculations organised by IMT
            target_investigation_time: Investigation time used in original PSHA calculations
            use_stats: Use just the mean and quantiles (True) or use all branches (False)
        Returns:
            rates_exceedance: Dictionary that for each IMT contains the rate
                              of exceeding each of the target IMLs for each station and
                              each logic tree realisation: shape [nsta, nimls, nrlzs]
        """
        rate_exceedance = {}
        logging.info("Getting model rates of exceedance")
        for imt in target_imtls:
            target_imls = target_imtls[imt]
            nimls = len(target_imls)
            # Pre-allocated arrays with zeros
            if use_stats:
                rate_exceedance[imt] = np.zeros([self.nsta, nimls, self.nqntl + 1])
                result_key = "hstats"
            else:
                rate_exceedance[imt] = np.zeros([self.nsta, nimls, self.nrlzs])
                result_key = "hcurves"
            orig_imls = self.models["IMTLs"][imt]
            imt_loc = self.model_imts.index(imt)
            for i, stn in enumerate(self.station_list):
                # Shapes of the PoE arrays are [nrlzs, nimt, nimls]
                poes = self.models[result_key][stn][:, imt_loc, :]
                # Interpolate to target_levels
                ipl = interp1d(np.log10(orig_imls), poes)
                annual_rates = -np.log(1.0 - ipl(np.log10(target_imls))) /\
                    self.investigation_time
                rate_exceedance[imt][i, :, :] = annual_rates.T * target_investigation_time
        return rate_exceedance

    def get_prob_exceedance_model(
            self,
            target_imtls: np.ndarray,
            target_investigation_time: float,
            use_stats: bool = False
            ) -> Dict:
        """Hazard curves given in terms of probabilities of exceedance within the
        specified time period

        Args:
            target_imtls: The inten
            investigation_time: Investigation time used in original PSHA calculations
            use_stats: Use just the mean and quantiles (True) or use all branches (False)
        Returns:
            prob_exceedance: Dictionary that for each IMT contains the probability
                             of exceeding each of the target IMLs for each station and
                             each logic tree realisation: shape [nsta, nimls, nrlzs]
        """
        prob_exceedance = {}
        logging.info("Getting model probability of exceedance")
        rates = self.get_rate_exceedance_model(target_imtls,
                                               target_investigation_time,
                                               use_stats)
        for imt in rates:
            prob_exceedance[imt] = 1.0 - np.exp(-rates[imt])
        return prob_exceedance

    def get_target_accelerations_model(
            self,
            target_probs: np.ndarray,
            imts: List,
            target_investigation_time: float,
            use_stats: bool = False
            ) -> Dict:
        """Returns the set of accelerations given fixed probabilities of exceedance in T years

        Args:
            target_probs: Array of probabilities of exceedance for which to determine the
                          target accelerations
            imts: List of intensity measure types
            target_investigation_time: The total investigation time for the probabilities of
                                       exceedance
            use_stats: Use just the mean and quantiles (True) or use all branches (False)

        Returns:
            target_imls: IMT indexed dictionary of accelerations corresponding to the
                         fixed probabilities of exceedance
        """
        target_imls = {}
        nprobs = len(target_probs)
        logging.info(
            "Getting accelerations for fixed probabilities of exceedance in %.2f years"
            % target_investigation_time)
        log_target_probs = np.log10(target_probs)
        for imt in imts:
            model_imls = self.models["IMTLs"][imt]
            limls = np.log10(model_imls[::-1])
            # Pre-allocated arrays with zeros
            if use_stats:
                target_imls[imt] = np.zeros([self.nsta, nprobs, self.nqntl + 1])
                result_key = "hstats"
            else:
                target_imls[imt] = np.zeros([self.nsta, nprobs, self.nrlzs])
                result_key = "hcurves"
            imt_loc = self.model_imts.index(imt)
            for i, stn in enumerate(self.station_list):
                # Get the PoEs for a given IML and sort from low to high
                poes = self.models[result_key][stn][:, imt_loc, ::-1]
                # Convert to PoEs in target investigation time
                # Convert from PoEs w.r.t. original investigation time to annual rates
                rates = -np.log(1.0 - poes) / self.investigation_time
                poes_t = 1.0 - np.exp(-rates * target_investigation_time)
                for j in range(poes_t.shape[0]):
                    # Convert from rates to PoEs w.r.t. target investigation time
                    ipl = interp1d(np.log10(poes_t[j, :]), limls,
                                   assume_sorted=True, fill_value="extrapolate")
                    target_imls[imt][i, :, j] = 10.0 ** (ipl(log_target_probs))
        return target_imls

    def get_mak_schorlemmer_aggregated_curves_type4(
            self,
            return_periods: np.ndarray,
            imts: List,
            use_stats: bool = False,
            alpha: float = 0.9
            ) -> Dict:
        """
        Args:
            return_periods: Array of return periods for which the target IMLs will be generated
            imts: List of intensity measure types
            use_stats: Test just the mean and quantiles [True] or all curves [False]
            alpha: Binomial confidence interval to define the upper and lower bound curves

        Returns:
            agg_curves_test: The set test results containing the observed hazard curves per
                             imt (`h_obs`), the expected exceedances from the model (`h_ex`)
                             and the `lower` and `upper` range from the confidence intervals
        """
        # Get annual rates of exceedance from the return periods
        target_rates = 1.0 / return_periods
        # Get accelerations corresponding to target probabilities in investigation time
        target_imls = self.get_target_accelerations_model(
            1.0 - np.exp(-target_rates),  # Get the accelerations for target annual rates
            imts,
            1.0,
            use_stats=use_stats)
        # Get the target probabilities in Nyrs
        target_probs = 1.0 - np.exp(-target_rates * self.nyrs)
        nprobs = len(target_probs)
        nrlzs = target_imls[imts[0]].shape[2]
        # In this case the sites all have equal observation periods, thus
        # equal probabilities
        # Can retrieve binomial distribution confidence intervals directly
        lower_bounds, upper_bounds = binom.interval(alpha, self.nsta, target_probs)
        agg_curves_test = {
            "h_obs": dict([(imt, np.zeros([nprobs, nrlzs])) for imt in imts]),
            "h_ex": target_probs * self.nsta,
            "lower": lower_bounds,
            "upper": upper_bounds
            }
        for imt in imts:
            for i in range(nrlzs):
                observed_exceedance = self.get_observed_exceedance(target_imls[imt][:, :, i],
                                                                   imt,
                                                                   as_prob=True)
                agg_curves_test["h_obs"][imt][:, i] =\
                    np.sum(observed_exceedance, axis=0).flatten()
        return agg_curves_test

    def get_all_exceedance_imls(
            self,
            target_imtls: Dict,
            nsamp: int = 0,
            as_prob: bool = True,
            use_stats: bool = True
            ) -> Tuple[Dict, Dict]:
        """For a given set of IMTLs this returns the observed number of stations exceeding them
        and the probabilities of exceedance from the models. Used as inputs for creating plots
        of the Type 2 curves

        Args:
            target_imtls: Dictionary of intensity measure levels organised by IMT
            nsamp: Number of samples to use for uncertainty
            as_prob: Returns the results as probabilities (True) or rates (False)
            use_stats: Use the mean and quantiles (True) or all curves (False)

        Returns:
            obs_exceed_imt: Number of stations exceeding the IML for each IML and each IMT
            model_prob_exceed: Dictionary of probabilities of exceedance for IML and IMT
        """
        obs_exceed_imt = {}
        for imt, imls in target_imtls.items():
            obs_exceed_imt[imt] = self.get_observed_exceedance(imls, imt, nsamp, as_prob)
        model_prob_exceed = self.get_prob_exceedance_model(target_imtls, self.nyrs, use_stats)
        return obs_exceed_imt, model_prob_exceed

    def get_all_exceedance_poes(
            self,
            target_probs: np.ndarray,
            imts: List,
            nsamp: int = 0,
            as_prob: bool = True,
            use_stats: bool = True,
            ) -> Tuple[Dict, Dict]:
        """Retrieves all of the observed number of stations exceeding their IMLs corresponding
        to the target probabilities of excedance

        Args:
            target_probs: Vector of target probabilities of exceedance
            imts: List of intensity measure types (as strings)
            nsamp: Number of samples to use for uncertainty
            as_prob: Returns the results as probabilities (True) or rates (False)
            use_stats: Use the mean and quantiles (True) or all curves (False)
        """
        obs_exceed_poes = {}
        nprobs = len(target_probs)
        if use_stats:
            ncurves = self.nqntl + 1
        else:
            ncurves = self.nrlzs

        model_imls = self.get_target_accelerations_model(target_probs, imts,
                                                         self.nyrs, use_stats)
        for imt in imts:
            obs_exceed_poes[imt] = np.zeros([self.nsta, nprobs, ncurves])
            for i in range(ncurves):
                obs_exceed_poes[imt][:, :, i] = self.get_observed_exceedance(
                    model_imls[imt][:, :, i],
                    imt,
                    as_prob=True)[:, :, 0]
        return obs_exceed_poes, model_imls

    def plot_nsites_exceeding_imls(
            self,
            target_imtls: Dict,
            obs_exceed_all: Dict,
            model_prob_exceed: Dict,
            imt: str,
            title: str = "",
            figax: Optional[Tuple] = None,
            figsize: Tuple = (10, 10),
            xlim: Tuple = (),
            ylim: Tuple = (),
            filename: str = "",
            filetype: str = "png",
            dpi: int = 300):
        """Creates a plot of the number of sites exceeding a given intensity measure level
        against the intensity measure levels (i.e. the Mak & Schorlemmer (2016) Type 2 curves)

        Args:
            target_imtls: Dictionary of intensity measure types and levels
            obs_exceed_all: Number of stations exceeding the IMLs for each IMT
                            (output from .get_all_exceedance_imls)
            model_prob_exceed: Dictionaries of probabilities of exceedance for each IMT
                            (output from .get_all_exceedance_imls)
            imt: Intensity measure type (as string)
            title: (Optional) Figrue title
            figax: To append the curve to an existing figure this is a tuple of the
                   matplotlib Figure and Axes objects
            figsize: If not adding to figure but rather setting a new one then this controls
                     the figure size
            xlim: (Low, High) limits of x-axis
            ylim: (Low, High) limits of y-axis
            filename: Path to file to save figure
            filetype: Type of file to save figure
            dpi: Resolution in dots per inch
        """
        if figax is None:
            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(111)
        else:
            fig, ax = figax
        obs_exceed = obs_exceed_all[imt]
        target_imls = target_imtls[imt]

        sum_probs = np.sum(model_prob_exceed[imt], axis=0)
        # If the model has quantiles then plot these
        nqntl = len(self.models["quantiles"])
        if nqntl:
            qntl_label = "-".join(["{:.0f}".format(100 * qntl)
                                   for qntl in self.models["quantiles"]])
            for i in range(1, nqntl - 1):
                ax.semilogx(target_imls, sum_probs[:, i], "ro--", lw=2)
            ax.semilogx(target_imls, sum_probs[:, -1], "ro--", lw=2,
                        label="Model Percentiles\n%s" % qntl_label)
        # Plot the mean model
        ax.semilogx(target_imls, sum_probs[:, 0], "o-",
                    color="tab:blue", lw=3, label="Mean")
        # Plot the observed exceedance
        ax.semilogx(target_imls, np.sum(obs_exceed, axis=0).flatten(),
                    "ko-", lw=3, label="Observed")
        # Decorate
        ax.grid(which="both")
        ax.set_xlabel("% s (g)" % imt, fontsize=18)
        ax.set_ylabel("# Sites Exceeding IML", fontsize=18)
        ax.tick_params(labelsize=14)
        if len(xlim):
            ax.set_xlim(*xlim)
        if len(ylim):
            ax.set_ylim(*ylim)
        else:
            ax.set_ylim(0)  # Set a minimum of 0
        ax.legend(loc="upper right", fontsize=20)
        ax.set_title("%s (%g Sites) - %s" % (title, self.nsta, imt), fontsize=20)
        if filename:
            plt.savefig(filename, format=filetype, dpi=dpi, bbox_inches="tight")
        return fig, ax

    def plot_nsites_exceeding_poes(
            self,
            target_probs: np.ndarray,
            obs_exceed_poes: Dict,
            imt: str,
            title: str = "",
            figax: Optional[Tuple] = None,
            figsize: Tuple = (10, 10),
            xlim: Tuple = (),
            ylim: Tuple = (),
            filename: str = "",
            filetype: str = "png",
            dpi: str = 300):
        """Creates a plot of the number of sites exceeding the ground motion for a given
        return period against the return periods (Mak & Schorlemmer (2016) Type 4 curves)

        Args:
            target_probs: Target probabilities of exceedance
            obs_exceed_poes: IMT-sorted dictionary containing the number of stations exceeding
                             the IMLs corresponding to the PoEs. Output from
                             .get_all_exceedance_poes

            imt: Intensity measure type (as string)
            title: (Optional) Figrue title
            figax: To append the curve to an existing figure this is a tuple of the
                   matplotlib Figure and Axes objects
            figsize: If not adding to figure but rather setting a new one then this controls
                     the figure size
            xlim: (Low, High) limits of x-axis
            ylim: (Low, High) limits of y-axis
            filename: Path to file to save figure
            filetype: Type of file to save figure
            dpi: Resolution in dots per inch
        """
        if figax is None:
            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(111)
        else:
            fig, ax = figax
        obs_exceed_poe = obs_exceed_poes[imt]
        nsites_poes = np.sum(obs_exceed_poe, axis=0)
        nqntl = len(self.models["quantiles"])
        # Plot the results for each quantile if present
        if nqntl:
            qntl_label = "-".join(["{:.0f}".format(100 * qntl)
                                   for qntl in self.models["quantiles"]])
            for i in range(1, nqntl):
                ax.semilogx(target_probs, nsites_poes[:, i], "ro--", lw=2)
            ax.semilogx(target_probs, nsites_poes[:, -1], "ro--", lw=2,
                        label="Percentiles\n%s" % qntl_label)
        # Add the mean
        ax.semilogx(target_probs, nsites_poes[:, 0], "o-",
                    color="tab:blue", lw=3, label="Mean")
        # Add the number expected from a Binomial distribution with
        # known number of stations and fixed probabilities
        ax.semilogx(target_probs, binom.mean(self.nsta, target_probs),
                    "k-", lw=3, label="Expected (Binom.)")
        # Add the confidence interval
        lower, upper = binom.interval(0.95, self.nsta, target_probs)
        ax.fill_between(target_probs, lower, upper, color="darkgrey",
                        alpha=0.6, label="95 % CI (Binom.)")
        # Decorate
        ax.grid(which="both")
        ax.set_xlabel("Prob. of Exceedance in T = %.0f years" % self.nyrs, fontsize=20)
        ax.set_ylabel("# Sites Exceeding IML for Target PoE", fontsize=20)
        ax.legend(loc="upper left", fontsize=18)
        ax.tick_params(labelsize=14)
        if len(xlim):
            ax.set_xlim(*xlim)
        if len(ylim):
            ax.set_ylim(*ylim)
        else:
            ax.set_ylim(0)  # Set minimum to 0
        ax.set_title("%s (%g Sites) - %s" % (title, self.nsta, imt), fontsize=20)
        if filename:
            plt.savefig(filename, format=filetype, dpi=dpi, bbox_inches="tight")
        return fig, ax

    def plot_likelihood_stats(
            self,
            model_stats: np.ndarray,
            obs_stats: np.ndarray,
            poes: np.ndarray,
            selected_poe: float,
            llh_specs: Tuple = (),
            title: str = "",
            xlim: Tuple = (),
            ylim: Tuple = (),
            figsize: Tuple = (12, 12),
            filename: str = "",
            filetype: str = "png",
            dpi: int = 300):
        """Plots the creates bar plots of the likelihood statistics with uncertaity in both
        model and observations

        Args:
            model_stats: Monte Carlo sampled log-likelihoods for the model curves
            obs_stats: Monte Carlo sampled log-likelihoods for the observations
            poes: Vector of probabilities of exceedance
            select_poe: PoE to show for plots (must be in poes)
            llh_specs: Contols bar plot bins (low llh, high llh, bin width)
            title: (Optional) Figrue title
            figax: To append the curve to an existing figure this is a tuple of the
                   matplotlib Figure and Axes objects
            figsize: If not adding to figure but rather setting a new one then this controls
                     the figure size
            xlim: (Low, High) limits of x-axis
            ylim: (Low, High) limits of y-axis
            filename: Path to file to save figure
            filetype: Type of file to save figure
            dpi: Resolution in dots per inch
        """
        # Get the position of the selected PoE in the array
        if not np.any(np.isclose(poes, selected_poe)):
            raise ValueError("Selected POE %s not in defined PoEs %s"
                             % (str(selected_poe), str(poes)))
        else:
            poe_loc = np.argmin(np.fabs(poes - selected_poe))
        headers = ["Mean"] + ["{:.2f} %-ile".format(qntl) for qntl in self.models["quantiles"]]
        nplots = len(headers)
        nrow, ncol = best_subplot_dimensions(nplots)
        fig, axs = plt.subplots(nrow, ncol, figsize=figsize)
        if not len(llh_specs):
            llh_low = np.floor(min(np.min(model_stats), np.min(obs_stats)))
            llh_high = np.ceil(max(np.max(model_stats), np.max(obs_stats)))
            dllh = np.diff(np.linspace(llh_low, llh_high, 40))[0]
            llh_specs = (llh_low, llh_high, dllh)
        llh_bins = np.arange(*llh_specs)
        for i, (header, ax) in enumerate(zip(headers, axs.flatten())):
            model_hist = np.histogram(model_stats[poe_loc, :, i],
                                      bins=llh_bins, density=True)[0]
            obs_hist = np.histogram(obs_stats[poe_loc, :, i], bins=llh_bins, density=True)[0]
            ax.bar(llh_bins[:-1], model_hist, llh_specs[-1], facecolor="tab:red",
                   edgecolor="k", label="Expected")
            ax.bar(llh_bins[:-1], obs_hist, llh_specs[-1], facecolor="tab:blue",
                   edgecolor="b", alpha=0.6, label="Model")
            ax.grid(which="both")
            if len(xlim):
                ax.set_xlim(*xlim)
            if len(ylim):
                ax.set_ylim(*ylim)
            ax.set_xlabel("Log-likelihood (l)", fontsize=16)
            ax.set_ylabel("Density", fontsize=16)
            ax.legend(fontsize=14)
            ax.set_title(header, fontsize=18)
            ax.tick_params(labelsize=12)
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        if title:
            fig.suptitle(title, fontsize=20)
        if filename:
            plt.savefig(filename, format=filetype, dpi=dpi, bbox_inches="tight")
        return

    def plot_mak_schorlemmer_type4_curves(
            self,
            agg_curves_all: Dict,
            return_periods: np.array,
            imts: List,
            agg_curves_hstats: Optional[Dict] = None,
            figsize: Tuple = (12, 12),
            n_row_col: Tuple = (),
            title: str = "",
            filename: str = "",
            filetype: str = "png",
            dpi: int = 300):
        """Plots the complete picture of the seismic hazard curve comparisons using the
        Mak & Schorlemmer (2016) "Type 4" curves (No. of stations vs. return period)
        """
        if len(n_row_col):
            nrow, ncol = n_row_col
        else:
            nrow, ncol = best_subplot_dimensions(len(imts))
        fig, axs = plt.subplots(nrow, ncol, figsize=figsize)
        for i, (imt, ax) in enumerate(zip(imts, axs.flatten())):
            # Plot the suite of hazard curves first
            for row in agg_curves_all["h_obs"][imt].T:
                ax.plot(return_periods, row, "-", color="lightsteelblue", alpha=0.6)
            ax.plot(return_periods, row, "o-", color="lightsteelblue",
                    alpha=0.6, label="Individ.\nBranch")

            if agg_curves_hstats is not None:
                # If curves for the mean and quantiles are present then plot these
                for row in agg_curves_hstats["h_obs"][imt][:, 1:].T:
                    ax.plot(return_periods, row, "ro--", lw=1.5)
                # Add the mean
                ax.plot(return_periods, agg_curves_hstats["h_obs"][imt][:, 0], "o-",
                        color="gold", lw=2.5, label="Mean")
                ax.plot(return_periods, row, "ro--", lw=1.5, label="Quantiles")
            # Plot the expected hazard and the 95 % CI
            ax.plot(return_periods, agg_curves_all["h_ex"], "k-", lw=2, label="Expected")
            ax.fill_between(return_periods, agg_curves_all["lower"], agg_curves_all["upper"],
                            color="darkgrey", alpha=0.6, zorder=3, label="95 % CI")
            # Decorate figure
            ax.grid(which="both")
            ax.set_xscale("log")
            ax.set_xlabel("Return Period (yrs)", fontsize=16)
            ax.set_ylabel("# Stations with Exceedances", fontsize=16)
            ax.tick_params(labelsize=13)
            ax.set_title("%s" % imt, fontsize=16)
        # Place the legen on the last axis
        ax.legend(loc="lower left", fontsize=16, bbox_to_anchor=[0.6, 0.4], framealpha=0.9)
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        if title:
            fig.suptitle(title, fontsize=20)
        if filename:
            plt.savefig(filename, format=filetype, dpi=dpi, bbox_inches="tight")
        return
