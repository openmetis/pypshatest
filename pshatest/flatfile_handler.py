"""
Tools for managing flatfiles - from preprocessing to translation into
OpenQuake context objects
"""
import logging
from typing import Dict, Tuple, Union, Optional, Set
from random import randint
import numpy as np
import pandas as pd
import geopandas as gpd
from scipy.constants import g
from shapely import geometry
from openquake.hazardlib.imt import SA, from_string
from openquake.hazardlib.gsim.base import RuptureContext
from pshatest.utils import vs30_to_z1pt0_cy14, vs30_to_z2pt5_cb14, geodetic_distance
from pshatest.scenario_validator import ScenarioProperties
logging.basicConfig(level=logging.INFO)


def low_level_catalogue_harmoniser(
        db: pd.DataFrame,
        catalogue: pd.DataFrame,
        time_win: int,
        dist_win: float,
        print_match_types: Tuple = (0, 1, 2, 3)
        ) -> Tuple[Dict, Dict]:
    """Function to identify the common events in a magnitude-harmonised catalogue
    that match with those in the flatfile database

    Matches are classified into four types:

    0: Unique and uniambiguous match (one event found within time and distance window)

    1: No events in the magnitude harmonised catalogue found in the time window with respect
       centred on the event in the flatfile database

    2: One or more events fall within the time window, but none fall within the distance window

    3: Multiple events fall within the time and distance windows centred on the event in the
       flatfile database. Here a selection process of "relative mismatch" is applied,
       identifying the closest events.

    Args:
        db: Catalogue of the flatfile in the form of a pandas DataFrame, requiring columns
            [ev_id, ev_time, lon, lat, depth]
        catalogue: Magnitude harmonised catalogue in the form of a Pandas Dataframe, requiring
                   columns [master_id, ev_id, ev_time, lon, lat, depth
        time_win: Maximum time difference (in seconds) to classify a match
        dist_win: Maximum distance between events (in km) to classify a match
        print_match_types: Choose which types of matches are printed (see above - default is
                           all)

    Returns:
        matching_indices: Indices indicating the event in the harmonised catalogue that
                          matches the event in the flatfile (or nan if Type 1 or Type 2
                          mismatch)
        mismatched_events: Event IDs of the events in flatfile that result in a Type 1 or
                           Type 2 mismatch

    """
    # Low level catalogue harmoniser
    matching_indices = {}
    mismatched_events = {1: [], 2: []}

    for i in range(db.shape[0]):
        row = db.iloc[i]
        # Get time difference between
        tdiff = catalogue["ev_time"] - row.ev_time
        tidx = np.fabs(tdiff.dt.total_seconds()) <= time_win
        if not tidx.any():
            # Type 1 mismatch - no events within the time window
            if 1 in print_match_types:
                logging.info("%g: Type 1: No events in time window for %s %s (%.4f, %.4f)"
                             % (i, row.ev_id, str(row.ev_time), row.lon, row.lat))
            matching_indices[row.ev_id] = np.nan
            mismatched_events[1].append(row.ev_id)
            continue
        sel_cat = catalogue[tidx]
        # Get distance
        dist = geodetic_distance(row.lon, row.lat,
                                 sel_cat["lon"].to_numpy(),
                                 sel_cat["lat"].to_numpy())
        ridx = dist <= dist_win
        if not np.any(ridx):
            # Type 2 mismatch - events in time window but nothing within distance window
            if 2 in print_match_types:
                logging.info("%g: Type 2: Events in time window for %s %s (%.4f, %.4f)"
                             "- but not distance window" % (i, row.ev_id, str(row.ev_time),
                                                            row.lon, row.lat))
            matching_indices[row.ev_id] = np.nan
            mismatched_events[2].append(row.ev_id)
        elif np.sum(ridx) > 1:
            # Multiple matches - need to figure out which to take
            sel_cat = sel_cat[ridx]
            # Get the distances and time differences of the matching events
            dist = dist[ridx]
            min_dist_loc = np.argmin(dist)
            tdiff2 = (sel_cat["ev_time"] - row.ev_time).dt.total_seconds().to_numpy()
            min_tdiff_loc = np.argmin(np.fabs(tdiff2))

            if min_dist_loc == min_tdiff_loc:
                # Nearest event in time agrees with the nearest event in distance
                sel_idx = sel_cat.index[min_dist_loc]
            else:
                # Select the distance over the time
                near_event_norm_dist = dist[min_dist_loc] / dist_win
                near_event_norm_time = np.fabs(tdiff2[min_tdiff_loc]) / time_win
                if near_event_norm_time < near_event_norm_dist:
                    sel_idx = sel_cat.index[min_tdiff_loc]
                else:
                    sel_idx = sel_cat.index[min_dist_loc]
            if 3 in print_match_types:
                logging.info(
                    "%g: Type 3: %g candidates match criteria for %s %s (%.4f, %.4f)"
                    % (i, sel_cat.shape[0], row.ev_id, str(row.ev_time), row.lon, row.lat)
                )
                for j, (sel_row_idx, sel_row) in enumerate(sel_cat.iterrows()):
                    logging.info(
                        "    - %g %s (%s - %.4f %.4f) AGY: %s Mharm: %.2f DR: %.3f DT: %.5f"
                        % (j, sel_row.master_id, str(sel_row.ev_time), sel_row.lon,
                           sel_row.lat, sel_row.agency, sel_row.M, dist[j], tdiff2[j]))
                logging.info("Selected %s" % sel_cat["master_id"].loc[sel_idx])
            matching_indices[row.ev_id] = sel_idx
        else:
            # One single match
            sel_cat = sel_cat[ridx]
            index, match_id = sel_cat.index[0], sel_cat["master_id"].iloc[0]
            if 0 in print_match_types:
                logging.info(
                    "%g: Type 0: %s (%s - %.4f, %.4f) matches  %g %s (%s - %.4f, %.4f)"
                    % (i, row.ev_id, str(row.ev_time), row.lon, row.lat,
                       index, match_id, str(sel_cat.ev_time.iloc[0]),
                       sel_cat.lon.iloc[0], sel_cat.lat.iloc[0])
                )
            matching_indices[row.ev_id] = index
    return matching_indices, mismatched_events


def add_harmonised_magnitudes_to_flatfile(
        flatfile_database: pd.DataFrame,
        harmonised_catalogue: pd.DataFrame,
        matching_index: Dict) -> pd.DataFrame:
    """Given a flatfile database, harmonised catalogue and a dictionary of matching
    IDs this adds the harmonised magnitude information to the original flatfile

    Args:
        flatfile_database: Database corresponding to the ground motion flatfile
        harmonised_catalogue: Dataframe corresponding to the harmonised catalogue
        matching_index: Dictionary mapping the event ID given in the flatfile
                        database to the index of the harmonised catalogue

    Returns:
        flatfile_database: Flatfile with magnitude harmonised catalogues added
    """
    nrecs = flatfile_database.shape[0]
    mags = np.nan * np.ones(nrecs)
    sigma_m = np.nan * np.ones(nrecs)
    mtype = [None] * nrecs
    mlog = [None] * nrecs
    mids = [None] * nrecs

    flatfile_ids = flatfile_database["ev_id"].unique()
    for fid in flatfile_ids:
        if fid not in list(matching_index):
            logging.info("Flatfile event ID %s not in matching index" % fid)
            continue
        # Get the boolean vector of the locations in the flatfile corresponding
        # to the event ID
        locs = flatfile_database["ev_id"] == fid
        if np.isnan(matching_index[fid]):
            logging.info("Flatfile event %s had no match in the harmonised catalogue"
                         % fid)
            # See if the flatfile has an Mw
            ff_mag = flatfile_database[locs]["mag_orig"].iloc[0]
            ff_mlog = flatfile_database[locs]["mag_ref_orig"].iloc[0]
            idx = np.where(locs)[0]
            mags[idx] = ff_mag
            sigma_m[idx] = np.nan
            for i in idx:
                mlog[i] = ff_mlog
        else:
            harm_event = harmonised_catalogue.loc[matching_index[fid]]
            idx = np.where(locs)[0]
            mags[idx] = harm_event.M
            sigma_m[idx] = harm_event.sigma_m
            for i in idx:
                mtype[i] = harm_event.mtype
                mlog[i] = harm_event.agency
                mids[i] = harm_event.master_id
    flatfile_database["mag"] = mags
    flatfile_database["mag_unc"] = sigma_m
    flatfile_database["mag_type"] = mtype
    flatfile_database["Mlog"] = mlog
    flatfile_database["M_CAT_ID"] = mids
    return flatfile_database


class FlatfileFormatter(object):
    """
    Read in the event data from the flatfile and store as set of Event objects with
    required metadata organised into the OpenQuake context objects

    Attributes:
        flatfile: Ground motion data flatfile as pandas dataframe
        name: Name to identifiy the flatfile
        record_ids: The ID of each ground motion observation in the flatfile
        event_ids: The ID of each event in the flatfile
        station_ids: The ID of each station in the flatfile
        imts: The list of intensity measure types found in the flatiles
        imt_keys: The corresponding set of keys for each intensity measure type
        reference_rock: If specified then will set the Vs30 to the reference site condition
        basin_region: For Z1.0 and Z2.5 defines which conversion equation from Vs30 to use
    """

    SOURCE_PARAMS = set(["mag", "rake", "dip", "z_tor", "z_bor", "ev_depth",
                         "f_width", "f_length", "ev_lon", "ev_lat", "ev_time"])

    DISTANCES = set(["r_jb", "r_rup", "r_hyp", "r_epi", "r_x", "r_y0"])

    SITE_PARAMS = set(["sta_lon", "sta_lat", "vs30", "vs30measured"])

    IDENTIFIERS = set(["ev_id", "gmid", "channel"])

    VALID_STRONG_MOTION_CHANNELS = ["HN", "HG", "LN", "LG", "ALL"]

    VALID_WEAK_MOTION_CHANNELS = ["HH", "EH", "HL", "EL"]

    ESSENTIAL_METADATA = set([])

    def __init__(
            self,
            flatfile: pd.DataFrame,
            name: str,
            reference_rock: Optional[float] = 800.0,
            station_id_type: str = "network-station",
            basin_region: str = "japan",
            ):
        """Instantiate the class with the flatfile

        Args:
            flatfile: The ground motion flatfile as a pandas Dataframe
            name: Name to use as an identifier of the flatfile
            reference_rock: If specified then will default all Vs30 to the reference case
            basin_region: For Z1.0 and Z2.5 defines which conversion equation from Vs30 to use.
                          Set to 'japan' for the Japanese relations of Chiou & Youngs (2014)
                          and Campbell & Bozorgnia (2014), otherwise the California model
                          will be used (default is Japan)
        """

        self.ESSENTIAL_METADATA.update(
            self.IDENTIFIERS, self.SOURCE_PARAMS, self.DISTANCES, self.SITE_PARAMS
            )
        for header in self.ESSENTIAL_METADATA:
            assert header in flatfile.columns,\
                "Required header %s not found in flatfile" % header
        self.flatfile = flatfile
        self.name = name
        self._add_unique_station_ids(station_id_type)
        self.ESSENTIAL_METADATA.update(set(["sta", "M_CAT_ID"]))
        # Create unique ground motion records IDs by concatenating event and station ID
        self.record_ids = ["{:s}|{:s}".format(ev_id, st_id) for ev_id, st_id in
                           zip(self.flatfile["ev_id"], self.flatfile["sta"])]
        self.flatfile["gmid"] = pd.Series(self.record_ids)

        self.event_ids = [evid for evid in self.flatfile.ev_id.astype(str).unique()]
        self.event_ids.sort()
        self.station_ids = [str(sta) for sta in self.flatfile.sta.astype(str).unique()]
        self.station_ids.sort()
        self.imts = []
        self.imt_keys = []
        for col in self.flatfile.columns:
            if col in ["PGA", "PGV"]:
                self.imts.append(from_string(col))
                self.imt_keys.append(col)
            elif col.startswith("pSA_"):
                self.imts.append(SA(float(col.replace("pSA_", ""))))
                self.imt_keys.append(col)
            else:
                pass
        self.reference_rock = reference_rock
        self.basin_region = basin_region
        self._station_database = None
        self._event_catalogue = None

    @property
    def station_database(self):
        """Returns a dataframe containng just the station information (not sorted by channel)
        """
        if self._station_database is not None:
            return self._station_database
        # Select attributes associated with the stations
        stations = self.flatfile[["sta", "station", "network", "sta_lon", "sta_lat",
                                  "vs30", "vs30measured", "Z1.0", "Z2.5"]]
        # Create a unique code based just on network and station code
        stations["sta_code"] = [
            "{:s}-{:s}".format(network, station)
            for network, station in zip(stations["network"], stations["station"])
            ]
        # Count the number of observations per stations
        vcounts = stations.value_counts("sta_code")
        # Reduce dataframe to one entry per station
        stations.drop_duplicates("sta_code", inplace=True, ignore_index=True, )
        stations.set_index("sta_code", drop=True, inplace=True)
        # Associate the counts
        N = pd.Series(np.zeros(stations.shape[0]), index=stations.index)
        for idx in vcounts.index:
            N.loc[idx] = vcounts[idx]
        stations["N"] = N.astype(int)
        self._station_database = stations.copy()
        return self._station_database

    @property
    def event_catalogue(self):
        """Returns a simple catalogue of events from the flatfile
        """
        if self._event_catalogue is not None:
            return self._event_catalogue
        # Select attributes associated with the events
        events = self.flatfile[
            ["ev_id", "ev_time", "ev_lon", "ev_lat", "ev_depth",
             "mag", "mag_type", "strike", "dip", "rake", "z_tor", "z_bor",
             "f_width", "f_length"]
            ]
        vcounts = events.value_counts("ev_id")
        events.drop_duplicates("ev_id", inplace=True, ignore_index=True)
        events.set_index("ev_id", drop=True, inplace=True)
        # Add the counts
        nsta = pd.Series(np.zeros(events.shape[0]), index=events.index)
        for idx in vcounts.index:
            nsta.loc[idx] = vcounts[idx]
        events["NOBS"] = nsta.astype(int)
        self._event_catalogue = events.copy()
        return self._event_catalogue

    def _add_unique_station_ids(self, station_id_type: str):
        """Creates a unique station ID joining the station name with one or both of the network
        or channel. This allows the user to control whether to include the channel in the
        unique station key, thereby splitting weak and strong motion channels.

        Args:
            station_id_type: Choice of "network-station" or "network-station-channel"
        """
        assert station_id_type in ("network-station", "network-station-channel"),\
            "'station_id_type' must be one of 'network-station','network-station-channel'"

        station_ids = []
        for i in range(self.flatfile.shape[0]):
            network = self.flatfile["network"].iloc[i]
            station = self.flatfile["station"].iloc[i]
            if "channel" in station_id_type:
                channel = self.flatfile["channel"].iloc[i]
            else:
                channel = "ALL"
            station_ids.append("{:s}-{:s}-{:s}".format(network, station, channel))
        self.flatfile["sta"] = pd.Series(station_ids)
        return

    @classmethod
    def build_from_original_file(cls, filename: str, sep=",", **kwargs):
        """Builds the flatfile dataframe from the original input file. This function must
        be over-ridden in the sub-classes and adapted to the specific structure of the flatfile

        Args:
            filename: The path to the input flatfile (.csv)
            sep: Column separator in original flatfile
        """
        raise NotImplementedError

    def merge_harmonised_catalogue(
            self, harmonised_catalogue: pd.DataFrame, time_window: int, distance_window: int,
            print_match_types: Tuple = (1, 2, 3)
            ):
        """Merge a harmonised earthquake catalogue into the flatfile in order
        to define a harmonised magnitude set.

        Args:
            harmonised_catalogue: Magnitude-harmonised earthquake, which must contain the
                                  columns ["ev_id", "ev_time", "lon", "lat", "depth", "M"],
                                  and "ev_time" must be a numpy datetime64 object
            time_window: Maximum tolerated time difference (in seconds) for two associated
                         events to be identified as a common event
            distance_window: Maximum distance difference (in km) between the event in the
                             harmonised catalogue and its representation in the flatfile
            print_match_types: Different types of matches are defined (see function string
                               for `low_level_catalogue_harmoniser`), so this chooses which
                               are printed.
        """
        flatfile_catalogue = self.flatfile_to_catalogue(include_magnitudes=False)
        idxs, mismatches = low_level_catalogue_harmoniser(
            flatfile_catalogue,
            harmonised_catalogue,
            time_win=time_window,
            dist_win=distance_window
        )
        self.flatfile = add_harmonised_magnitudes_to_flatfile(
            self.flatfile, harmonised_catalogue, idxs)
        return

    def __len__(self):
        return self.flatfile.shape[0]

    @property
    def num_events(self):
        """Return the number of events"""
        return len(self.event_ids)

    @property
    def num_stations(self):
        """Return the number of stations"""
        return len(self.station_ids)

    def setup_gmvs(
            self,
            filters: Optional[Dict] = None,
            use_reference_rock: bool = False,
            split_strong_weak_motion: bool = False,
            ) -> Union[Dict, Tuple[Dict, Dict]]:
        """Retrieves the source, path and site information for each event and transforms
        them into Event objects, with attributes sorted into an OpenQuake context object.

        A set of filters can be applied to the flatfile when creating the OpenQuake contexts,
        which must be specified in the form of a dictionary, e.g.::

            filters = {"param1": ("range", value_low, value_high),
                       "param2": ("equality", value)}

        where "param#" is the flatfile column for the given parameter, "range" and "equality"
        refer to filter types, and the value refer to the exact value to be filtered to (for
        the "range" filter type) or the upper and lower values of the "range"

        Args:
            filters: Dictionary describing the set of filters to be applied to the data before
                     rendering to a set of contexts
            use_reference_rock: If 'True' then sets all site Vs30 to the reference rock
                                (as set in instantiation), otherwise will use the site Vs30.
                                If using reference rock then Z1.0 and Z2.5 will be over-written
                                to corresponding values using conversion equations
            split_strong_weak_motion: If 'True' then it will return two sets of ground motion
                                      values, one for strong motion and one for weak motion,
                                      otherwise it will combine all of them into one ground
                                      motion set
        Returns:
            gmdb: A dictionary containing formatted metadata and context objects for the whole
                  ground motion database

        """
        data = self.flatfile.copy()
        # Apply the filters
        if filters is not None:
            for key, filter_value in filters.items():
                if filter_value[0] == "equality":
                    idx = ~pd.isna(data[key]) & (data[key] == filter_value)
                else:
                    xlow, xhigh = filter_value[1:]
                    idx = ~pd.isna(data[key]) & (data[key] >= xlow) & (data[key] <= xhigh)
                data = data[idx]
        if split_strong_weak_motion:
            # Split the strong and weak motion records into two separate ground motion
            # data sets
            data_strong = data[data["channel"].isin(self.VALID_STRONG_MOTION_CHANNELS)]
            data_weak = data[data["channel"].isin(self.VALID_WEAK_MOTION_CHANNELS)]
            if not data_strong.shape[0] or not data_strong.shape[0]:
                raise ValueError("Cannot split data into strong/weak motion - Use All Records"
                                 "Strong Motion has %g records, Weak Motion %g"
                                 % (data_strong.shape[0], data_weak.shape[0]))
            # Get data for the strong motion records
            gmdb_strong = {
                "metadata": data_strong[self.ESSENTIAL_METADATA],
                "nrec": data_strong.shape[0],
                "ctx": self._get_rupture_context(data_strong),
                "gmvs": data_strong[self.imt_keys],
                "imts": self.imts,
            }
            gmdb_strong["ctx"] = self._get_sites_context(data_strong,
                                                         gmdb_strong["ctx"],
                                                         use_reference_rock)
            gmdb_strong["ctx"] = self._get_distances_context(data_strong, gmdb_strong["ctx"])
            # Get data for the weak motion records
            gmdb_weak = {
                "metadata": data_weak[self.ESSENTIAL_METADATA],
                "nrec": data_weak.shape[0],
                "ctx": self._get_rupture_context(data_weak),
                "gmvs": data_weak[self.imt_keys],
                "imts": self.imts,
            }
            gmdb_weak["ctx"] = self._get_sites_context(data_weak,
                                                       gmdb_weak["ctx"],
                                                       use_reference_rock)
            gmdb_weak["ctx"] = self._get_distances_context(data_weak, gmdb_weak["ctx"])
            return gmdb_strong, gmdb_weak
        else:
            # Return all records in one data set
            gmdb = {
                "metadata": data[self.ESSENTIAL_METADATA],
                "nrec": data.shape[0],
                "ctx": self._get_rupture_context(data),
                "gmvs": data[self.imt_keys],
                "imts": self.imts,
            }
            gmdb["ctx"] = self._get_sites_context(data, gmdb["ctx"], use_reference_rock)
            gmdb["ctx"] = self._get_distances_context(data, gmdb["ctx"])
            return gmdb

    def to_shapefile(self, filename, geometry_type: str, reduce: bool = False,
                     driver: Optional[str] = None):
        """Exports the flatfile to a shapefile (through a GeoPandas GeoDataFrame), allowing the
        user to assign to the geometry object either the event epicentre, the station locations
        or a line connecting events to the stations

        Args:
            filename: path to export shapefile
            geometry_type: Which information to use as geometry. Set "source" to use the
                           earthquake epicentres, "site" to use the recording stations, and
                           "path" to define geometry as lines connecting the epicentres to the
                           recording stations
            reduce: For the "source" and "site" options, if 'True' this will reduce the
                    shapefile to just one entry per event or per station respectively
            driver: OGR format driver to write to vector shapefile. If not set then will
                    default to ESRI Shapefile (saved in a folder)
        """
        assert geometry_type in ("source", "path", "site"),\
            "Geometry type %s not recognised (must be either 'source', 'path' or 'site')"\
            % geometry_type
        if geometry_type == "source":
            if reduce:
                output_flatfile = self.flatfile.drop_duplicates("ev_id", inplace=False,
                                                                ignore_index=True)
            else:
                output_flatfile = self.flatfile.copy()
            geometries = gpd.points_from_xy(output_flatfile["ev_lon"].to_numpy(),
                                            output_flatfile["ev_lat"].to_numpy())
        elif geometry_type == "site":
            if reduce:
                output_flatfile = self.flatfile.drop_duplicates("sta", inplace=False,
                                                                ignore_index=True)
            else:
                output_flatfile = self.flatfile.copy()
            geometries = gpd.points_from_xy(output_flatfile["sta_lon"].to_numpy(),
                                            output_flatfile["sta_lat"].to_numpy())
        elif geometry_type == "path":
            geometries = []
            output_flatfile = self.flatfile.copy()
            for i, row in self.flatfile.iterrows():
                geometries.append(geometry.LineString([[row.ev_lon, row.ev_lat],
                                                       [row.sta_lon, row.sta_lat]]))
            geometries = gpd.GeoSeries(geometries)
        gdf = gpd.GeoDataFrame(output_flatfile, geometry=geometries, crs="EPSG:4326")
        gdf.to_file(filename, index=False, driver=driver)
        return

    def _get_rupture_context(self, event_data: pd.DataFrame) -> RuptureContext:
        """Extracts the rupture properties from the event dataframe and returns them as an
        openquake.hazardlib.gsim.base.RuptureContext object
        """
        ctx = RuptureContext()
        ctx.sids = np.arange(0, event_data.shape[0])
        ctx.mag = event_data["mag"].to_numpy()
        ctx.rake = event_data["rake"].to_numpy()
        ctx.dip = event_data["dip"].to_numpy()
        ctx.ztor = event_data["z_tor"].to_numpy()
        ctx.zbor = event_data["z_bor"].to_numpy()
        ctx.hypo_depth = event_data["ev_depth"].to_numpy()
        ctx.width = event_data["f_width"].to_numpy()
        ctx.length = event_data['f_length'].to_numpy()
        ctx.hypo_lon = event_data["ev_lon"].to_numpy()
        ctx.hypo_lat = event_data["ev_lat"].to_numpy()
        return ctx

    def _get_sites_context(
            self,
            event_data: pd.DataFrame,
            ctx: RuptureContext,
            use_reference_rock: bool,
            ) -> RuptureContext:
        """Extracts the site properties from the event dataframe and adds them to the
        RuptureContext
        """
        # Get site data
        if use_reference_rock:
            nrecs = event_data["vs30"].shape[0]
            ctx.vs30 = self.reference_rock * np.ones(nrecs)
            ctx.vs30measured = np.ones(nrecs, dtype=bool)
            ctx.z1pt0 = vs30_to_z1pt0_cy14(ctx.vs30, self.basin_region)
            ctx.z2pt5 = vs30_to_z2pt5_cb14(ctx.vs30, self.basin_region)
        else:
            ctx.vs30 = event_data['vs30'].to_numpy()
            ctx.vs30measured = event_data["vs30measured"].to_numpy()
            ctx.z1pt0 = event_data["Z1.0"].to_numpy().astype(float)
            ctx.z2pt5 = event_data["Z2.5"].to_numpy().astype(float)
        ctx.lon = event_data["sta_lon"].to_numpy()
        ctx.lat = event_data["sta_lat"].to_numpy()
        ctx.region = np.zeros(ctx.vs30.shape, dtype=int)
        if "slope" in event_data.columns:
            ctx.slope = event_data["slope"].to_numpy()
        if "geology" in event_data.columns:
            ctx.geology = event_data["geology"].to_numpy()
        return ctx

    def _get_distances_context(
            self,
            event_data: pd.DataFrame,
            ctx: RuptureContext
            ) -> RuptureContext:
        """Extracts the distances from the event dataframe and adds them to the RuptureContext
        """
        # Get distance data
        ctx.rrup = event_data['r_rup'].to_numpy()
        ctx.rjb = event_data['r_jb'].to_numpy()
        ctx.rhypo = event_data['r_hyp'].to_numpy()
        ctx.repi = event_data['r_epi'].to_numpy()
        ctx.rx = event_data['r_x'].to_numpy()
        ctx.ry0 = event_data['r_y0'].to_numpy()

        # These parameters not implemented in flatfile
        ctx.rcdpp = np.zeros_like(ctx.rrup)
        ctx.azimuth = np.zeros_like(ctx.rrup)
        return ctx

    def flatfile_to_catalogue(self,
                              start_time: Optional[str] = None,
                              end_time: Optional[str] = None,
                              is_master: bool = False,
                              master_id_stem: str = "MASTER_IDX",
                              include_magnitudes: bool = False) -> pd.DataFrame:
        """Creates an earthquake catalogue from the flatfile.

        Args:
            flatfile_identifier: String to identify the flatfile
            start_time: Start time to filter the catalogue
            end_time: End time to filter the catalogue
            is_master: The catalogue is the "master catalogue", meaning that
                       it will be the reference ID for the merging
            master_id_stem: String stem to create a unique index-based ID for
                            each earthquake
            include_magnitudes: Includes magnitude columns (True) or not (False)

        Returns:
            db_events: Flatfile earthquakes as a catalogue in the form of a
                       pandas DataFrame
        """
        db_events = self.flatfile.drop_duplicates("ev_id", inplace=False)
        start_time = np.datetime64(start_time) if start_time else\
            np.min(self.flatfile["ev_time"])
        end_time = np.datetime64(end_time) if end_time else\
            np.max(self.flatfile["ev_time"])
        db_events = db_events[(db_events["ev_time"] >= start_time) &
                              (db_events["ev_time"] <= end_time)]
        db_events.sort_values("ev_time", inplace=True)
        # Generate the merge identified index
        if is_master:
            master_ids = ["{:s}_{:s}".format(master_id_stem, str(i).zfill(6))
                          for i in range(db_events.shape[0])]
            db_out = {"master_id": master_ids}
        else:
            db_out = {}

        db_out["ev_id"] = db_events["ev_id"]
        db_out["ev_time"] = db_events["ev_time"]
        db_out["lon"] = db_events["ev_lon"]
        db_out["lat"] = db_events["ev_lat"]
        db_out["depth"] = db_events["ev_depth"]
        if include_magnitudes:
            db_out["M"] = db_events["mag_orig"]
            db_out["sigma_m"] = np.nan * np.ones(db_events.shape[0])
            db_out["mtype"] = [None] * db_events.shape[0]
        else:
            db_out["M"] = np.nan * np.ones(db_events.shape[0])
            db_out["sigma_m"] = np.nan * np.ones(db_events.shape[0])
            db_out["mtype"] = [None] * db_events.shape[0]

        db_out = pd.DataFrame(db_out)
        db_out.reset_index(inplace=True, drop=True)
        return db_out

    def cleanup(self, reference_vs30: Optional[float] = None,
                reference_z1pt0: Optional[float] = None,
                reference_z2pt5: Optional[float] = None,
                basin_region: Optional[str] = None):
        """Cleans up the flatfile to put in valid values into the essential columns
        based on simple assumptions about the source, combined with available data

        Args:
            flatfile: Ground motion database flatfile as dataframe
        """
        if "mag" not in self.flatfile.columns or np.all(self.flatfile["mag"].isna()):
            raise ValueError("No magnitude values in 'mag' column or missing 'mag' column. "
                             "Run catalogue harmonisor and/or re-check the data!")
        valid_idx = pd.Series(np.zeros(self.flatfile.shape[0], dtype=bool),
                              index=self.flatfile.index.copy())
        nrecs = self.flatfile.shape[0]
        neqs = self.num_events
        nsta = self.num_stations
        events = self.flatfile.groupby("ev_id")
        if not reference_vs30:
            reference_vs30 = self.reference_rock
        if not basin_region:
            basin_region = self.basin_region

        for ev_id, event in events:
            # Minimum required is longitude, latitude and magnitude
            fail1 = False
            for key in ["ev_lon", "ev_lat", "mag"]:
                if np.isnan(event[key].iloc[0]):
                    fail1 = True
                    break
            if fail1:
                logging.info("Event %s lacks valid location and/or magnitude" % ev_id)
                continue
            scenario = ScenarioProperties(
                event["ev_lon"].iloc[0],
                event["ev_lat"].iloc[0],
                event["ev_depth"].iloc[0] if not np.isnan(event["ev_depth"].iloc[0]) else 10.0,
                event["mag"].iloc[0],
                site_lon=event["sta_lon"].to_numpy(),
                site_lat=event["sta_lat"].to_numpy(),
                site_vs30=event["vs30"].to_numpy(),
                site_z1pt0=event["Z1.0"].to_numpy().astype(float),
                site_z2pt5=event["Z2.5"].to_numpy().astype(float),
                strike=event["strike"].iloc[0] if not np.isnan(
                    event["strike"].iloc[0]) else None,
                dip=event["dip"].iloc[0] if not np.isnan(event["dip"].iloc[0]) else None,
                rake=event["rake"].iloc[0] if not np.isnan(event["rake"].iloc[0]) else None,
                reference_vs30=reference_vs30,
                reference_z1pt0=reference_z1pt0,
                reference_z2pt5=reference_z2pt5,
                basin_region=basin_region,
                vs30measured=event["vs30measured"].to_numpy() if not np.isnan(
                    event["vs30measured"].iloc[0]) else None,
            )
            # Get the required source, path and site attributes
            keys = [
                ("r_rup", "rrup"),
                ("r_jb", "rjb"),
                ("r_x", "rx"),
                ("r_y0", "ry0"),
                ("r_hyp", "rhypo"),
                ("r_epi", "repi"),
                ("dip", "dip"),
                ("rake", "rake"),
                ("f_width", "width"),
                ("f_length", "length"),
                ("z_tor", "ztor"),
                ("z_bor", "zbor"),
                ("vs30", "vs30"),
                ("vs30measured", "vs30measured"),
                ("Z1.0", "z1pt0"),
                ("Z2.5", "z2pt5"),
            ]
            for key1, key2 in keys:
                vals = event[key1]
                idx = pd.isna(event[key1])
                vals[idx] = getattr(scenario, key2)[idx.to_numpy()]
                self.flatfile[key1].loc[event.index] = vals
            valid_idx.loc[event.index] = True
        # Update the flatfile to just the valid records
        self.flatfile = self.flatfile[valid_idx]
        # Re-count event, station and record IDs
        self.record_ids = [str(rec_id) for rec_id in self.flatfile["gmid"]]
        self.event_ids = [evid for evid in self.flatfile["ev_id"].unique()]
        self.event_ids.sort()
        self.station_ids = [str(sta) for sta in self.flatfile["sta"].unique()]
        self.station_ids.sort()
        nrecs_new = self.flatfile.shape[0]
        logging.info("Clean up leaves %g / %g records (%g / %g events, %g / %g stations)"
                     % (nrecs_new, nrecs, self.num_events, neqs, self.num_stations, nsta))
        return


AS_RECORDED_COMPONENTS = {
    "geometric": lambda u, v: np.sqrt(u * v),
    "envelope": lambda u, v: np.max(np.column_stack([u, v]), axis=1),
    "random": lambda u, v: [u, v][randint(0, 1)],
}


def _extract_sa_columns_rotd(input_flatfile: pd.DataFrame,
                             output_flatfile: Dict,
                             gm_comp: str,
                             exclude_imts: Set) -> Dict:
    """Extracts the columns related to rotDPP intensity measures.

    Args:
        input_flatfile: The raw flatfile input as dataframe
        output_flatfile: The flatfile (as a dictionary) in the process of being constructed
        gm_comp: String to indicate the stem of the selected horizontal GM components,
                     which should start with "rotD", e.g. "rotD50", "rotD100", etc.

    Returns:
        output_flatfile with additional columns to indicate the horizontal ground motion
        as each period
    """
    assert gm_comp.startswith("rotD")
    for key in input_flatfile.columns:
        if gm_comp in key:
            input_col = input_flatfile[key]
            imt = key.replace(gm_comp + "_", "").upper()
            imt = imt.replace("_", ".")
            if "PGA" in imt:
                output_flatfile[imt] = input_col / (100.0 * g)
            elif "PGV" in imt:
                output_flatfile[imt] = input_col
            elif imt in exclude_imts:
                continue
            else:
                # Is a spectral acceleration
                output_flatfile["pSA_{:s}".format(imt[1:])] = input_col / (100.0 * g)
        else:
            pass
    return output_flatfile


def _extract_sa_columns_as_recorded(input_flatfile: pd.DataFrame,
                                    output_flatfile: Dict,
                                    gm_comp: str,
                                    exclude_imts: Set) -> Dict:
    """Extracts columns to get as-recorded intensity measures

    Args:
        input_flatfile: The raw flatfile input as dataframe
        output_flatfile: The flatfile (as a dictionary) in the process of being constructed
        gm_comp: String to indicate the type of horizontal component from ["geometric",
                     "envelope", "random"]

    Returns:
        output_flatfile with additional columns to indicate the horizontal ground motion
        as each period
    """
    ucols = []
    vcols = []
    for key in input_flatfile.columns:
        if key.startsith("U_") and (key.replace("U_", "") not in exclude_imts):
            # Valid NS component
            ucols.append(key)
        elif key.startsith("V_") and (key.replace("V_", "") not in exclude_imts):
            vcols.append(key)
        else:
            continue
    assert len(ucols) == len(vcols),\
        "Number of valid U- and V- IMT columns does not match (%g, %g)" % (len(ucols),
                                                                           len(vcols))
    for ucol, vcol in zip(ucols, vcols):
        imt_u = ucol.replace("U_T", "").replace("_", ".")
        imt_v = ucol.replace("V_T", "").replace("_", ".")
        assert imt_u == imt_v
        if imt_u == "PGV":
            adj = 1.0
        else:
            # Convert accelerations from cm/s/s to g
            adj = 1.0 / (100.0 * g)
        uvals = input_flatfile[ucol] * adj
        vvals = input_flatfile[vcol] * adj
        if imt_u in ("PGA", "PGV"):
            output_flatfile[imt_u] = AS_RECORDED_COMPONENTS[gm_comp](uvals, vvals)
        else:
            output_flatfile["pSA_{:s}".format(imt_u)] =\
                AS_RECORDED_COMPONENTS[gm_comp](uvals, vvals)
    return output_flatfile


def itaca_flatfile_to_input(
        input_flatfile: pd.DataFrame,
        gm_comp: str = "rotD50",
        exclude_imts: Set = set(["PGD", "T90", "HOUSNER", "CAV", "IA"])) -> pd.DataFrame:
    """Transforms the ITACA flatfile from its raw form into a commonly formatted dataframe
    that can be used for analysis

    Args:
        input_flatfile: Input flatfile as a dataframe, usually loaded by
                        pd.read_csv("path/to/itaca_flatfile.csv", sep=";")
        gm_comp: Horizontal component to use for the spectrum, from ["rotD##", "geometric",
                 "envelope", "random"], where "rotD##" can be "rotD50" or "rotD100"
    Returns:
        output_flatfile: The ITACA database is returned as a re-formatted flatfile
    """
    nobs = input_flatfile.shape[0]

    # Generate rake from SOF
    fm_rake = pd.Series(np.nan + np.zeros(nobs))
    fm_rake[input_flatfile["fm_type_code"] == "TF"] = 90.0
    fm_rake[input_flatfile["fm_type_code"] == "NF"] = -90.0
    fm_rake[input_flatfile["fm_type_code"] == "SS"] = 0.0
    idx = ~pd.isna(input_flatfile["es_rake"])
    fm_rake[idx] = input_flatfile["es_rake"][idx]
    # Build initial dictionary
    output_flatfile = {
        "ev_id": input_flatfile["event_id"].astype(str),
        "sta": None,
        "gmid": None,
        "tect_type": pd.Series([pd.NA] * nobs),
        "ev_time": input_flatfile["event_time"].astype(np.datetime64),
        "ev_lon": input_flatfile["ev_longitude"],
        "ev_lat": input_flatfile["ev_latitude"],
        "ev_depth": input_flatfile["ev_depth_km"],
        "mag_orig": input_flatfile["Mw"],
        "mag_ref_orig": input_flatfile["Mw_ref"],
        "rake": fm_rake,
        "dip": input_flatfile["es_dip"],
        "strike": input_flatfile["es_strike"],
        "z_tor": input_flatfile["es_z_top"],
        "z_bor": input_flatfile["es_z_bottom"],
        "f_width": input_flatfile["es_width"],
        "f_length": input_flatfile["es_length"],
        # Distances
        "r_jb": input_flatfile["JB_dist"],
        "r_rup": input_flatfile["rup_dist"],
        "r_hyp": np.sqrt(input_flatfile["epi_dist"] ** 2.0 +
                         input_flatfile["ev_depth_km"] ** 2.0),
        "r_epi": input_flatfile["epi_dist"],
        "r_x": input_flatfile["Rx_dist"],
        "r_y0": input_flatfile["Ry0_dist"],
        "azimuth": input_flatfile["epi_az"],
        # Site Properties
        "network": input_flatfile["network_code"],
        "station": input_flatfile["station_code"],
        "sta_lon": input_flatfile["st_longitude"],
        "sta_lat": input_flatfile["st_latitude"],
        "elevation": input_flatfile["st_elevation"],
        "channel": input_flatfile["instrument_code"],
        "sensor_depth": input_flatfile["sensor_depth_m"],
        "vs30": None,
        "vs30measured": None,
        "Z1.0": None,
        "Z2.5": None,
    }

    output_flatfile["rake"] = fm_rake
    # Get the vs30
    vs30_all = []
    vs30_measured = []
    for vs_obs, vs_inf in zip(input_flatfile["vs30_m_s_from_vs_profile"],
                              input_flatfile["vs30_m_s_from_topography"]):
        if not pd.isna(vs_obs):
            # Measured Vs30
            vs30_all.append(vs_obs)
            vs30_measured.append(True)
        elif not pd.isna(vs_inf):
            # Inferred Vs30
            vs30_all.append(vs_inf)
            vs30_measured.append(False)
        else:
            vs30_all.append(np.nan)
            vs30_measured.append(False)
    output_flatfile["vs30"] = pd.Series(vs30_all)
    output_flatfile["vs30measured"] = pd.Series(vs30_measured)

    # Get the SA columns
    if gm_comp in ("rotD50", "rotD100"):
        output_flatfile = _extract_sa_columns_rotd(input_flatfile,
                                                   output_flatfile,
                                                   gm_comp,
                                                   exclude_imts)
    elif gm_comp in ("geometric", "envelope", "random"):
        output_flatfile = _extract_sa_columns_as_recorded(input_flatfile,
                                                          output_flatfile,
                                                          gm_comp,
                                                          exclude_imts)

    return pd.DataFrame(output_flatfile)


class ITACAFlatfile(FlatfileFormatter):
    """Object to handle ground motion flatfiles based on the ITACA data set and
    flatfile

    The ITACA flatfile is available for download from:
    https://itaca.mi.ingv.it/ItacaNet_32/#/products/itacaext_flatfile
    """
    # Flatfile includes IMTs rarely used for residual analysis. List of IMTs
    # to not parse
    EXCLUDE_IMTS = set(["PGD", "T90", "HOUSNER", "CAV", "IA"])

    @classmethod
    def build_from_original_file(cls, filename: str, sep: str = ";",
                                 name: str = "ITACA",
                                 reference_rock: float = 800.0,
                                 station_id_type: str = "network-station",
                                 **kwargs):
        """Setup the flatfile object from the original ITACA file

        Args:
            filename: The path to the input flatfile (.csv)
            sep: Column separator in original flatfile
            reference_rock: Vs30 value to use to define the reference rock
        """
        gm_comp = kwargs.get("gm_comp", "rotD50")
        basin_region = kwargs.get("basin_region", "japan")
        flatfile = pd.read_csv(filename, sep=sep)
        flatfile = itaca_flatfile_to_input(flatfile, gm_comp)
        flatfile["mag"] = np.nan + np.zeros(flatfile.shape[0])
        data = cls(flatfile, name,
                   reference_rock=reference_rock,
                   station_id_type=station_id_type,
                   basin_region=basin_region)

        # Log which necessary headers are missing in the columns
        for i_d in data.IDENTIFIERS:
            if i_d not in flatfile.columns:
                raise ValueError("Essential ID column %s not found in flatfile" % i_d)
        for src in data.SOURCE_PARAMS:
            if src not in flatfile.columns:
                raise ValueError("Essential source parameter column %s not found in flatfile"
                                 % src)
        for dst in data.DISTANCES:
            if dst not in flatfile.columns:
                raise ValueError("Essential distance parameter %s not found in flatfile" % dst)
        for site in data.SITE_PARAMS:
            if site not in flatfile.columns:
                raise ValueError("Essential site parameter %s not found in flatfile" % site)
        return data
