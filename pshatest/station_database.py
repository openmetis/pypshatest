"""
File to handle complete database of observed ground motions for stations across
Europe
"""
import os
import h5py
import logging
import json
from math import ceil, sqrt
from datetime import date, time, datetime
from typing import Optional, List, Dict, Union, Tuple
import numpy as np
from scipy.interpolate import interp1d
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from shapely.geometry import Polygon, Point
from openquake.baselib.node import Node
from openquake.hazardlib.stats import mean_curve, quantile_curve, norm_cdf
from openquake.hazardlib import nrml
from openquake.hazardlib.geo import geodetic
from openquake.hazardlib.gsim import get_available_gsims
from openquake.hazardlib.imt import PGA, SA, from_string
from openquake.hazardlib.gsim.base import CoeffsTable
from openquake.hazardlib.site import site_param_dt
import pshatest.scenario_validator as sv
from pshatest.utils import (
    best_subplot_dimensions, dbe_dframe_to_numpy, dbe_numpy_to_dframe,
    vs30_to_z1pt0_cy14, vs30_to_z2pt5_cb14)
from pshatest.openquake_hazard_manager import (
    extract_information_from_openquake_datastore,
    extract_information_from_openquake_ses_calculation,
    SITE_DTYPES
)


logging.basicConfig(level=logging.INFO)


STRONG_MOTION_CHANNELS = ["HG", "HN"]
WEAK_MOTION_CHANNELS = ["HH", "EH"]
DATE_GROUPER = {"W": ("Week", 7), "M": ("Month", 31), "Y": ("Year", 365)}


dateaxis_formatter = {
    (0, 1): (mdates.MonthLocator(interval=1), mdates.DateFormatter("%d/%m/%Y")),
    (1, 3): (mdates.MonthLocator(interval=3), mdates.DateFormatter("%m/%Y")),
    (3, 10): (mdates.MonthLocator(interval=6), mdates.DateFormatter("%m/%Y")),
    (10, 20): (mdates.YearLocator(), mdates.DateFormatter("%Y")),
    (20, 10000): (mdates.YearLocator(base=5), mdates.DateFormatter("%Y")),
}

CHANNEL_PALETTE = {
    "HN": "tab:blue",
    "HH": "tab:red",
    "HG": "tab:orange",
    "EH": "tab:green",
    }


# European phi_s2s model from ESHM2020
PHI_S2S = CoeffsTable(sa_damping=5, table="""\
    imt        phi_s2s_obs
    pgv         0.36257068
    pga         0.38806156
    0.010       0.40044760
    0.025       0.40623719
    0.040       0.41977221
    0.050       0.43465421
    0.070       0.44921838
    0.100       0.46432610
    0.150       0.47703588
    0.200       0.48025344
    0.250       0.46891833
    0.300       0.44983953
    0.350       0.43569377
    0.400       0.43045602
    0.450       0.43223316
    0.500       0.43887979
    0.600       0.44724118
    0.700       0.45268279
    0.750       0.45583313
    0.800       0.46384687
    0.900       0.47448247
    1.000       0.48134887
    1.200       0.48708350
    1.400       0.49596280
    1.600       0.50237219
    1.800       0.49599967
    2.000       0.47661567
    2.500       0.44991701
    3.000       0.42220113
    3.500       0.39951709
    4.000       0.38303088
    4.500       0.36840706
    5.000       0.35254196
    6.000       0.33854229
    7.000       0.33074643
    8.000       0.32874669
    """)


def get_station_channel_histories(db: h5py.File,
                                  station_history_dir: str,
                                  stem: str = "Station_History"):
    """
    Retrieves the daily time series for each channel
    Args:
        db: Database object
        station_history_dir: Path to directory with station histories
        stem: Filestem for each station history file
    """
    for stn_key in db["stations"]:
        logging.info("Processing daily history for station %s" % stn_key)
        fname = os.path.join(
            station_history_dir,
            "{:s}_{:s}.csv.zip".format(stem, stn_key.replace("-", "_"))
        )
        if not os.path.exists(fname):
            logging.info("---- No daily history found for %s" % stn_key)
            continue
        # Read the file
        station_hist = pd.read_csv(fname, sep=",", compression="zip")
        # Get the available channels
        headers = station_hist.columns.to_list()
        headers.remove("date")
        available_channels = []
        for header in headers:
            if header[:2] not in available_channels:
                available_channels.append(header[:2])
        for stn_channel in db["stations/{:s}".format(stn_key)]:
            if stn_channel not in available_channels:
                logging.info("---- No history for channel %s" % stn_channel)
                continue
            stn_channel_dpath = "stations/{:s}/{:s}".format(stn_key, stn_channel)
            # Get the minimum availability from the two channels
            min_avail = station_hist[[stn_channel + "E",
                                      stn_channel + "N"]].min(axis=1).to_numpy()
            channel_hist = np.zeros(
                min_avail.shape[0],
                dtype=[("date", (np.string_, 10)), ("availability", np.float32)]
            )
            channel_hist["date"] = station_hist["date"].to_numpy()
            channel_hist["availability"] = min_avail
            channel_dset = db[stn_channel_dpath].create_dataset(
                "daily_history", channel_hist.shape, channel_hist.dtype)
            channel_dset[:] = channel_hist
            logging.info("---- Channel %s written" % stn_channel)
    return


def get_between_event_residuals(db: h5py.File, gmms: List,
                                strong_motion_residual_file: Optional[str] = None,
                                weak_motion_residual_file: Optional[str] = None):
    """
    Extracts the between event residuals (dbe) from the residual files
    and stores them as numpy arrays
    Args:
        db: Database object (open h5py.File object)
        gmms: List of GMMs
        strong_motion_residual_file: Path to file of strong motion residuals
        weak_motion_residual_file: Path to file of weak motion residuals
    """
    event_grp = db.create_group("dbe")
    if strong_motion_residual_file:
        sm_event_grp = event_grp.create_group("strong_motion")
    if weak_motion_residual_file:
        wm_event_grp = event_grp.create_group("weak_motion")
    for gmm in gmms:
        logging.info("GMM: %s" % gmm)
        # Load in the strong motion dbe
        dbe_str = "RandomEffects/{:s}/between_event".format(gmm)
        if strong_motion_residual_file:
            try:
                dbe = pd.read_hdf(strong_motion_residual_file,
                                  dbe_str)
            except KeyError as err:
                if str(err) == 'No object named {:s} in the file'.format(dbe_str):
                    logging.info("No dbes in strong motion flatfile for %s" % gmm)
                else:
                    raise err
            dbe_array = dbe_dframe_to_numpy(dbe)
            dset_sm = sm_event_grp.create_dataset(gmm, dbe_array.shape,
                                                  dtype=dbe_array.dtype)
            dset_sm[:] = dbe_array
            logging.info("--- added strong motion")
        if weak_motion_residual_file:
            # Now repeat for the weak motion file
            try:
                dbe = pd.read_hdf(weak_motion_residual_file,
                                  dbe_str)
            except KeyError as err:
                if str(err) == 'No object named {:s} in the file'.format(dbe_str):
                    logging.info("No dbes in weak motion flatfile for %s" % gmm)
                else:
                    raise err
            dbe_array = dbe_dframe_to_numpy(dbe)
            dset_wm = wm_event_grp.create_dataset(gmm, dbe_array.shape,
                                                  dtype=dbe_array.dtype)
            dset_wm[:] = dbe_array
            logging.info("--- added weak motion")
    return


def _parse_channel_data(channel_grp: h5py.Group,
                        stn_grp: h5py.Group, fle: h5py.File,
                        sm_idx: pd.DataFrame) -> List:
    """
    Parses the ground motion data and residuals for each given channel
    Args:
        channel_grp: h5py.Group object for the channel
        stn_grp:
        fle: Open hdf5 file containing the residual information
        sm_idx: Index of values for the station
    """
    gmm_list = []
    # Get observations
    # Gets the indices
    idx = sm_idx.loc[stn_grp.index]
    idx_dset = channel_grp.create_dataset("indices", (len(idx),), dtype="i")
    idx_dset[:] = idx.index.to_numpy()
    locs = idx["idx"].to_numpy()
    obs_vals = fle["Observations"][locs, :]
    obs_dset = channel_grp.create_dataset("acceleration",
                                          obs_vals.shape, dtype="f")
    obs_dset[:] = obs_vals
    pred_grp = channel_grp.create_group("predictions")
    # Get model predictions
    for gmm in fle["Predictions"]:
        gmm_grp = pred_grp.create_group(gmm)
        if gmm not in gmm_list:
            gmm_list.append(gmm)
        for key in ["Mean", "between_event_stddev",
                    "within_event_stddev", "total_stddev"]:
            vals = fle["Predictions/{:s}/{:s}".format(gmm, key)][:, 0, locs].T
            dset = gmm_grp.create_dataset(key, vals.shape, dtype="f")
            dset[:] = vals
    return gmm_list


def build_station_geodataframe(db: h5py.File, threshold: float = 90.0) -> gpd.GeoDataFrame:
    """Returns basic station data in the form of a geopandas geodataframe

    Args:
        db: Database in the form of an h5py.File object
        threshold: Threshold of operation for a day in the station history to be considered as
                   an operational day

    Returns:
        Station database in the form of a geopandas GeoDataFrame
    """
    station_data = {"sids": [], "lons": [], "lats": [], "N": [], "net": [],
                    "name": [], "vs30": [], "vs30measured": [],
                    "start_eida": [], "end_eida": [], "ndays": [], "HN": [],
                    "HG": [], "HH": [], "EH": []}
    station_ids = []
    for key in list(db["stations"]):
        station_ids.append(key)
        key_str = "stations/{:s}".format(key)
        st_net, st_name = key.split("-")
        station_data["lons"].append(db[key_str].attrs["longitude"])
        station_data["lats"].append(db[key_str].attrs["latitude"])
        station_data["net"].append(st_net)
        station_data["name"].append(st_name)
        station_data["N"].append(db[key_str].attrs["nrec"])
        station_data["vs30"].append(db[key_str].attrs["vs30"])
        station_data["vs30measured"].append(db[key_str].attrs["vs30measured"])
        station_data["start_eida"].append(db[key_str].attrs["start_eida"])
        station_data["end_eida"].append(db[key_str].attrs["end_eida"])
        station_data["sids"].append(db[key_str].attrs["unique_integer_id"])
        available_channels = list(db[key_str])
        station_hists = []
        for chan in ["HN", "HG", "HH", "EH"]:
            has_chan = chan in available_channels
            station_data[chan].append(has_chan)
            if not has_chan:
                continue
            if "daily_history" in list(db["stations/{:s}/{:s}".format(key, chan)]):
                daily_hist = db["stations/{:s}/{:s}/daily_history".format(key, chan)][:]
                station_hists.append(daily_hist["availability"])
        if len(station_hists):
            station_hists = np.max(np.column_stack(station_hists), axis=1)
            station_data["ndays"].append(np.sum(station_hists >= threshold))
        else:
            station_data["ndays"].append(np.nan)
    # Get the geometries
    station_data["geometry"] = gpd.points_from_xy(
        station_data["lons"], station_data["lats"], crs="EPSG:4326"
    )
    return gpd.GeoDataFrame(station_data, index=station_ids)


def assign_sites_to_regions(
        sites: gpd.GeoDataFrame,
        regions: gpd.GeoDataFrame,
        cart_crs: str = "EPSG:3035") -> gpd.GeoDataFrame:
    """Assigns the attributes of the polygons specified in regions to their corresponding sites

    Args:
        sites: Geodataframe of site properties (site (lon, lat) as Point geometry, EPSG:4326)
        regions: Geodataframe of regions properties (regions as Polygon geometry, EPSG:4326)
        cart_crs: Cartesian Coordinate reference system for point-in-polygon operation
    """
    sites_cart = sites.to_crs(cart_crs)
    regions_cart = regions.to_crs(cart_crs)
    sites_cart = gpd.sjoin(sites_cart, regions_cart, how="left")
    region_cls = sites_cart["REGION"].to_numpy()
    region_cls[np.isnan(region_cls)] = 0.0
    sites["region"] = sites_cart["REGION"].astype(int)
    return sites


def select_sites_minimim_spacing(sites: Union[pd.DataFrame, gpd.GeoDataFrame],
                                 rmin: float, shuffle: bool = False, verbose: bool = False) \
                                 -> Union[pd.DataFrame, gpd.GeoDataFrame]:
    """
    Selects as many sites from a geodataframe as possible that preserve a minimum distance
    spacing between them.

    Args:
        sites: Input sites as dataframe with columns lons, lats and N (number of obs.)
               with the site name used as an index
        rmin: Minimum spacing between sites (km)
        shuffle: If True then shuffles the sites randomly, otherwise sorts into order
                 of highest number of observations to lowest
        verbose: If true then prints out site-by-site decision on keeping
    Returns:
        selected sites from original geodataframe
    """
    basic_data = sites[["lons", "lats", "N"]]
    if shuffle:
        # Shuffle the sites randomly
        idx = np.arange(0, basic_data.shape[0])
        np.random.shuffle(idx)
        basic_data = basic_data.loc[basic_data.index[idx]]
    else:
        # Start with the site with the most records
        basic_data.sort_values("N", inplace=True, ascending=False)
    stn = [basic_data.index[0]]
    lons = np.array([basic_data.lons.iloc[0]])
    lats = np.array([basic_data.lats.iloc[0]])
    for i in range(1, basic_data.shape[0]):
        dist = geodetic.geodetic_distance(basic_data.lons.iloc[i],
                                          basic_data.lats.iloc[i],
                                          lons, lats)
        if not np.any(dist < rmin):
            # Site not within distance of any other sites
            stn.append(basic_data.index[i])
            lons = np.hstack([lons, basic_data.lons.iloc[i]])
            lats = np.hstack([lats, basic_data.lats.iloc[i]])
            if verbose:
                logging.info("%s (%.4f, %.4f) not within %.1f km of any other sites - adding"
                             % (basic_data.index[i], basic_data.lons.iloc[i],
                                basic_data.lats.iloc[i], rmin))
            continue
        else:
            if verbose:
                min_dist = np.min(dist)
                logging.info("%s (%.4f, %.4f) %.1f km from nearest site - skipping"
                             % (basic_data.index[i], basic_data.lons.iloc[i],
                                basic_data.lats.iloc[i], min_dist))
    return sites.loc[stn]


class StationDatabase():
    """
    Master class for building and interacting with a database of
    acceleration values for stations across the study region including both observations,
    random effects residuals with respect to a specific GMM and hazard
    curves from various PSHA models

    Attributes:
        dbname: Database name (path to the database file
        start_date: Beginning date of database
        end_date: End date of the database
        sm_file: Path to hdf5 file containing residuals for strong motions
        has_strong_motion: Boolean variable indicating that the database has strong motion data
        wm_file: Path to hdf5 file containing residuals for weak motions
        has_weak_motion: Boolean variable indicating that the database has weak motion data
        station_history_directory: Path to the directory containing the
                                   station daily operation histories
        metadata_file: File containing metadata of complete
                                  database used to calculate residuals
        catalogue_file: Path to csv containing earthquake catalogue
        imts: Intensity measures for which accelerations are available
        gmms: List of ground motion models (for residuals and expected motions)
        db: Connection to the database as a h5py.File object (open or closed)
        stations: List of sites/stations in the database
    """
    OQ_SITE_MODEL_ATTRIBUTES = list(site_param_dt) + ["site_name"]

    def __init__(self, dbname: str, start_date: str = "2000-01-01",
                 end_date: str = "2020-12-31"):
        """Instantiate with just the database name
        """
        self.dbname = dbname
        self.start_date = start_date
        self.end_date = end_date
        self.sm_file = None
        self.wm_file = None
        self.station_history_directory = None
        self.metadata_file = None
        self.catalogue_file = None
        self.imts = []
        self.gmms = []
        self.db = None
        self.stations = None
        self._catalogue = None
        self._metadata = None
        self.station_id_map = {}
        self.has_strong_motion = False
        self.has_weak_motion = False
        self._periods = None

    def open(self, mode: str = "r"):
        """Opens the h5py.File of the database in the specified mode
        """
        if not os.path.exists(self.dbname):
            raise OSError("Database %s does not yet exist - try builduing it using .setup()"
                          % self.dbname)
        if self.db is not None:
            raise OSError("Cannot connect to open database")
        self.db = h5py.File(self.dbname, mode)
        self.stations = build_station_geodataframe(self.db)
        self.has_strong_motion = self.db.attrs["has_strong_motion"]
        self.has_weak_motion = self.db.attrs["has_strong_motion"]

    def close(self):
        """Close the h5py.File of the database
        """
        self.db.close()
        self.db = None

    def __len__(self):
        # Number of stations
        if self.db:
            return self.stations.shape[0]
        else:
            return None

    def __repr__(self):
        # Simple string to indicate database information and status
        if self.db:
            return "Station and Event Database with %s stations" % str(len(self))
        else:
            return "Station and Event Database - not connected"

    @property
    def catalogue(self):
        """Returns the earthquake catalogue associated with the station database
        """
        if self._catalogue is None:
            # Catalogue is not defined, need to load from database
            if self.db:
                # Need to close the database, read the catalogue and then
                # reopen
                self.db.close()
                self._catalogue = pd.read_hdf(self.dbname, "catalogue")
                self.db = h5py.File(self.dbname)
            else:
                # Database is closed, so no need to reopen
                self._catalogue = pd.read_hdf(self.dbname, "catalogue")
        else:
            # Return the predefined catalogue
            return self._catalogue

    @property
    def metadata(self):
        """Returns the metadata of the residuals
        """
        if self._metadata is None:
            # Catalogue is not defined, need to load from database
            if self.db:
                # Need to close the database, read the catalogue and then
                # reopen
                self.db.close()
                self._metadata = pd.read_hdf(self.dbname, "metadata")
                self.db = h5py.File(self.dbname)
            else:
                # Database is closed, so no need to reopen
                self._metadata = pd.read_hdf(self.dbname, "metadata")
        else:
            # Return the predefined catalogue
            return self._metadata

    @property
    def periods(self):
        """Returns the numpy array of periods for which the ground motion is defined
        """
        if self._periods is not None:
            return self._periods
        if self.db is None:
            # Database was closed, re-open it and get the periods, then close it
            self.db.open("r")
            self._periods = self.db["periods"][:]
            self.db.close()
        else:
            self._periods = self.db["periods"][:]
        return self._periods

    def setup(
            self,
            strong_motion_residual_file: Optional[str] = None,
            weak_motion_residual_file: Optional[str] = None,
            station_history_directory: Optional[str] = None,
            earthquake_catalogue_file: Optional[str] = None,
            debug_lim: Optional[int] = None
            ):
        """Sets up the database importing data from the strong and weak motion
        residual files

        Args:
            strong_motion_residual_file: Path to strong motion residual hdf5
            weak_motion_residual_file: Path to weak motion residual hdf5
            station_history_directory: Path to directory of daily station histories
            earthquake_catalogue_file: Path to csv file containing earthquake catalogue
        """
        if os.path.exists(self.dbname):
            raise OSError("Cannot run setup on existing database: %s"
                          % self.dbname)

        # Create the database file
        db = h5py.File(self.dbname, "a")

        if strong_motion_residual_file:
            # Parse the strong motion data
            self.sm_file = strong_motion_residual_file
            db.attrs["strong_motion_residual_file"] = self.sm_file
            logging.info("Parsing strong motion data")
            self._parse_station_data(self.sm_file, db, debug_lim)
            self.imts = db.attrs["IMTs"].split(",")
            # Add the metadata
            logging.info("Loading the strong motion metadata")
            metadata = pd.read_hdf(self.sm_file, "flatfile")
            db.attrs["has_strong_motion"] = True
            self.has_strong_motion = True
        if weak_motion_residual_file:
            self.wm_file = weak_motion_residual_file
            # Parse the weak motion data
            db.attrs["weak_motion_residual_file"] = self.wm_file
            logging.info("Parsing weak motion data")
            self._parse_station_data(self.wm_file, db, debug_lim)
            # Load metadata and concatenate to existing metadata
            logging.info("Loading and concatenating the weak motion metadata")
            metadata_weak = pd.read_hdf(self.wm_file, "flatfile")
            metadata = pd.concat([metadata, metadata_weak], axis=0)
            db.attrs["has_weak_motion"] = True
            self.has_weak_motion = True

        if station_history_directory:
            # Build the station daily operation histories
            self.station_history_directory = station_history_directory
            db.attrs["Station_history_directory"] = self.station_history_directory
            logging.info("Building station daily operation histories")
            get_station_channel_histories(db, self.station_history_directory)
        # Add the between event residuals
        logging.info("Parsing the between event residuals")
        get_between_event_residuals(db, self.gmms,
                                    self.sm_file,
                                    self.wm_file)
        # Create the empty hazard group
        _ = db.create_group("hazard")
        # Close the file
        db.close()
        logging.info("Storing metadata")
        metadata.to_hdf(self.dbname, "metadata")
        # Add the metadata
        if earthquake_catalogue_file:
            self.catalogue_file = earthquake_catalogue_file
            logging.info("Loading earthquake catalogue")
            catalogue = pd.read_csv(earthquake_catalogue_file, sep=",")
            catalogue.to_hdf(self.dbname, "catalogue")
        return

    def _parse_station_data(self, fname: str, db: h5py.File,
                            debug_lim: Optional[int] = None):
        """Retrieves station data (attributes, observations, predicted motions, random effects)
        from an hdf5 file - can be either strong or weak motion

        Args:
            fname: Path to file containing residual data
            db: Open database
            debug_lim: For debugging purposes limit to the first N stations
        """
        # Get the metadata from the flatfiles
        sm_metadata = pd.read_hdf(fname, "metadata")
        nrec = sm_metadata.shape[0]
        # Get station list
        # Group by stations
        sm_sta_grps = sm_metadata.groupby("sta")
        nsta = len(sm_sta_grps)
        # Create a general indexing array
        sm_idx = pd.DataFrame({"idx": np.arange(0, nrec)}, index=sm_metadata.index)
        # Open the flatfile data file
        fle = h5py.File(fname, "r")
        imts = fle.attrs["IMT"]
        if "IMTs" in db.attrs:
            imts_str = ",".join(imts)
            assert db.attrs["IMTs"] == imts_str, "Input IMTs don't match IMTs in database"
        else:
            db.attrs["IMTs"] = ",".join(imts)

        # Get the periods
        if "periods" not in list(db):
            periods_accel = []
            for imt in imts:
                if imt == "PGA":
                    periods_accel.append(0.0)
                elif imt.startswith("SA("):
                    periods_accel.append(float(imt.replace("SA(", "").replace(")", "")))
                else:
                    # Not an acceleration
                    continue
            periods_accel = np.array(periods_accel)
            per_dset = db.create_dataset("periods", periods_accel.shape, "f")
            per_dset[:] = periods_accel
            self._periods = periods_accel
        # Get the stations
        if "stations" not in list(db):
            db_sta_grp = db.create_group("stations")
        else:
            db_sta_grp = db["stations"]
        cntr = 1
        # Generate unique station integer IDs
        unique_int_ids = 100000 + np.arange(0, nsta)
        for i, (stn, stn_grp) in enumerate(sm_sta_grps):
            st_net, st_name, channel = stn.split("-")
            sta_key = "-".join([st_net, st_name])
            logging.info(sta_key)
            if sta_key not in list(db_sta_grp):
                # Create the station and add on the attributes
                sta = db_sta_grp.create_group(sta_key)
                unique_id = unique_int_ids[i]
                # Add on the attributes
                sta.attrs["nrec"] = stn_grp.shape[0]
                sta.attrs["longitude"] = self._get_sm_attr(stn_grp, "sta_lon")
                sta.attrs["latitude"] = self._get_sm_attr(stn_grp, "sta_lat")
                sta.attrs["start_eida"] = self._get_sm_attr(stn_grp, "sta_start_date",
                                                            dtype="s")
                sta.attrs["end_eida"] = self._get_sm_attr(stn_grp, "sta_end_date", dtype="s")
                sta.attrs["status"] = self._get_sm_attr(stn_grp, "sta_status", dtype="s")
                sta.attrs["lon_eida"] = self._get_sm_attr(stn_grp, "sta_lon_eida", dtype="s")
                sta.attrs["lat_eida"] = self._get_sm_attr(stn_grp, "sta_lat_eida", dtype="s")
                sta.attrs["vs30"] = self._get_sm_attr(stn_grp, "vs30")
                sta.attrs["vs30measured"] = self._get_sm_attr(stn_grp, "vs30measured")
                sta.attrs["unique_integer_id"] = unique_id
                self.station_id_map[unique_id] = sta_key
            else:
                sta = db_sta_grp[sta_key]
            if channel in list(sta):
                raise ValueError("Station %s already contains channel %s" % (sta_key, channel))
            channel_grp = sta.create_group(channel)
            # Get the channel data
            logging.info("---- %s" % channel)
            gmm_list = _parse_channel_data(channel_grp, stn_grp, fle, sm_idx)
            cntr += 1
            if debug_lim and cntr >= debug_lim:
                break
        logging.info("All Channel Observed accelerations and predictions parsed from %s"
                     % fname)
        fle.close()
        logging.info("Retrieving ds2s")
        ds2s = {}
        for gmm in gmm_list:
            if gmm not in self.gmms:
                self.gmms.append(gmm)
            ds2s[gmm] = pd.read_hdf(fname, "RandomEffects/{:s}/between_station".format(gmm))
        cntr = 1
        for stn in list(sm_sta_grps.groups):
            st_net, st_name, channel = stn.split("-")
            sta_key = "-".join([st_net, st_name])
            ds2s_grp = db["stations/{:s}/{:s}".format(sta_key, channel)].create_group("ds2s")
            for gmm in gmm_list:
                ds2s_gmm_sta = ds2s[gmm].loc[stn].to_numpy()
                dset = ds2s_grp.create_dataset(gmm, ds2s_gmm_sta.shape, dtype="f")
                dset[:] = ds2s_gmm_sta
            cntr += 1
            if debug_lim and cntr >= debug_lim:
                break
        logging.info("done")
        return

    def add_eida_station_book_data(self, eida_data_file: str):
        """If an EIDA station book json is available then this information an be added to
        or update the EIDA station metadata

        Args:
            eida_data_file: path/to/eida_station_data.json
        """
        if not self.db:
            self.open()
            was_open = False
        else:
            was_open = True
        with open(eida_data_file, "r") as f:
            eida_data = json.load(f)
        eida_networks = list(eida_data)
        for stn in list(self.db["stations"]):
            # Find in database
            network, stn_code = stn.split("-")
            if network not in eida_networks:
                logging.info("No EIDA data %s -  No entry for network %s" % (stn, network))
                continue
            has_entry = False
            for source in eida_data[network]:
                if stn_code in eida_data[network][source]:
                    # Has an EID entry - get data and move on
                    self.db["stations/{:s}".format(stn)].attrs["lon_eida"] =\
                        eida_data[network][source][stn_code]["longitude"]
                    self.db["stations/{:s}".format(stn)].attrs["lat_eida"] =\
                        eida_data[network][source][stn_code]["latitude"]
                    self.db["stations/{:s}".format(stn)].attrs["start_eida"] =\
                        eida_data[network][source][stn_code]["startDate"]
                    self.db["stations/{:s}".format(stn)].attrs["end_eida"] =\
                        eida_data[network][source][stn_code]["endDate"]
                    self.db["stations/{:s}".format(stn)].attrs["status"] =\
                        eida_data[network][source][stn_code]["status"]
                    has_entry = True
                    break
            if not has_entry:
                logging.info("No EIDA data for %s - Network %s has no entry for %s"
                             % (stn, network, stn_code))
        if not was_open:
            self.db.close()
        return

    @staticmethod
    def _get_sm_attr(grp, key, dtype="f"):
        """Get strong motion attributes
        """
        if (key in grp.columns) and (grp[key].shape[0] >= 1):
            return grp[key].iloc[0]
        else:
            if dtype == "s":
                return "NA"
            else:
                return h5py.Empty(dtype=dtype)

    @staticmethod
    def _extract_station_history_data(db, station: str) -> Dict:
        """Extracts daily histories for the station

        Args:
            db: Database at h5py.File object
            station: Station name

        Returns:
            station_history: The daily operation histories for the station by channel
        """
        stn_grp = db["stations/{:s}".format(station)]
        station_history = {"start_eida": stn_grp.attrs["start_eida"],
                           "end_eida": stn_grp.attrs["end_eida"],
                           "status": stn_grp.attrs["status"],
                           "channels": {}}
        for channel in list(stn_grp):
            if "daily_history" in list(stn_grp[channel]):
                data = stn_grp["{:s}/daily_history".format(channel)][:]
                # Convert from numpy
                station_history["channels"][channel] = pd.DataFrame({
                    "date": data["date"].astype(np.datetime64),
                    "availability": data["availability"]
                })
            else:
                print("No daily history for station: %s channel: %s" % station, channel)
        return station_history

    def plot_station_data_availability(self, station: str, groupby: str = "W",
                                       channel: Optional[str] = None,
                                       figsize: Tuple = (12, 6),
                                       start_date: Optional[str] = None,
                                       end_date: Optional[str] = None,
                                       figname: str = "", figtype: str = "png", dpi: int = 300):
        """
        Creates plots of the availability of data for each station (one plot per channel
        if multiple channels are specified
        """
        station_history = self._extract_station_history_data(self.db, station)
        if not station_history["channels"]:
            raise ValueError("No station histories found for %s" % station)
        if channel:
            station_history["channels"] = {channel: station_history["channels"][channel]}
        nchannels = len(list(station_history["channels"]))
        if nchannels == 1:
            nrow = 1
            ncol = 1
        elif nchannels == 2:
            nrow = 1
            ncol = 2
        else:
            nrow = 2
            ncol = 2

        start_date = np.datetime64(start_date) if start_date else np.datetime64(self.start_date)
        end_date = np.datetime64(end_date) if end_date else np.datetime64(self.end_date)
        tdiff = end_date - start_date

        for (dlow, dhigh), (dint, dfmt) in dateaxis_formatter.items():
            if (tdiff >= (366 * dlow)) & (tdiff <= 366 * dhigh):
                break

        fig = plt.figure(figsize=figsize)
        for i, channel in enumerate(station_history["channels"]):
            ax = fig.add_subplot(nrow, ncol, i + 1)
            date_key, ndays = DATE_GROUPER[groupby]
            sta_hist = station_history["channels"][channel].resample(groupby, on="date")
            sta_hist_chan = sta_hist["availability"].sum() / ndays
            ax.axvspan(np.datetime64(station_history["start_eida"]),
                       np.datetime64(station_history["end_eida"]),
                       color="lightgreen", alpha=0.2,
                       label="EIDA\nStation\nBook")
            ax.fill_between(sta_hist_chan.index,
                            np.zeros(len(sta_hist_chan)),
                            sta_hist_chan.to_numpy(),
                            color="darkblue", alpha=0.5, label='EIDA\nWFCatalog')
            ax.grid(which="both")
            ax.set_ylabel("% Operational per {:}".format(date_key), fontsize=18)
            ax.set_xlabel("Date", fontsize=18)
            ax.set_ylim(0.0, 100.0)
            ax.set_xlim(np.datetime64(start_date), np.datetime64(end_date))

            ax.xaxis.set_major_locator(dint)
            ax.xaxis.set_major_formatter(dfmt)
            plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
            ax.legend(loc="upper left", fontsize=16)
            ax.set_title("%s (%s)" % (station, channel),
                         fontsize=16)
            ax.tick_params(labelsize=12)
            fig.tight_layout()
            if figname:
                plt.savefig(figname, format=figtype, dpi=300, bbox_inches="tight")
            return

    def get_observed_accelerations(
            self,
            station: str,
            channel: Optional[str] = None,
            periods: Optional[np.ndarray] = None
            ) -> Tuple[np.ndarray, Dict]:
        """Extracts the observed acceleration data for a given station and channel
        (or all channels).

        Args:
            station: Station ID
            channel: Channel ID
            periods: Selected periods for accelerations. Will return the original
                     periods if not specified, otherwise will interpolate to the
                     desired periods.

        Returns:

            periods: Selected periods

            accelerations: Dictionary containing for each channel the accelerations
                           (as a numpy array), the EIDA start and end dates of the
                           station (as np.datetime64 objects) the indexes of the
                           observations in the metadata and the station daily
                           history (as pd.DataFrame) if available.

        Raises:
            ValueError: if selected periods outside the observed range.
        """
        sel_periods = self.db["periods"][:]
        if periods is None:
            periods = sel_periods.copy()
        else:
            min_t, max_t = np.min(sel_periods), np.max(sel_periods)
            for per in periods:
                if (per < min_t) or (per > max_t):
                    raise ValueError(
                        "Selected period %.6f outside measured period range (%.5f - %.5f)"
                        % (per, min_t, max_t)
                    )
        stn_grp = self.db["stations/{:s}".format(station)]
        if channel is None:
            channels = list(stn_grp)
        else:
            channels = [channel]
        accelerations = {}
        for channel in channels:
            accelerations[channel] = {
                "values": np.exp(stn_grp["{:s}/acceleration".format(channel)][:]),
                "indexes": stn_grp["{:s}/indices".format(channel)][:],
            }
            start_eida, end_eida = stn_grp.attrs["start_eida"], stn_grp.attrs["end_eida"]

            accelerations[channel]["start_eida"] =\
                None if start_eida == "NA" else np.datetime64(start_eida)
            accelerations[channel]["end_eida"] =\
                None if end_eida == "NA" else np.datetime64(end_eida)

            if "daily_history" in list(stn_grp[channel]):
                data = stn_grp["{:s}/daily_history".format(channel)][:]
                accelerations[channel]["history"] = pd.DataFrame({
                        "date": data["date"].astype(np.datetime64),
                        "availability": data["availability"]
                    })
            else:
                accelerations[channel]["history"] = None
        if (len(periods) == len(sel_periods)):
            if np.allclose(periods, sel_periods):
                # Keep all of the periods
                return periods, accelerations
        # Otherwise interpolate accelerations to selected periods
        for channel in channels:
            ipl = interp1d(np.log10(sel_periods), np.log10(accelerations[channel]["values"]))
            accelerations[channel]["values"] = 10.0 ** ipl(np.log10(periods))
        return periods, accelerations

    def get_ds2s_for_sites(self, stations: List, gmms: List, channels: Optional[List] = None,
                           periods: Optional[np.ndarray] = None) -> Tuple[Dict, np.ndarray]:
        """Retrieves delta s2s values for each of the requested stations

        Args:
            stations: List of station IDs
            gmms: List of GMM IDs
            channels: List of channel IDs
            periods: Periods to calculate dBe for (uses all if None)

        Returns:
            ds2s: The site-to-site residuals for all requested stations
            periods: Numpy array of selected periods
        """
        if channels is None:
            channels = ["HN", "HG", "HH", "EH"]

        orig_periods = self.db["periods"][:]
        if periods is None:
            periods = orig_periods.copy()
            use_original_periods = True
        else:
            use_original_periods = False
        # IMTs
        imts = ["PGA" if np.isclose(per, 0.0) else "SA({:.6f})".format(per) for per in periods]
        # Pre-allocate values with nans
        nsites = len(stations)
        nper = len(periods)
        # Pre-allocate
        ds2s = {}
        nrec = {}
        for gmm in gmms:
            ds2s[gmm] = {}
            nrec[gmm] = {}
            for channel in channels:
                ds2s[gmm][channel] = np.nan + np.zeros([nsites, len(orig_periods)])
        for i, station in enumerate(stations):
            stn_grp = self.db["stations/{:s}/".format(station)]
            for channel in channels:
                if channel not in list(stn_grp):
                    nrec[gmm][channel] = 0
                    continue
                for gmm in gmms:
                    if gmm not in list(stn_grp["{:s}/ds2s".format(channel)]):
                        continue
                    ds2s_i = stn_grp["{:s}/ds2s/{:s}".format(channel, gmm)][:]
                    idx = np.fabs(ds2s_i) > 1.0E-10
                    ds2s[gmm][channel][i, idx] = ds2s_i[idx]
                    nrec[gmm][channel] = stn_grp["{:s}/acceleration".format(channel)].shape[0]
        # To dataframe
        for gmm in gmms:
            for channel in channels:
                if use_original_periods:
                    # Take the periods as they are
                    ds2s[gmm][channel] = pd.DataFrame(
                        dict([(imts[i], ds2s[gmm][channel][:, i]) for i in range(nper)]),
                        index=stations)
                else:
                    # Interpolate to desired periods
                    ipl = interp1d(orig_periods, ds2s[gmm][channel])
                    ds2s_i = ipl(periods)
                    ds2s[gmm][channel] = pd.DataFrame(
                        dict([(imts[i], ds2s_i[:, i]) for i in range(nper)]),
                        index=stations)

        return ds2s, periods, nrec

    def get_preferred_amplification_from_ds2s(
            self,
            stations: List,
            imts: List,
            gmm_label: str,
            phi_s2s: Optional[CoeffsTable] = None,
            well_constrained: int = 10,
            partially_constrained: int = 3,
            channel_set: Optional[List] = None
            ) -> pd.DataFrame:
        """
        Retrieve preferred ds2s for each station from all channels listed and return
        as a pandas dataframe

        Args:
            stations: List of stations
            imts: List of intensity measure types
            gmm_label: GMM to which the ds2s refers
            phi_s2s: Coefficients table to use for the phi_s2s values (adopts ESHM20
                     model ds2s for observed Vs30 if not specified)
            well_constrained: No. of observations at station for ds2s to be considered
                              well constrained (i.e. phi_s2s is 0)
            partially_constrained: No. of observations at station for ds2s to be considered
                                   constrained at all (less than this value then no
                                   amplification will be considered and the full phi_s2s
                                   assigned as the uncertainty)
            channel_set: List of channels from which to retrieve ds2s

        Returns:
            outputs: Dataframe indexed by station containing the exp(ds2s) for each IMT and
                     the standard deviation
        """
        if channel_set is None:
            channel_set = ["HH", "EH", "HN", "HG"]
        if not phi_s2s:
            # Take Phis2s from Europe
            phi_s2s = PHI_S2S
        if self.db:
            # The database is open, so need to close it
            db_open = True
            self.close()
        else:
            # Database originally closed
            db_open = False
        self.open("r")
        # Retrieve the station list and periods
        all_stns = list(self.db["stations"])
        periods = self.db["periods"]
        sel_periods = []
        # Get periods for the IMTs
        for imt in imts:
            if "PGA" in imt:
                sel_periods.append(0.01)
            else:
                sel_periods.append(float(imt.replace("SA(", "").replace(")", "")))
        sel_periods = np.array(sel_periods)
        outputs = {"station": [], "N": [], "channel": []}
        for key in imts:
            outputs[key] = []
        for key in imts:
            outputs["sigma_" + key] = []
        for i, stn in enumerate(stations):
            if stn not in all_stns:
                logging.info("No data for station %s" % stn)
                continue
            # Retrieve the ds2s for each channel and take the ds2s for the
            # channel with the largest no. of observations
            channel_ds2s = {}
            channels = []
            channel_n = []
            for channel in channel_set:
                if channel not in list(self.db["stations/{:s}".format(stn)]):
                    # Channel not available for this station
                    continue
                channels.append(channel)
                nobs = self.db["stations/{:s}/{:s}/acceleration".format(stn, channel)].shape[0]
                channel_n.append(nobs)
                ds2s = self.db["stations/{:s}/{:s}/ds2s/{:s}".format(stn,
                                                                     channel,
                                                                     gmm_label)][:]
                channel_ds2s[channel] = ds2s
            if len(channels):
                # One or more channels - select the ds2s from the most well constrained
                # channel
                iloc = np.argmax(np.array(channel_n))
                sel_channel = channels[iloc]
                nobs = channel_n[iloc]
                ds2s = channel_ds2s[sel_channel]
                logging.info("%s - Selected ds2s from channel %s (%g obs.)"
                             % (stn,  sel_channel, nobs))
                # Interpolate to the selected periods then take exponent to get amplification
                ipl = interp1d(np.log10(periods), ds2s)
                ds2s = np.exp(ipl(np.log10(sel_periods)))
            else:
                # No ds2s available from selected channels
                ds2s = np.ones(len(imts))
                nobs = 0
                sel_channel = "U"
            # Uncertainties should come from the phi_s2s. If the number of observations is
            # greater than the well constrained threshold then we assume the uncertainty on
            # amplification is 0. If the no. obs. is less than the partially constrained
            # threshold then we assume no ds2s and retain the full uncertainty. For the
            # intermediate values we linearly interpolate between 0 and phi_s2s
            sigma = np.zeros(len(ds2s))
            for j, imt in enumerate(imts):
                phi_s2s_imt = phi_s2s[from_string(imt)]["phi_s2s_obs"]
                if nobs >= well_constrained:
                    sigma[j] = 0.0
                elif nobs < partially_constrained:
                    sigma[j] = phi_s2s_imt
                    ds2s[j] = 1.0  # Reset the ds2s to zero as we don't trust the ds2s
                else:
                    sigma[j] = 0.0 + (nobs - well_constrained) *\
                        (phi_s2s_imt / (partially_constrained - well_constrained))
            outputs["station"].append(stn)
            outputs["N"].append(nobs)
            outputs["channel"].append(sel_channel)
            for j in range(len(imts)):
                outputs[imts[j]].append(ds2s[j])
                outputs["sigma_{:s}".format(imts[j])].append(sigma[j])
        outputs = pd.DataFrame.from_records(outputs, 'station')
        if not db_open:
            self.close()
        return outputs

    def get_dbes(self, gmms: List, periods: Optional[np.ndarray] = None) ->\
            Tuple[Dict, Dict, np.ndarray]:
        """Get the between event residuals for a given GMM(s)

        Args:
            gmms: The selected GMM names
            periods: Target periods for the dbes

        Returns:
            dbe_strong: Dictionary of dataframes of dbes for the strong motion records
            dbe_weak: Dictionary of dataframes of dbes for the strong motion records
            periods: Numpy array of periods
        """
        if self.db:
            # The database is open, so need to close it
            db_open = True
            self.close()
        else:
            # Database originall closed
            db_open = False
        # Close database is easy - read from pandas
        dbe_strong = {}
        dbe_weak = {}
        for gmm in gmms:
            if self.has_strong_motion:
                dbe_strong[gmm] = pd.read_hdf(
                    self.dbname,
                    "dbe/strong_motion/{:s}".format(gmm)
                )
                dbe_strong[gmm].set_index("evid", inplace=True)
            if self.has_weak_motion:
                dbe_weak[gmm] = pd.read_hdf(
                    self.dbname,
                    "dbe/weak_motion/{:s}".format(gmm)
                )
                dbe_weak[gmm].set_index("evid", inplace=True)
        # Open (or re-open) database to get periods
        self.open()
        orig_periods = self.db["periods"][:]
        if not db_open:
            # Database was closed originally, so re-close it now
            self.close()
        if periods is None:
            return dbe_strong, dbe_weak, orig_periods

        # Need to interpolate periods
        imt_str = ["SA({:.5f})".format(per) for per in periods]
        for gmm in gmms:
            if self.has_strong_motion:
                assert dbe_strong[gmm].shape[1] == len(orig_periods)
                ipl = interp1d(orig_periods, dbe_strong[gmm].to_numpy())
                dbe_strong_gmm = ipl(periods)
                # Re-set the dataframe
                dbe_strong[gmm] = pd.DataFrame({}, index=dbe_strong[gmm].index)
                # and add the imts for each period
                for j, imt in enumerate(imt_str):
                    dbe_strong[gmm][imt] = dbe_strong_gmm[:, j]
            if self.has_weak_motion:
                assert dbe_weak[gmm].shape[1] == len(orig_periods)
                ipl = interp1d(orig_periods, dbe_weak[gmm].to_numpy())
                dbe_weak_gmm = ipl(periods)
                dbe_weak[gmm] = pd.DataFrame({}, index=dbe_weak[gmm].index)
                for j, imt in enumerate(imt_str):
                    dbe_weak[gmm][imt] = dbe_weak_gmm[:, j]
        return dbe_strong, dbe_weak, periods

    def plot_observed_hazard_curves_for_station(self, station: str, periods: np.ndarray,
                                                xvals: Optional[np.ndarray] = None,
                                                xlim: Tuple[float, float] = (),
                                                ylim: Tuple[float, float] = (),
                                                availability_threshold: float = 90.0,
                                                figsize: Tuple[float, float] = (12.0, 10.0),
                                                figname: str = "", figtype: str = "png",
                                                dpi: int = 300):
        """
        Creates a plot of observed hazard curves for each station
        """
        model_periods, accelerations = self.get_observed_accelerations(station,
                                                                       periods=periods)
        if xvals is None:
            xvals = np.logspace(-3.0, 1.0, 100)
        fig = plt.figure(figsize=figsize)
        nrow, ncol = best_subplot_dimensions(len(model_periods))
        for i, per in enumerate(model_periods):
            ax = fig.add_subplot(nrow, ncol, i + 1)
            for channel in accelerations:
                accels = accelerations[channel]["values"][:, i]
                if accelerations[channel]["history"] is None:
                    # Get number of days from EIDA start and end times
                    eida_start = accelerations[channel]["eida_start"]
                    eida_end = accelerations[channel]["eida_end"]
                    eida_start = eida_start if eida_start else np.datetime64(self.start_date)
                    eida_end = eida_end if eida_end else np.datetime64(self.end_date)

                    ndays = (eida_end - eida_start).astype("timedelta64[D]")
                    ndays = ndays.astype(float)
                else:
                    # Get the number of days with availability over the threshold
                    idx = accelerations[channel]["history"]["availability"] >=\
                        availability_threshold
                    ndays = idx.sum()
                time_fact = 365.25 / ndays
                rates = np.zeros(len(xvals))
                for j, xval in enumerate(xvals):
                    rates[j] = np.sum(accels >= xval)
                rates *= time_fact
                ax.loglog(xvals, rates, "-", color=CHANNEL_PALETTE[channel], lw=2,
                          label=channel)
            ax.grid(which="both")
            if len(xlim):
                ax.set_xlim(xlim[0], xlim[1])
            if len(ylim):
                ax.set_ylim(ylim[0], ylim[1])
            ax.set_xlabel("SA(%.4f s) (g)" % per, fontsize=16)
            ax.set_ylabel("Observed Exceedance Rate", fontsize=16)
            ax.tick_params(labelsize=12)
            ax.legend(loc="upper right", fontsize=16)
        fig.tight_layout()
        if figname:
            plt.savefig(figname, format=figtype, dpi=dpi, bbox_inches="tight")
        return

    def plot_ds2s_for_site(self, station: str, gmms: List, channels: List,
                           xlim: Tuple[float, float] = (),
                           ylim: Tuple[float, float] = (),
                           figsize: Tuple[float, float] = (12.0, 10.0),
                           legend_loc: str = "lower left",
                           figname: str = "", figtype: str = "png",
                           dpi: int = 300):
        """
        Compare all ds2s with period for a station on a single plot
        """
        stn_grp = self.db["stations/{:s}".format(station)]
        lon = stn_grp.attrs["longitude"]
        lat = stn_grp.attrs["latitude"]
        ds2s, periods, nrec = self.get_ds2s_for_sites([station], gmms)
        fig = plt.figure(figsize=figsize)
        nrow, ncol = best_subplot_dimensions(len(gmms))
        for i, gmm in enumerate(gmms):
            if gmm not in ds2s:
                print("No ds2s for %s" % gmm)
                continue
            ax = fig.add_subplot(nrow, ncol, i + 1)
            for channel in channels:
                if channel not in ds2s[gmm]:
                    continue
                ds2s_stn = ds2s[gmm][channel].loc[station].to_numpy()
                ax.semilogx(periods, ds2s_stn, "-", color=CHANNEL_PALETTE[channel], lw=2,
                            label="%s [%g]" % (channel, nrec[gmm][channel]))
            ax.grid(which="both")
            ax.set_xlabel("Period (s)", fontsize=16)
            ax.set_ylabel(r"$\delta S2S_S$ (T)", fontsize=16)
            if len(xlim):
                ax.set_xlim(xlim[0], ylim[1])
            if len(ylim):
                ax.set_ylim(ylim[0], ylim[1])
            ax.tick_params(labelsize=14)
            ax.set_title(gmm, fontsize=16)
            ax.legend(loc=legend_loc, fontsize=16)
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        fig.suptitle("%s (%.4f E, %.4f N)" % (station, lon, lat),
                     fontsize=16)
        if figname:
            plt.savefig(figname, format=figtype, dpi=dpi, bbox_inches="tight")

    def plot_dbe_for_event(self, event: str, gmms: List,
                           xlim: Tuple[float, float] = (),
                           ylim: Tuple[float, float] = (),
                           figsize: Tuple[float, float] = (12.0, 10.0),
                           figname: str = "", figtype: str = "png",
                           dpi: int = 300):
        """
        Plots the between event residuals for a given event
        """
        dbe_strong, dbe_weak, periods = self.get_dbes(gmms)
        fig = plt.figure(figsize=figsize)
        nrow, ncol = best_subplot_dimensions(len(gmms))
        for i, gmm in enumerate(gmms):
            ax = fig.add_subplot(nrow, ncol, i + 1)
            if dbe_strong and event in dbe_strong[gmm].index:
                dbe_i = dbe_strong[gmm].loc[event].to_numpy()
                dbe_i[np.isclose(dbe_i, 0.0)] = np.nan
                if np.all(np.isnan(dbe_i)):
                    print("No strong motion dbe for GMM %s and event %s " % (gmm, event))
                ax.semilogx(periods, dbe_i, "b-", lw=2, label="Strong Motion")
            if dbe_weak and event in dbe_weak[gmm].index:
                dbe_i = dbe_weak[gmm].loc[event].to_numpy()
                dbe_i[np.isclose(dbe_i, 0.0)] = np.nan
                if np.all(np.isnan(dbe_i)):
                    print("No weak motion dbe for GMM %s and event %s " % (gmm, event))
                ax.semilogx(periods, dbe_i, "r-", lw=2, label="Weak Motion")
            ax.grid(which="both")
            ax.set_xlabel("Period (s)", fontsize=18)
            ax.set_ylabel(r"$\delta B_e$ (T)", fontsize=18)
            if len(xlim):
                ax.set_xlim(xlim[0], xlim[1])
            if len(ylim):
                ax.set_ylim(ylim[0], ylim[1])
            ax.tick_params(labelsize=14)
            ax.set_title(gmm, fontsize=16)
            ax.legend(loc="upper left", fontsize=16)
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        fig.suptitle("%s" % event, fontsize=20)
        if figname:
            plt.savefig(figname, format=figtype, dpi=dpi, bbox_inches="tight")
        return

    def sites_to_openquake_site_model(
            self,
            oq_site_model_file: str,
            sites: Optional[List] = None,
            reference_vs30: Optional[float] = None,
            reference_vs30_measured: bool = True,
            reference_z1pt0: Optional[float] = None,
            reference_z2pt5: Optional[float] = None,
            reference_xvf: Optional[float] = 150.0,
            regions: Optional[gpd.GeoDataFrame] = None,
            site_model_name: Optional[str] = "",
            ):
        """
        Exports the site model to OpenQuake formats (.csv or .xml)

        Args:
            os_site_model_file: Path to the OQ site model file
            sites: List of sites (otherwise use all the stations)
            reference_vs30: Assign all sites to the reference Vs30
            reference_vs30_measured: Assign the reference site to measured (True) or
                                     inferred (False)
            site_model_name: Name to go into the Site Model name
        """
        selected_columns = ["lons", "lats"]
        for col in self.OQ_SITE_MODEL_ATTRIBUTES:
            if col in self.stations.columns:
                selected_columns.append(col)
        selected_attrs = self.stations[selected_columns]
        del selected_attrs["sids"]
        if sites is not None:
            selected_attrs = selected_attrs.loc[sites]
        selected_attrs["custom_site_id"] = selected_attrs.index.copy()
        selected_attrs.reset_index(inplace=True, drop=True)
        selected_attrs.rename(columns={"lons": "lon",
                                       "lats": "lat"},
                              inplace=True,)
        nsites = selected_attrs.shape[0]
        if reference_vs30 is not None:
            selected_attrs["vs30"] = reference_vs30 * np.ones(nsites)
            selected_attrs["vs30measured"] = reference_vs30_measured * np.ones(nsites,
                                                                               dtype=bool)
        else:
            vs30_data = self.stations[["vs30", "vs30measured"]].reset_index(inplace=False,
                                                                            drop=True)
            selected_attrs["vs30"] = vs30_data["vs30"].copy()
            selected_attrs["vs30measured"] = vs30_data["vs30measured"].copy()
        if reference_z1pt0 is not None:
            selected_attrs["z1pt0"] = reference_z1pt0 * np.ones(nsites)
        else:
            selected_attrs["z1pt0"] = vs30_to_z1pt0_cy14(selected_attrs["vs30"].to_numpy(),
                                                         japan=True)

        if reference_z2pt5 is not None:
            selected_attrs["z2pt5"] = reference_z2pt5 * np.ones(nsites)
        else:
            selected_attrs["z2pt5"] = vs30_to_z2pt5_cb14(selected_attrs["vs30"].to_numpy(),
                                                         japan=True)
        selected_attrs["xvf"] = reference_xvf * np.ones(nsites)
        if regions is not None:
            geom = gpd.points_from_xy(selected_attrs["lon"].to_numpy(),
                                      selected_attrs["lat"].to_numpy())
            geo_df = gpd.GeoDataFrame(selected_attrs, geometry=geom, crs="EPSG:4326")
            geo_df = assign_sites_to_regions(geo_df, regions)
            selected_attrs["region"] = geo_df.region
            if "geometry" in selected_attrs.columns:
                del selected_attrs["geometry"]

        # Need to drop the duplicates
        selected_attrs.drop_duplicates(["lon", "lat"], inplace=True)
        if os.path.splitext(oq_site_model_file)[1] == ".csv":
            # Export to csv
            selected_attrs.to_csv(oq_site_model_file, index=False)
        else:
            # Export to xml
            sites = []
            for i, row in selected_attrs.iterrows():
                site = dict(row)
                if row.vs30measured:
                    site["vs30Type"] = "measured"
                else:
                    site["vs30Type"] = "inferred"
                del site["vs30measured"]
                sites.append(Node("site", site))
            site_model = Node("siteModel",
                              {"name": site_model_name},
                              nodes=sites)
            with open(oq_site_model_file, "wb") as f:
                nrml.write([site_model], f, fmt="%s", xmlns=nrml.NAMESPACE)

        print("Written site model to %s" % oq_site_model_file)
        return selected_attrs

    def _extract_stations_id_mapping(self) -> Dict:
        """Returns the mapping between the unique integer IDs of the stations and the
        actual station names
        """
        site_map = {}
        for stn in list(self.db["stations"]):
            site_map[self.db["stations/{:s}".format(stn)].attrs["unique_integer_id"]] = stn
        return site_map

    def add_hazard_from_oq_manager(
            self,
            hazard_file: str,
            models: Optional[List] = None,
            ):
        """Copy across hazard models from the OpenQuakeHazardManager

        Args:
            manager: OpenQuakeHazardManager
            models: List of model names to copy
        """
        # Close database and re-open with write permissions
        # Setup space in the database
        if self.db:
            was_open = True
            # Close the db and re-open with write permission
            self.close()
        else:
            was_open = False
        self.db = h5py.File(self.dbname, "r+")
        haz_db = h5py.File(hazard_file, "r")
        # For each model check if it is in the original file and NOT already in
        # the new database
        haz_models = list(haz_db)
        current_models = list(self.db["hazard"])
        if models is None:
            models = haz_models
        copy_fail = False
        for model in models:
            if model not in haz_models:
                # Check if the model is already in the source hazard database
                # Close the databases and raise the error
                copy_fail = True
                copy_error_message = "Requested model %s not found in source database" % model
            # Check if the model is already in the station database
            if model in current_models:
                copy_fail = True
                copy_error_message = "Requested model %s already exists in database" % model
            if copy_fail:
                # Close the databases and raise the error
                haz_db.close()
                self.db.close()
                if was_open:
                    self.db.open("r")
                raise ValueError(copy_error_message)
            # All good - copy from source database to current station database
            haz_db.copy(model, self.db["hazard"])
            logging.info("Adding model %s from %s" % (model, hazard_file))
        haz_db.close()
        self.db.close()
        if was_open:
            self.db.open("r")
        return

    def add_hazard_model_from_openquake_datastore(
            self,
            calc_id: Union[int, str],
            model_name: str,
            str_len: int = 200,
            gsim_map: Optional[Dict] = None,
            ):
        """Adds PSHA results to the database directly from an OpenQuake hazard
        calculation datastore
        """
        # Setup space in the database
        if self.db:
            was_open = True
            # Close the db and re-open with write permission
            self.close()
        else:
            was_open = False
        self.db = h5py.File(self.dbname, "r+")
        haz_grp = self.db["hazard"]
        if "model_name" in list(haz_grp):
            raise ValueError("Model name %s already exists in database %s" % model_name)
        logging.info("Adding model %s to database" % model_name)
        extract_information_from_openquake_datastore(calc_id, haz_grp, str_len, gsim_map)
        self.close()
        if was_open:
            # Re-open in read-only mode
            self.open("r")
        return

    def add_hazard_model_results(self, model_results: Dict, model_name: str):
        """
        Adds the results of a given seismic hazard model to the database.
        The a model by the same name is present then this raises an error
        and the user will need to remove that model from the database

        Args:
            model_results: Dictionary containing the relevant model information
                           (as output from hazard extractor)
            model_name: Name of the model

        """
        if self.db:
            was_open = True
            # Close the db and re-open with write permission
            self.close()
        else:
            was_open = False
        self.db = h5py.File(self.dbname, "r+")
        haz_grp = self.db["hazard"]
        if "model_name" in list(haz_grp):
            raise ValueError("Model name %s already exists in database %s" % model_name)
        logging.info("Adding model %s to the database" % model_name)
        logging.info("... adding metadata")
        model_grp = haz_grp.create_group(model_name)
        model_grp.attrs["description"] = model_results["description"]
        model_grp.attrs["gsim_logic_tree"] = model_results["gsim_logic_tree"]
        model_grp.attrs["job_ini"] = model_results["job_ini"]
        model_grp.attrs["source_model_logic_tree"] = model_results["source_model"]
        # Serialize the gsim map to a json string
        model_grp.attrs["gsim_map"] = json.dumps(model_results["gsim_map"])
        # Add IMTLs
        imtls_grp = model_grp.create_group("IMTLs")
        for imtl in model_results["IMTLs"]:
            imtl_dset = imtls_grp.create_dataset(
                imtl,
                (len(model_results["IMTLs"][imtl]),),
                dtype="f")
            imtl_dset[:] = np.array(model_results["IMTLs"][imtl])
        # Add the poes and quantiles
        poes_dset = model_grp.create_dataset(
            "poes",
            (len(model_results["poes"]),),
            dtype="f")
        poes_dset[:] = np.array(model_results["poes"])
        qntls_dset = model_grp.create_dataset(
            "quantiles",
            (len(model_results["quantiles"]),),
            dtype="f")
        qntls_dset[:] = np.array(model_results["quantiles"])
        # Store the realizations and weights
        logging.info("... adding realizations data")
        rlzs = []
        weights = []
        for rlz, weight in model_results["realizations"]:
            rlzs.append(rlz)
            weights.append(weight)
        rlz_dtype = [("rlz", (np.string_, 40)), ("weight", np.float64)]
        rlz_array = np.zeros(len(rlzs), rlz_dtype)
        rlz_array["rlz"] = rlzs
        rlz_array["weight"] = np.array(weights)
        rlz_dset = model_grp.create_dataset('realizations', rlz_array.shape, rlz_dtype)
        rlz_dset[:] = rlz_array
        # Get the sites information and add on the station keys
        sites = model_results["sites"]
        logging.info("... adding sites data")
        if "custom_site_id" in sites.columns:
            sites["stations"] = sites["custom_site_id"]
        # Convert to array
        site_dtypes = []
        for key in sites.columns:
            site_dtypes.append((key, SITE_DTYPES[key]))
        site_array = np.zeros(sites.shape[0], dtype=site_dtypes)
        for key in sites.columns:
            site_array[key] = sites[key].to_numpy()
        site_dset = model_grp.create_dataset("sites", site_array.shape, site_dtypes)
        site_dset[:] = site_array
        logging.info("... adding curves")
        # Add the curves and stats
        for key in ["hstats", "hcurves"]:
            if key not in model_results or not len(model_results[key]):
                logging.info("%s not present in model" % key)
                continue
            dset = model_grp.create_dataset(key,
                                            model_results[key].shape,
                                            model_results[key].dtype)
            dset[:] = model_results[key]
        logging.info("done!")
        self.close()
        if was_open:
            # Re-open in read-only mode
            self.open("r")
        return

    def add_declustering_results(self, vcl: np.ndarray, flagvector: np.ndarray,
                                 event_id: np.ndarray, decluster_model_name: str,
                                 info: Optional[Dict] = None):
        """Adds declustering results corresponding to a given catalogue

        Args:
            vcl: Array containng the index to which a cluster belongs
            flagvector: Array containing the flag for each earthquakes with -1 for foreshocks,
                        0 for mainshocks, 1 for aftershocks
            event_id: IDs of events in the catalogue
            decluster_model_name: Key to indicate the declustering
            info: Dictionary of attributes or information about the declustering algorithm
                  to add as attributes in the database
        """
        neqs = len(event_id)
        assert len(vcl) == neqs, "vcl length doesn't agree with event id length"
        assert len(flagvector) == neqs, "flagvector length doesn't agree with event id length"
        # Get the maximum string length for the event ID
        max_str_len = 0
        for evid in event_id:
            if len(evid) > max_str_len:
                max_str_len = len(evid)
        if self.db:
            was_open = True
            # Close the db and re-open with write permission
            self.close()
        else:
            was_open = False
        self.db = h5py.File(self.dbname, "r+")
        # If no declustering group is present then create the declustering group
        if "declustering" in list(self.db):
            decl_grp = self.db["declustering"]
        else:
            decl_grp = self.db.create_group("declustering")
        decl_dtype = [("master_id", (np.string_, max_str_len)),
                      ("vcl", int),
                      ("flagvector", int)]
        decl_results = np.zeros(neqs, dtype=decl_dtype)
        decl_results["master_id"] = event_id
        decl_results["vcl"] = vcl
        decl_results["flagvector"] = flagvector
        # Create the data set and write to hdf5
        decl_dset = decl_grp.create_dataset(decluster_model_name,
                                            decl_results.shape,
                                            decl_results.dtype)
        decl_dset[:] = decl_results
        # Add on attributes,  if any
        if info is not None:
            for key in info:
                decl_dset.attrs[key] = info[key]
        self.close()
        if was_open:
            # Re-open in read-only mode
            self.open("r")
        return

    def get_model_hazard_curves_stations(self, haz_model: str,
                                         imts: Optional[List] = None,
                                         stations: Optional[List] = None) -> Dict:
        """
        Retrieves the model hazard curves for stations from the database of curves

        Args:
            haz_model: Name of hazard model (must be in database)
            imts: List of intensity measure types
            stations: List of selected stations (otherwise all for which the curves are
                      defined)
        Returns:
            curves: Dictionary containing the hazard curve information including: IMTLs,
                    hcurves, hstats, weights, stations, realizations (rlzs) and quantiles
        """
        if self.db:
            was_open = True
            self.close()
        else:
            was_open = False
        self.open()
        if haz_model not in list(self.db["hazard"]):
            raise ValueError("Hazard model %s not found in database" % haz_model)
        # Get the sites
        haz_sites = self.db["hazard/{:s}/sites".format(haz_model)][:]
        if "stations" not in haz_sites.dtype.names:
            # Add the stations from the station IDs
            haz_stations = [self.station_id_map[int(sid)]
                            for sid in haz_sites["custom_site_id"]]
        else:
            haz_stations = [sta.decode() for sta in haz_sites["stations"]]
        if stations is None:
            # If no stations are selected then select all from list
            stations = [stn for stn in haz_stations]
        # Get imtls
        imtls = {}
        imtl_pos = []
        haz_imtls = list(self.db["hazard/{:s}/IMTLs".format(haz_model)])
        if imts is None:
            # Load all hazard IMTLs
            imts = haz_imtls
        for imt in imts:
            if imt in haz_imtls:
                imtls[imt] = self.db["hazard/{:s}/IMTLs/{:s}".format(haz_model, imt)][:]
                imtl_pos.append(haz_imtls.index(imt))
            else:
                logging.info("No hazard results in model %s for imt %s" % (haz_model, imt))
        imtl_pos = np.array(imtl_pos)
        rlzs = self.db["hazard/{:s}/realizations".format(haz_model)][:]
        weights = rlzs["weight"]
        rlz = []
        for i in range(rlzs.shape[0]):
            rlz.append("~".join([str(rlzs["ordinal"][i]),
                                 rlzs["smlt_id"][i].decode(),
                                 rlzs["gsim_id"][i].decode()]))
        # Quantiles
        if "quantiles" in list(self.db["hazard/{:s}".format(haz_model)]):
            qntls = self.db["hazard/{:s}/quantiles".format(haz_model)][:]
        else:
            qntls = np.array([])
        # Get hazard curves and hazard stats
        hcurves = {}
        hstats = {}
        for stn in stations:
            iloc = stations.index(stn)
            if "hcurves" in list(self.db["hazard/{:s}".format(haz_model)]):
                hcurves[str(stn)] =\
                    self.db["hazard/{:s}/hcurves".format(haz_model)][iloc, :, imtl_pos, :]
            else:
                hcurves[str(stn)] = []

            if "hstats" in list(self.db["hazard/{:s}".format(haz_model)]):
                hstats[str(stn)] =\
                    self.db["hazard/{:s}/hstats".format(haz_model)][iloc, :, imtl_pos, :]
            else:
                hstats[str(stn)] = []
        self.close()
        if was_open:
            self.open()
        return {"IMTLs": imtls, "hcurves": hcurves, "hstats": hstats,
                "weights": weights, "stations": stations, "rlzs": rlz,
                "quantiles": qntls}

    def adjust_hazard_curves_with_ds2s(self, curves: Dict, gmm_label: str,
                                       phi_s2s: Optional[CoeffsTable] = None,
                                       well_constrained: int = 10,
                                       partially_constrained: int = 3
                                       ) -> Dict:
        """
        For a set of hazard curves for stations (de-)amplify the curves using the
        ds2s values (with uncertainty where ds2s is partially constrained or unconstrained)

        Args:
            curves: All hazard curves at a site as output from function
                    'get_model_hazard_curves_stations'
            gmm_label: GMM to which the ds2s refers
            phi_s2s: Coefficients table to use for the phi_s2s values (adopts ESHM20
                     model ds2s for observed Vs30 if not specified)
            well_constrained: No. of observations at station for ds2s to be considered
                              well constrained (i.e. phi_s2s is 0)
            partially_constrained: No. of observations at station for ds2s to be considered
                                   constrained at all (less than this value then no
                                   amplification will be considered and the full phi_s2s
                                   assigned as the uncertainty)
            channel_set: List of channels from which to retrieve ds2s
        Returns:
            curves: Hazard curves updated with site amplification
        """
        imts = list(curves["IMTLs"])
        stations = list(curves["hcurves"])
        df = self.get_preferred_amplification_from_ds2s(
            stations, imts, gmm_label,
            phi_s2s, well_constrained, partially_constrained)
        ampl_curves = {}
        ampl_stats = {}
        for site in stations:
            logging.info("Adjusting curves for station %s" % site)
            all_curves = curves["hcurves"][site]
            all_stats = curves["hstats"][site]
            ampl = df.loc[site]
            ampl_poes = np.zeros_like(all_curves)
            ampl_poes_stats = np.zeros_like(all_stats)
            for j, imt in enumerate(imts):
                imls = curves["IMTLs"][imt]
                mean_amp = ampl[imt]
                std_amp = ampl["sigma_{:s}".format(imt)]
                midpoints = np.diff(imls) / 2. + imls[:-1]
                for i in range(all_curves.shape[0]):
                    # For each branch
                    probs_curves = -np.diff(all_curves[i, j, :])
                    for mdp, prob in zip(midpoints, probs_curves):
                        ampl_poes[i, j, :] += (1.0 - norm_cdf(np.log(imls / mdp),
                                                              np.log(mean_amp),
                                                              std_amp)) * prob

                for i in range(all_stats.shape[0]):
                    # For each branch
                    probs_curves = -np.diff(all_stats[i, j, :])
                    for mdp, prob in zip(midpoints, probs_curves):
                        ampl_poes_stats[i, j, :] += (1.0 - norm_cdf(np.log(imls / mdp),
                                                     np.log(mean_amp),
                                                     std_amp)) * prob
            ampl_curves[site] = ampl_poes
            ampl_stats[site] = ampl_poes_stats
        curves["hcurves"] = ampl_curves
        curves["hstats"] = ampl_stats
        return curves
