"""
Tools for managing earthquake catalogues and harmonisation with flatfile databases
"""
import logging
from copy import deepcopy
from datetime import datetime
from typing import List, Dict, Union, Tuple, Optional
import h5py
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
logging.basicConfig(level=logging.INFO)


class Magnitude():
    """
    Class to store a list of magnitudes associated to an event

    Attributes:
        id: Event ID to which the magnitudes are associated
        mags: Array of magnitudes
        mtypes: List of magnitude types
        agencies: List of agencies report the magnitude
        stddev: Standard deviation of magnitudes
    """
    def __init__(self, event_id: str, mags: np.ndarray,
                 mtypes: List, agencies: Optional[Union[List, str]] = None,
                 stddev: Optional[Union[np.ndarray, float]] = None,
                 duplicate_magnitude_tolerance: float = 0.1):
        """Instantiate with an event ID and an array of magnitudes

        Args:
            event_id: Event ID
            mags: Array of magnitudes
            mtypes: List of magnitude types
            agencies: List of agencies (or string if a single agency)
            stddev: Standard deviation of magnitudes
        """
        self.id = event_id
        self.mags = mags
        self.mtypes = mtypes
        if isinstance(agencies, List):
            self.agencies = agencies
        elif isinstance(agencies, str):
            self.agencies = [agencies for i in range(len(self.mags))]
        else:
            # No agencies
            self.agencies = [self.id for i in range(len(self.mags))]
        if isinstance(stddev, np.ndarray):
            assert len(stddev) == len(self.mags), "No. of stddevs does not "\
                "equal number of magnitudes"
            self.stddev = stddev
        elif isinstance(stddev, float):
            self.stddev = stddev * np.ones(len(self.mags))
        else:
            self.stddev = np.zeros(len(self.mags))
        self._table = None

    def __len__(self):
        return len(self.mags)

    def __repr__(self):
        return "Magnitude Container of %g magnitudes for event %s" % (
            len(self), self.id)

    def __iter__(self):
        """
        Return the magnitude value, type and std deviation and, if available,
        the agency
        """
        for mag, mtype, sigma, agency in zip(self.mags, self.mtypes,
                                             self.stddev, self.agencies):
            yield mag, mtype, sigma, agency

    def append(self, mag: float, mtype: str, agency: str, sigma: float = 0.0):
        """Append a single magnitude representation to the list

        Args:
            mag: Magnitude
            mtype: Magnitude type
            agency: Agency of the magnitude
            sigma: Standard deviation of magnitude
        """
        self.mags = np.hstack(self.mags, np.array([mag]))
        self.mtype.append(mtype)
        self.agency.append(agency)
        self.stddev = np.hstack([self.stddev, np.array([sigma])])

    def merge(self, magnitude, strict: bool = True):
        """Merge another magnitude
        """
        self.mags = np.hstack([self.mags, magnitude.mags])
        self.mtypes.extend(magnitude.mtypes)
        self.agencies.extend(magnitude.agencies)
        self.stddev = np.hstack([self.stddev, magnitude.stddev])

    @property
    def table(self):
        """Returns the magnitudes in the form of a table
        """
        if self._table is not None:
            return self._table
        if not len(self.agencies):
            self.agences
        self._table = pd.DataFrame({
            "id": [self.id] * len(self),
            "M": self.mags,
            "mtype": self.mtypes,
            "agency": self.agencies,
            "msigma": self.stddev}
            )
        return self._table

    @classmethod
    def from_table(cls, table: pd.DataFrame):
        """Build the magnitude object from the pandas dataframe
        """
        event_id = table["master_id"].iloc[0]
        return cls(event_id, table["M"].to_numpy(),
                   list(table["mtype"]), list(table["agency"]),
                   table["msigma"].to_numpy())


class Event():
    """
    Class to represent an event as a set of origins (locations and times) and of magnitudes

    Attributes:
        id: Event ID
        magnitudes: Magnitude
    """
    def __init__(self, identifier: str, origins, magnitudes=None):
        """
        Args:
            identifier: Event ID
            origins: ?
            magnitudes: ?

        """
        self.id = identifier
        self._origins = origins
        self._table = None
        if magnitudes:
            assert isinstance(magnitudes, Magnitude)
            self.magnitudes = magnitudes
        else:
            self.magnitudes = []

    def __repr__(self):
        """Return descriptive information about event
        """
        if len(self._origins) <= 3:
            origin_strings = []
            for origin in self._origins:
                origin_strings.append(
                    "Origin: %s, %s (%.4f, %.4f, %.1f)" %
                    (origin.ev_id, str(origin.ev_time), origin.lon, origin.lat, origin.depth))
        else:
            origin_strings = []
            for i in [0, 1, 2, -1]:
                if i == 2:
                    origin_strings.append("...")
                else:
                    origin = self._origins[i]
                    origin_strings.append(
                        "Origin: %s, %s (%.4f, %.4f, %.1f)" %
                        (origin.ev_id, str(origin.ev_time), origin.lon, origin.lat,
                         origin.depth))
        return "Event %s\n%s\n%g magnitudes" % (self.id,
                                                "\n".join(origin_strings),
                                                len(self.magnitudes))

    def append(self, origins, magnitude):
        """Append a set of origins and magnitudes to the current Event
        """
        self._origins.extend(origins)
        if not len(self.magnitudes):
            self.magnitudes = deepcopy(magnitude)
        else:
            self.magnitudes.merge(magnitude)

    @property
    def table(self):
        if self._table is not None:
            return self._table
        if len(self._origins):
            self._table = pd.DataFrame(self._origins)
            return self._table
        else:
            return None

    @classmethod
    def from_dframes(cls, origins, magnitudes):
        identifier = origins["master_id"].iloc[0]
        orig_list = [val for i, val in origins.iterrows()]
        magnitudes = Magnitude.from_table(magnitudes)
        return cls(identifier, orig_list, magnitudes)


class EventSet():
    """
    Handles a set of Event objects (not necessarily the same as a catalogue)
    """
    def __init__(self, identifier: str, events: List):
        """
        Args:
            identifier: Identifier of the event set
            events: List of events as Event objects
        """
        self.id = identifier
        if events is not None:
            self.events = events
        else:
            self.events = []
        self._event_table = None
        self._magnitude_table = None

    def __repr__(self):
        return "EventSet %s (%g events)" % (self.id, len(self))

    def __len__(self):
        return len(self.events)

    def __getitem__(self, i: int):
        return self.events[i]

    def __iter__(self):
        for event in self.events:
            yield event

    @property
    def event_table(self):
        """Cache the table of event origins as a single dataframe
        """
        if self._event_table is None:
            self._event_table = pd.concat(
                [event.table for event in self.events], ignore_index=True
            )
            self._event_table["agency"] = [self.id] * len(self)
        return self._event_table

    @property
    def magnitude_table(self):
        """Cache the table of magnitudes as a single dataframe
        """
        if self._magnitude_table is None:
            self._magnitude_table = pd.concat([event.magnitudes.table
                                               for event in self.events],
                                              ignore_index=True)
        return self._magnitude_table

    @classmethod
    def from_hdf5(cls, identifier: str, filename: str):
        """Instantiate the class from an hdf5 containing the `origins` table and
        `magnitudes` table
        """
        orig_table = pd.read_hdf(filename, "origins")
        mag_table = pd.read_hdf(filename, "magnitudes")
        origins = orig_table.groupby("master_id")
        magnitudes = mag_table.groupby("master_id")
        events = []
        for key in origins.groups:
            orig = origins.get_group(key)
            mag = magnitudes.get_group(key)
            events.append(Event.from_dframes(orig, mag))
        return cls(identifier, events)


class Catalogue():
    """
    General class for handling catalogues of earthquakes

    Attributes:
        id: Identifier of catalogue
        catalogue: Catalogue as dataframe
    """
    def __init__(self, source: str, events: pd.DataFrame, default_sigma_m: float = 0.0,
                 default_mtype: str = "", default_mag_list: Optional[str] = None):
        """
        Args:
            source: Catalogue source
            events: Catalogue as dataframe of [ev_id, ev_time, lon, lat, depth,
                                               M, sigma_m, mtype, mag_list]
            default_sigma_m: Default standard deviation if not provided
            default_mtype: Default magnitude type if not provided
        """
        self.id = source
        self.catalogue = events
        if "sigma_m" not in events.columns:
            self.catalogue["sigma_m"] = default_sigma_m +\
                np.zeros(events.shape[0], dtype=float)
        if "mtype" not in events.columns:
            self.catalogue["mtype"] = [default_mtype
                                       for i in range(events.shape[0])]
        if "mag_list" not in events.columns:
            self.catalogue["mag_list"] = [default_mag_list
                                          for i in range(events.shape[0])]
        # Ensure catalogue is sorted by date
        self.catalogue.sort_values("ev_time", inplace=True)
        self.catalogue = self.catalogue.reset_index()
        del self.catalogue["index"]
        self.catalogue["agency"] = [self.id] * self.catalogue.shape[0]

    def __len__(self):
        # Returns the length of the catalogue
        return self.catalogue.shape[0]

    def __getitem__(self, idx: Union[str, int]) -> Union[np.ndarray, pd.DataFrame]:
        if isinstance(idx, str):
            # Return the attribute
            return self.catalogue[idx].to_numpy()
        else:
            # Return the row
            return self.catalogue.iloc[idx, :]

    def get_event(self, event_id):
        """Returns the catalogue corresponding to a single event
        """
        return self.catalogue[self.catalogue["ev_id"] == event_id]

    def select(self, indices: Union[np.ndarray, pd.Series], reindex=True, drop=True):
        """
        Selects a subset of the catalogue according to a list (of positions)

        Args:
            indices: Array of positions or boolean array for selection
            reindex: Re-set indexes of the selected catalogue (True) or not (False)
            drop: Combined with reindex, drops the previous index
        """
        n1 = len(self)
        self.catalogue = self.catalogue.iloc[indices, :]
        if reindex:
            self.catalogue.reset_index(drop=drop, inplace=True)
        print("%g events selected from %g" % (len(self), n1))

    def filter(self, key, lower_value=None, upper_value=None, inplace=False,
               reindex=True):
        """
        Filter the catalogue according to some key and upper/lower values
        """
        catalogue = self.catalogue.copy()
        if lower_value:
            catalogue = catalogue[catalogue[key] >= lower_value]
        if upper_value:
            catalogue = catalogue[catalogue[key] <= upper_value]
        if reindex:
            catalogue.reset_index(drop=True, inplace=True)
        if inplace:
            # Update the current catalogue
            self.catalogue = catalogue
            return
        else:
            return catalogue

    def pretty_row(self, i: Union[int, str]) -> str:
        # Get the row from the object
        row = self[i]
        return "{:s} {:s} ({:.5f}, {:.5f}, {:.2f}) - {:s} {:.3f}".format(
            row.ev_id, str(row.ev_time), row.lon, row.lat, row.depth,
            row.mtype, row.M)

    def __iter__(self):
        for i in range(len(self)):
            yield self[i]

    def to_csv(self, filename: str, sep: str = ","):
        """
        Exports the catalogue to a csv file
        """
        self.catalogue.to_csv(filename, sep=sep, index=False)
        print("Written to %s" % filename)
        return

    def to_shapefile(self, shapefile):
        """
        Exports the catalogue to shapefile
        """
        geometry = gpd.GeoSeries(
            [Point(lon, lat) for (lon, lat) in zip(self["lon"], self["lat"])]
        )
        # Build geodataframe
        df = gpd.GeoDataFrame({"geometry": geometry})
        for key in self.catalogue.columns:
            if key == "ev_time":
                df[key] = [str(val) for val in self[key]]
            else:
                df[key] = self.catalogue[key]
        df.crs = {"init": "epsg:4326"}
        df.to_file(shapefile)
        print("Written to %s" % shapefile)
        return

    def to_event_set(self):
        """Returns the catalogue in the form of an event set
        """
        events = []
        for i, row in self.catalogue.iterrows():
            # Get magnitudes
            mags = [row.M]
            mtypes = [row.mtype]
            agencies = [self.id]
            sigma_m = [row.sigma_m]
            if row.mag_list:
                for mag_string in row.mag_list.split("|"):
                    mag_rep = mag_string.split()
                    agencies.append(mag_rep[0])
                    mtypes.append(mag_rep[1])
                    mags.append(float(mag_rep[2]))
                    if len(mag_rep) > 3:
                        sigma_m.append(float(mag_rep[3]))
                    else:
                        sigma_m.append(0.0)

            events.append(Event(
                row.ev_id, [row[["ev_id", "ev_time", "lon", "lat", "depth", "agency"]]],
                Magnitude(row.ev_id, np.array(mags), mtypes, agencies, np.array(sigma_m))
            ))

        return EventSet(self.id, events)


def harmonised_catalogue_csv_parser(
        catalogue_file: str, catalogue_name: str, mmin: Optional[float] = -np.inf,
        mmax: Optional[float] = np.inf,
        start_time: Optional[str] = None, end_time: Optional[str] = None,
        is_master: bool = False, input_sep=",") -> Catalogue:
    """Parses a magnitude harmonised catalogue in generic csv format to dataframe
    for merging with flatfile.

    Catalogue requires header columns ["ev_id", "ev_time", "lon", "lat", "depth", "M"]
    plus optional columns ["sigma_m", "mtype"]

    Args:
        catalogue_file: Path to the catalogue file
        mmin: Minimum magnitude to filter the catalogue
        mmax: Maximum magnitude to filter the catalogue
        start_time: Start time to filter the catalogue in format YYYY-mm-dd:THH:MM:ss.ss
        end_time: Start time to filter the catalogue in format YYYY-mm-dd:THH:MM:ss.ss
        is_master: If the catalogue is intended to be the "master" catalogue then indicate
                   True, otherwise False
        input_sep: Character to identify the separator for the catalogue file

    Returns:
        Magnitude catalogue as a Catalogue object
    """
    cat = pd.read_csv(catalogue_file, sep=input_sep)
    cat["ev_time"] = cat["ev_time"].astype(np.datetime64)
    cat = cat[(cat["M"] >= mmin) & (cat["M"] <= mmax)]
    start_time = np.datetime64(start_time) if start_time else cat["ev_time"].min()
    end_time = np.datetime64(end_time) if end_time else cat["ev_time"].max()
    cat = cat[(cat["ev_time"] >= start_time) & (cat["ev_time"] <= end_time)]
    if is_master:
        cat["master_id"] = ["MERGE_IDX_{:g}".format(i) for i in range(cat.shape[0])]

    if "sigma_m" not in cat.columns:
        # Add on a null sigma m column
        cat["sigma_m"] = np.nan + np.zeros(cat.shape[0])
    if "mtype" not in cat.columns:
        cat["mtype"] = [None] * cat.shape[0]
    return Catalogue(catalogue_name, cat)


def horus_parser(catalogue_file: str, catalogue_name: str, mmin: Optional[float] = -np.inf,
                 mmax: Optional[float] = np.inf, start_time: Optional[str] = None,
                 end_time: Optional[str] = None, is_master: bool = False,
                 input_sep="\t") -> Catalogue:
    """
    Parse the HORUS catalogue to a simple data frame with a common header set of
    [ev_id, ev_time, lon, lat, depth, M, sigma_m, mtype]

    Args:
        catalogue_file: Path to the catalogue file
        mmin: Minimum magnitude to filter the catalogue
        mmax: Maximum magnitude to filter the catalogue
        start_time: Start time to filter the catalogue in format YYYY-mm-dd:THH:MM:ss.ss
        end_time: Start time to filter the catalogue in format YYYY-mm-dd:THH:MM:ss.ss
        is_master: If the catalogue is intended to be the "master" catalogue then indicate
                   True, otherwise False
        input_sep: Character to identify the separator for the catalogue file

    Returns:
        HORUS catalogue as a Catalogue object
    """
    cat = pd.read_csv(catalogue_file, sep=input_sep)
    cat = cat[(cat["Mw"] >= mmin) & (cat["Mw"] <= mmax)]
    ev_times = []
    for i in range(cat.shape[0]):
        ev = cat.iloc[i]
        if f"{ev.Se:05.2f}" == "60.00":
            # Invalid second
            sec = 59.99
        else:
            sec = ev.Se
        if ev.Mi >= 60.0:
            minute = ev.Mi - 60.0
        else:
            minute = ev.Mi
        date_string = f"{int(ev.Year):04d}-{int(ev.Mo):02d}-{int(ev.Da):02d}"
        time_string = f"T{int(ev.Ho):02d}:{int(minute):02d}:{sec:05.2f}"
        ev_times.append(date_string + time_string)
    start_time = np.datetime64(start_time) if start_time else np.datetime64(ev_times[0])
    end_time = np.datetime64(end_time) if end_time else np.datetime64(ev_times[-1])
    ev_times = np.array(ev_times, dtype='datetime64[ms]')
    if is_master:
        master_ids = ["MERGE_IDX_{:s}".format(str(i).zfill(6))
                      for i in range(cat.shape[0])]
        output_dframe = pd.DataFrame({
            "master_id": master_ids,
            "ev_id": ["HORUS_{:g}".format(ev_id) for ev_id in cat.index],
            "ev_time": ev_times,
            "lon": cat["Lon"],
            "lat": cat["Lat"],
            "depth": cat["Depth"],
            "M": cat["Mw"],
            "sigma_m": cat["sigMw"],
            "mtype": ["Mw"] * len(cat),
        })
    else:
        output_dframe = pd.DataFrame({
            "ev_id": ["HORUS_{:g}".format(ev_id) for ev_id in cat.index],
            "ev_time": ev_times,
            "lon": cat["Lon"],
            "lat": cat["Lat"],
            "depth": cat["Depth"],
            "M": cat["Mw"],
            "sigma_m": cat["sigMw"],
            "mtype": ["Mw"] * len(cat),
        })

    output_dframe = output_dframe[(output_dframe["ev_time"] >= start_time) &
                                  (output_dframe["ev_time"] <= end_time)]
    return Catalogue(catalogue_name, output_dframe)


CATALOGUE_PARSERS = {
    "CSV": harmonised_catalogue_csv_parser,
    "HORUS": horus_parser,
}


def weak_motion_flatfile_to_cat(db):
    """
    """
    new_id_set = []
    for i in range(db.shape[0]):
        new_id = "{:s}|{:.5f}|{:.5f}".format(str(db.ev_time[i]),
                                             db.ev_lon[i],
                                             db.ev_lat[i])
        new_id_set.append(new_id)
    db["unique_event_id"] = pd.Series(new_id_set)
    db_events = db.drop_duplicates("unique_event_id", inplace=False)
    db_cat = pd.DataFrame({
        "ev_id": ["WMFF-{:s}".format(str(val)) for val in db_events["unique_event_id"]],
        "ev_time": db_events["ev_time"],
        "lon": db_events["ev_lon"],
        "lat": db_events["ev_lat"],
        "depth": db_events["ev_dep"],
        "M": db_events["ev_mag"],
        "mtype": db_events["ev_mty"],
    })
    return Catalogue("WMFF", db_cat)
