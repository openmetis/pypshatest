"""
Basic Module for creating seismicity rate maps from OQ input files
"""
from __future__ import annotations

import os
import warnings
from copy import deepcopy
from datetime import datetime, timedelta, timezone
from typing import Dict, Tuple, List, Union, Optional
import h5py
import numpy as np
from scipy.stats import distributions, wasserstein_distance
import geopandas as gpd
import rtree
import rasterio
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize, LogNorm
from shapely import geometry
from pyproj import Transformer
from openquake.baselib.node import Node
from openquake.hazardlib import nrml, geo, mfd
from openquake.hazardlib.mfd.base import BaseMFD
from openquake.hazardlib.stats import quantile_curve
from pshatest.utils import ks_weighted


try:
    from csep.core.regions import CartesianGrid2D
    from csep.models import Polygon
    from csep.core.forecasts import GriddedForecast
    from csep.core.catalogs import CSEPCatalog
    no_csep = False
except ImportError:
    warnings.warn("PyCSEP not installed. Functions interacting with it are not available")
    no_csep = True


KNOWN_MFDS = ('incrementalMFD', 'truncGutenbergRichterMFD',
              'arbitraryMFD', 'YoungsCoppersmithMFD', 'multiMFD')


def convert_mfdist(node: Node, width_of_mfd_bin: float = 0.1) -> BaseMFD:
    """
    Convert the given node into a OpenQuake Magnitude-Frequency Distribution object.

    Args:
        node: an OpenQuake node representing an MFD of kind[incrementalMFD,
              truncGutenbergRichterMFD, 'arbitraryMFD, YoungsCoppersmithMFD', 'multiMFD']
        width_of_mfd_bin: Width of the magnitude frequency distribution to be used in the case
                          of the exponantial MFDs (default = 0.1)

    Returns:
        The corresponding OpenQuake MFD class for the MFD type
    """
    [mfd_node] = [subnode for subnode in node
                  if subnode.tag.endswith(KNOWN_MFDS)]
    if mfd_node.tag.endswith('incrementalMFD'):
        return mfd.EvenlyDiscretizedMFD(
            min_mag=mfd_node['minMag'], bin_width=mfd_node['binWidth'],
            occurrence_rates=~mfd_node.occurRates)
    elif mfd_node.tag.endswith('truncGutenbergRichterMFD'):
        return mfd.TruncatedGRMFD(
            a_val=mfd_node['aValue'], b_val=mfd_node['bValue'],
            min_mag=mfd_node['minMag'], max_mag=mfd_node['maxMag'],
            bin_width=width_of_mfd_bin)
    elif mfd_node.tag.endswith('arbitraryMFD'):
        return mfd.ArbitraryMFD(
            magnitudes=~mfd_node.magnitudes,
            occurrence_rates=~mfd_node.occurRates)
    elif mfd_node.tag.endswith('YoungsCoppersmithMFD'):
        return mfd.YoungsCoppersmith1985MFD(
            min_mag=mfd_node["minMag"],
            b_val=mfd_node["bValue"],
            char_mag=mfd_node["characteristicMag"],
            char_rate=mfd_node.get("characteristicRate"),
            total_moment_rate=mfd_node.get("totalMomentRate"),
            bin_width=mfd_node["binWidth"])
    elif mfd_node.tag.endswith('multiMFD'):
        return mfd.multi_mfd.MultiMFD.from_node(mfd_node, width_of_mfd_bin)


class Grid2D(object):
    """Core class representing a spatial grid with standard gridding operations.
    This is mostly to be extended by subclasses to handle source model activity
    rates and catalogues.

    Attributes:

        llon: Western limit of the grid
        llat: Southern Limit of the grid
        ulon: Eastern limit of the grid
        ulat: Northern limit of the grid
        spcx: Grid spacing in the longitudinal axis (in decimal degrees)
        spcy: Grid spacing in the latitudinal axis (in decimal degrees)
        lons: 1D numpy array of grid longitude values
        lats: 1D numpy array of grid latitude values
        glons: 2D grid of longitude values (from meshgrid)
        glats: 2D grid of latitude values (from meshgrid)
        geo_to_cart: PyProj Transformer from geodetic coordinates to cartesian
        cart_to_geo: PyProj Transformer from cartesian coordinates to geodetic
        xylons: Cartesian coordinates of the grid longitude values
        xylats: Cartesian coordinates of the grid latitude values
        ny: Number of latitude values
        nx: Number of longitude values
        grid_tree: RTree spatial index of grid
        polygons: Cells of grid as a set of polygons in cartesian space

    """
    def __init__(
            self,
            bbox: Tuple[float, float, float, float],
            spcx: float,
            spcy: float,
            geodetic_crs: str = "EPSG:4326",
            cartesian_crs: str = "EPSG:3035",
            build_tree_polygons: bool = True
            ):
        """
        Instantiate grid with bounding box and spacings

        Args:
            bbox: Bounding box of region as tuple of (west, south, east, north) in decimal
                  degrees
            Bounding box as (llon, llat, ulon, ulat)
            spcx: Grid spacing in the longitudinal axis (in decimal degrees)
            spcy: Grid spacing in the latitudinal axis (in decimal degrees)
            geodetic_crs: Coordinate reference system to use for geodetic coordinates
                          (long., lat.) given in terms of a full EPSG code, e.g. WGS84
                          (default) is "EPSG:4326"
            cartesian_crs: Coordinate reference system to use for cartesian coordinates
                           (easting, northing) given in terms of a full EPSG code,
                           e.g. European Equal Area Projection (default) is "EPSG:3035"
            build_tree_polygons: If True then this will build the RTree for spatial indexing
                                 and the polygons, which are needed for building or adapti
                                 a grid from a source model. If False then this step is
                                 skipped, which may be more efficient if retreiving a grid
                                 from file.
        """
        # Setup basic grid
        self.llon, self.llat, self.ulon, self.ulat = bbox
        self.spcx = spcx
        self.spcy = spcy
        self.lons = np.arange(self.llon, self.ulon + spcx, spcx)
        self.lats = np.arange(self.llat, self.ulat + spcy, spcy)
        self.glons, self.glats = np.meshgrid(self.lons, self.lats)
        self.geo_to_cart = Transformer.from_crs(geodetic_crs, cartesian_crs, always_xy=True)
        self.cart_to_geo = Transformer.from_crs(cartesian_crs, geodetic_crs, always_xy=True)
        # Transform grid to a cartesian reference frame
        self.xylons, self.xylats = self.transform(self.glons, self.glats)
        self.ny, self.nx = self.glons.shape
        self.ny -= 1
        self.nx -= 1
        # Build rtree
        if build_tree_polygons:
            self.grid_tree = rtree.index.Index()
            cntr = 0
            print("Building rtree ...")
            for i in range(self.ny):
                for j in range(self.nx):
                    bbox = (self.xylons[i, j], self.xylats[i, j],
                            self.xylons[i, j + 1], self.xylats[i + 1, j])
                    self.grid_tree.insert(cntr, bbox)
                    cntr += 1
            print("done")
            # Building polygon set
            self.polygons = self.to_polygon_set(as_xy=True)
        else:
            self.grid_tree = None
            self.polygons = None
        self._areas = None

    def transform(
            self,
            x: np.ndarray,
            y: np.ndarray,
            xy2geo: bool = False
            ) -> Tuple[np.ndarray, np.ndarray]:
        """Transforms a set of coordinates between geodetic and cartesian

        Args:
            x: X-values of grid (either longitudes or cartesian X)
            y: Y-values of grid (either latitudes or cartesian Y)
            xy2geo: Convert cartesian to geodetic (True) or geodetic to cartesian (False)

        Returns:
            Tuple of [X, Y] or [Longitude, Latitude]
        """
        if xy2geo:
            return self.cart_to_geo.transform(x, y)
        else:
            return self.geo_to_cart.transform(x, y)

    def __repr__(self):
        # Basic string to represent grid attributes
        return "GRID:{:.3f}/{:.3f}/{:.3f}/{:.3f} ({:.3f}, {:.3f})".format(
            self.llon, self.llat, self.ulat, self.ulon, self.spcx, self.spcy)

    def to_polygon_set(self, as_xy: bool = False) -> List:
        """Transforms the grid into a set of shapely polygons

        Args:
            as_xy: Create the polygon out of the Cartesian coordinates (True) or geodetic
                   coordinates (False)

        Returns:
            polygons: Grid cells as a list of Shapely polygons
        """
        polygons = []
        for i in range(self.ny):
            for j in range(self.nx):
                if as_xy:
                    polygon = [
                        (self.xylons[i, j], self.xylats[i, j]),
                        (self.xylons[i + 1, j], self.xylats[i + 1, j]),
                        (self.xylons[i + 1, j + 1], self.xylats[i + 1, j + 1]),
                        (self.xylons[i, j + 1], self.xylats[i, j + 1]),
                        (self.xylons[i, j], self.xylats[i, j])
                    ]
                else:
                    polygon = [
                        (self.glons[i, j], self.glats[i, j]),
                        (self.glons[i + 1, j], self.glats[i + 1, j]),
                        (self.glons[i + 1, j + 1], self.glats[i + 1, j + 1]),
                        (self.glons[i, j + 1], self.glats[i, j + 1]),
                        (self.glons[i, j], self.glats[i, j])
                    ]
                polygons.append(geometry.Polygon(polygon))
        return polygons

    @property
    def areas(self):
        """Returns the areas of the grid cells (in km ^ 2)
        """
        if self._areas is not None:
            return self._areas
        dlons = self.xylons[:, 1:] - self.xylons[:, :-1]
        dlats = self.xylats[1:, :] - self.xylats[:-1, :]
        self._areas = 1.0E-6 * (dlons[:-1, :] * dlats[:, :-1])
        return self._areas

    @property
    def area(self):
        """Returns the total area of the grid
        """
        return np.sum(self.areas)


class RateGrid2D(Grid2D):
    """
    2D grid for storing seismicity rate information extracted from a source
    model or a catalogue

    Attributes:
        mmin: Minimum magnitude of the magnitude bins
        mmax: Maximum magnitude of the magnitude bins
        dm: Magnitude bin spacing
        mbins: Edges of the magnitude bins
        nm: Number of magnitude bins
        rates: 3D array containing the activity rates per longitude, latitude and
               magnitude bin
        flt_buffer: Distance to buffer around the surface projection of a fault in order
                    to distribute its seismicity rate to the grid (default = 0.0)
        weight: Weight attributed to this grid (default = 1.0)
        name: Name to associate to the grid
    """
    def __init__(
            self,
            bbox: Tuple[float, float, float, float],
            spcx: float,
            spcy: float,
            mmin: float,
            mmax: float,
            dm: float,
            geodetic_crs: str = "EPSG:4326",
            cartesian_crs: str = "EPSG:3035",
            flt_buffer: float = 0.0,
            weight: float = 1.0,
            name: str = "",
            build_tree_polygons: bool = True
            ):
        """Instantiate with same data as `Grid2D` plus the magnitude range information
        """
        super().__init__(bbox, spcx, spcy, geodetic_crs, cartesian_crs,
                         build_tree_polygons = build_tree_polygons)
        # Add on 3D seismicity rate grid
        self.mmin, self.mmax, self.dm = mmin, mmax, dm
        self.mbins = np.arange(self.mmin, self.mmax + self.dm, self.dm)
        self.nm = len(self.mbins) - 1
        self.rates = np.zeros([self.ny, self.nx, self.nm])
        self.flt_buffer = flt_buffer
        self.weight = weight
        self.name = name

    def __repr__(self):
        # Basic string to represent grid attributes
        return "RATE GRID:{:.3f}/{:.3f}/{:.3f}/{:.3f}/{:.2f}/{:.2f} ({:.3f}, {:.3f}, {:.2f})".\
            format(self.llon, self.llat, self.ulat, self.ulon,
                   self.mmin, self.mmax, self.spcx, self.spcy, self.dm)

    @classmethod
    def from_hdf5(
            cls,
            fle: Union[h5py.File, h5py.Group, h5py.Dataset],
            geodetic_crs: str = "EPSG:4326",
            cartesian_crs: str = "EPSG:3035",
            name: str = "",
            build_tree_polygons: bool = True
            ) -> Grid2D:
        """Builds a rate grid from an hdf5 object with information stored

        Args:
            fle: Grid of information as either a top level h5py.File object or a
                 h5py.Group object
            name: If set then this will over-ride the name found in the hdf5 store
        Returns:
            Instance of the grid
        """
        bbox = (fle.attrs["llon"], fle.attrs["llat"],
                fle.attrs["ulon"], fle.attrs["ulat"])
        spcx, spcy = fle.attrs["spcx"], fle.attrs["spcy"]
        mmin, mmax, dm = fle.attrs["lm"], fle.attrs["um"], fle.attrs["dm"]
        weight = fle.attrs["weight"]
        if not name and ("name" in list(fle.attrs)):
           name = fle.attrs["name"]
        grd = cls(bbox, spcx, spcy, mmin, mmax, dm, geodetic_crs, cartesian_crs,
                  weight=weight, name=name, build_tree_polygons=build_tree_polygons)
        grd.rates = fle[:]
        return grd

    def to_hdf5(self, fle: Union[h5py.File, h5py.Group, h5py.Dataset], key: str):
        """Exports the rate grid to an hdf5 object

        Args:
            fle: Target for storing the grid as either a top level h5py.Filr object or a
                 h5py.Group object
        """
        dset = fle.create_dataset(key, self.rates.shape, dtype="f")
        dset[:] = self.rates
        dset.attrs["llon"] = self.llon
        dset.attrs["llat"] = self.llat
        dset.attrs["ulon"] = self.ulon
        dset.attrs["ulat"] = self.ulat
        dset.attrs["spcx"] = self.spcx
        dset.attrs["spcy"] = self.spcy
        dset.attrs["lm"] = self.mmin
        dset.attrs["um"] = self.mmax
        dset.attrs["dm"] = self.dm
        dset.attrs["weight"] = self.weight
        dset.attrs["name"] = self.name
        return

    def to_geotiff(
            self,
            gtiff_file: str,
            mmin: float,
            mmax: float = np.inf,
            crs: str = "EPSG:4326"
            ):
        """Exports a 2D rate grid to geotiff summing the rates between the upper and lower
           magnitudes

        Args:
            gtiff_file: Path to geotiff file for export
            mmin: Minimum magnitude
            mmax: Maximum magnitude (default to np.inf)
            crs: Coordinate reference system for exported geotiff file (default EPSG:4326)
        """
        output_grid = self.get_rates(mmin, mmax)
        height, width = output_grid.shape
        transform = rasterio.transform.from_origin(self.llon, self.ulat,
                                                   self.spcx, self.spcy)
        with rasterio.open(gtiff_file, "w", driver="GTiff", height=height,
                           width=width, count=1, dtype=output_grid.dtype,
                           crs=crs, transform=transform, nodata=0.0) as dst:
            dst.write(np.flipud(output_grid), 1)
        return

    def _parse_source(self, src_node: Node, width_of_mfd_bin: float):
        """Adds the source to the grid depending on the source type

        Args:
            src_node: Seismogenic source as an instance of :class:`openquake.baselib.node.Node`
            grid: Grid to add the source rates.

        """
        if "areaSource" in src_node.tag:
            self.add_area_source(src_node, width_of_mfd_bin)
        elif "multiPointSource" in src_node.tag:
            self.add_multipoint_source(src_node, width_of_mfd_bin)
        elif "simpleFaultSource" in src_node.tag:
            self.add_simple_fault_source(src_node, width_of_mfd_bin)
        elif "pointSource" in src_node.tag:
            self.add_point_source(src_node, width_of_mfd_bin)
        else:
            warnings.warn("Source Type %s not supported - skipping" % src_node.tag)
        return

    def parse_source_model(
            self,
            source_file: str,
            source_model_id: Optional[str] = "rate",
            weight: float = 1.0,
            dstore: Optional[Union[h5py.File, h5py.Group]] = None,
            width_of_mfd_bin: float = 0.1):
        """Parses an entire source model from an OpenQuake source model file

        Args:
            source_file: Path to source model file (.xml)
            source_model_id: ID of the source model, if serializing to a hdf5 datastore
            weight: Weight of the source model
            dstore: Open hdf5 datastore for serialization (either h5py.File object or
                    h5py.Group)
            width_of_mfd_bin: Width of the magnitude bin to use for parametric MFDs
        """
        src_model = nrml.read(source_file)
        self.name = src_model.sourceModel["name"]
        # Check whether it is 0.4 or 0.5
        if src_model["xmlns"].endswith("0.5"):
            # NRML 0.5 - contains source groups
            for src_grp in src_model.sourceModel:
                for src_node in src_grp:
                    self._parse_source(src_node, width_of_mfd_bin)
        elif src_model["xmlns"].endswith("0.4"):
            # NRML 0.4 - no source groups, just loop over sources
            for src_node in src_model.sourceModel:
                self._parse_source(src_node, width_of_mfd_bin)
        else:
            raise ValueError("%s not an OpenQuake nrml source model!"
                             % src_model["xmlns"])
        self.weight = weight
        # Serialize the grid to a datastore
        if dstore:
            self.to_hdf5(dstore, source_model_id)
        return

    def add_area_source(
            self,
            src_node: Node,
            width_of_mfd_bin: Optional[float] = None
            ):
        """Adds the rate of magnitudes in each bin from an area source, with the
           rate per grid cell proportional to the area of the area source polygon in
           each grid cell.

        Args:
            src_node: Area source as :class:`openquake.baselib.node.Node` object read in
                      from nrml
            width_of_mfd_bin: If the area source has a parametric MFD then this defines the
                              width of the MFD bin. Ideally this should be the same as the
                              `dm` attribute of the rate grid

        """
        assert "areaSource" == src_node.tag.partition("}")[-1]
        # Get the polygon in cartesian coordinates
        polygon = self.get_xy_polygon_from_area_geometry(src_node)
        indices, areas, weights = self._distribute_polygon_to_grid(polygon)
        if not len(indices):
            # Area does not intersect grid
            return
        # Get mfd
        mfd_bin = width_of_mfd_bin if width_of_mfd_bin else self.dm
        mfd0 = convert_mfdist(src_node, mfd_bin)
        mags, src_rates = zip(*mfd0.get_annual_occurrence_rates())
        mags = np.array(mags)
        src_rates = np.array(src_rates)
        t_rate = np.zeros([self.ny * self.nx, self.nm])
        for i, (lm, um) in enumerate(zip(self.mbins[:-1], self.mbins[1:])):
            midx = np.logical_and(mags >= lm, mags < um)
            mag_rate = np.sum(src_rates[midx])
            t_rate[indices, i] += (mag_rate * weights)
        for i in range(self.nm):
            self.rates[:, :, i] += np.reshape(t_rate[:, i], [self.ny, self.nx])
        return

    def _distribute_polygon_to_grid(self, polygon: geometry.Polygon) \
            -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Works out which grid cells the polygons will be distributed to, the
        area of the polygon in each cell and the corresponding weight that will
        be used to distributed the rates

        Args:
            polygon: Area source geometry or surface projection of fault as shapely Polygon

        Returns:
            indices: Pointer to the grid cells that intersect with the polygon
            areas: Area of the polygon inside each grid cell
            weights: Corresponding weights to mulitply the activity rate based on area
        """
        # Get indices
        indices = list(self.grid_tree.intersection(polygon.bounds))
        if not len(indices):
            # Area does not intersect grid
            return [], [], []
        indices = []
        areas = []
        for idx in self.grid_tree.intersection(polygon.bounds):
            area = polygon.intersection(self.polygons[idx]).area
            if area:
                indices.append(idx)
                areas.append(area)
        areas = np.array(areas)
        indices = np.array(indices)
        weights = areas / np.sum(areas)
        return indices, areas, weights

    def add_multipoint_source(self, src_node: Node, width_of_mfd_bin: Optional[float] = None):
        """Adds the rates from a multiPointSource node

        Args:
            src_node: Multipoint source as :class:`openquake.baselib.node.Node` object read in
                      from nrml
        """
        assert "multiPointSource" == src_node.tag.partition("}")[-1]
        # Get the lons and lats
        npnts = len(src_node.multiPointGeometry.posList.text) // 2
        lons, lats = zip(
            *[(src_node.multiPointGeometry.posList.text[i],
               src_node.multiPointGeometry.posList.text[i + 1])
              for i in range(0, 2 * npnts, 2)])
        width_of_mfd_bin = width_of_mfd_bin if width_of_mfd_bin else self.dm
        mfd0 = mfd.multi_mfd.MultiMFD.from_node(src_node.multiMFD,
                                                width_of_mfd_bin)
        # Get grid locations
        dx = ((lons - self.lons[0]) / self.spcx).astype(int)
        dy = ((lats - self.lats[0]) / self.spcy).astype(int)
        for (dxi, dyi, mfdi) in zip(dx, dy, list(mfd0)):
            if (dxi < 0) or (dyi < 0):
                # Outside of the box
                continue
            if (dxi >= self.rates.shape[1]) or (dyi >= self.rates.shape[0]):
                # Outside of bbox
                continue
            mags, rates = zip(*mfdi.get_annual_occurrence_rates())
            mx = ((mags - self.mbins[0]) / self.dm).astype(int)
            idx = np.logical_and(mags >= self.mbins[0], mags <= self.mbins[-1])
            self.rates[dyi, dxi, mx[idx]] += np.array(rates)[idx]
        return

    def add_point_source(self, src_node: Node, width_of_mfd_bin: Optional[float] = None):
        """Adds the rates from a PointSource

        Args:
            src_node: Point source as :class:`openquake.baselib.node.Node` object read in
                      from nrml
        """
        assert "pointSource" == src_node.tag.partition("}")[-1]
        lon, lat = src_node.pointGeometry.pos.text
        dx = ((lon - self.lons[0]) / self.spcx).astype(int)
        dy = ((lat - self.lats[0]) / self.spcy).astype(int)
        if (dx >= self.rates.shape[1]) or (dy >= self.rates.shape[0]) or\
                (dx < 0) or (dy < 0):
            # Outside of bbox
            return
        mfd_bin = width_of_mfd_bin if width_of_mfd_bin else self.dm
        mfd0 = convert_mfdist(src_node, mfd_bin)
        mags, rates = zip(*mfd0.get_annual_occurrence_rates())

        mx = ((mags - self.mbins[0]) / self.dm).astype(int)

        self.rates[dy, dx, mx] += np.array(rates)
        return

    def add_simple_fault_source(
            self,
            src_node: Node,
            width_of_mfd_bin: Optional[float] = None
            ):
        """Adds the rates from a simple fault source

        Args:
            src_node: SimpleFaultSource as :class:`openquake.baselib.node.Node` object read in
                      from nrml
        """
        assert "simpleFaultSource" in src_node.tag
        # Get the polygon in cartesian coordinates
        dip = src_node.simpleFaultGeometry.dip.text
        if (np.fabs(dip - 90.) < 1.0) and not self.flt_buffer:
            # (near) vertical dipping fault - add a 1 km buffer if there is
            # no other buffer defines
            polygon = self.get_xy_polygon_from_fault_geometry(src_node, 1.0)
        else:
            polygon = self.get_xy_polygon_from_fault_geometry(src_node, 0.0)
        indices, areas, weights = self._distribute_polygon_to_grid(polygon)
        if not len(indices):
            # Area does not intersect grid
            return
        # Get mfd
        mfd_bin = width_of_mfd_bin if width_of_mfd_bin else self.dm
        mfd0 = convert_mfdist(src_node, mfd_bin)
        mags, src_rates = zip(*mfd0.get_annual_occurrence_rates())
        mags = np.array(mags)
        src_rates = np.array(src_rates)
        t_rate = np.zeros([self.ny * self.nx, self.nm])
        for i, (lm, um) in enumerate(zip(self.mbins[:-1], self.mbins[1:])):
            midx = np.logical_and(mags >= lm, mags < um)
            mag_rate = np.sum(src_rates[midx])
            t_rate[indices, i] += (mag_rate * weights)
        for i in range(self.nm):
            self.rates[:, :, i] += np.reshape(t_rate[:, i], [self.ny, self.nx])
        return

    def reset(self):
        """Returns the rates to zero
        """
        self.rates = np.zeros(self.rates.shape)

    def get_xy_polygon_from_area_geometry(self, src: Node) -> geometry.Polygon:
        """Retrieves the polygon from the area geometry and transforms it into Cartesian
        coordinates

        Args:
            src: Area source node

        Returns:
            Cartesian geometry of polygon as Shapely Polygon
        """
        coords = src.areaGeometry.Polygon.exterior.LinearRing.posList.text
        # Get shapely geometry
        lons = []
        lats = []
        for i in range(0, len(coords) - 1, 2):
            lons.append(coords[i])
            lats.append(coords[i + 1])
        # Close polygon
        lons.append(coords[0])
        lats.append(coords[1])
        xvals, yvals = self.transform(lons, lats)
        return geometry.Polygon([(x, y) for x, y in zip(xvals, yvals)])

    def get_xy_polygon_from_fault_geometry(
            self,
            src: Node,
            flt_buffer: float = 0.0
            ) -> geometry.Polygon:
        """Retrieves the polygon from the surface projection of the fault, buffers it if
        required and then transforms it into Cartesian coordinates

        Args:
            src: Simple fault source node
            flt_buffer: Number of km to buffer the surface projection

        Returns:
            Cartesian geometry of polygon as Shapely Polygon
        """
        assert "simpleFaultSource" in src.tag
        if not flt_buffer:
            flt_buffer = self.flt_buffer
        coords = src.simpleFaultGeometry.LineString.posList.text
        trace = geo.Line([geo.Point(coords[i], coords[i + 1])
                          for i in range(0, len(coords), 2)])
        dip = src.simpleFaultGeometry.dip.text
        usd = src.simpleFaultGeometry.upperSeismoDepth.text
        lsd = src.simpleFaultGeometry.lowerSeismoDepth.text
        surface = geo.SimpleFaultSurface.from_fault_data(trace, usd, lsd, dip,
                                                         0.5)
        lons, lats = surface.get_surface_boundaries()
        # Transform
        xvals, yvals = self.transform(lons, lats)
        poly = geometry.Polygon([(x, y) for x, y in zip(xvals, yvals)])
        if flt_buffer:
            # Buffer in km - conver to m
            poly = poly.buffer(flt_buffer * 1000.)
        return poly

    def get_rates(self, mmin: float, mmax: float = np.inf) -> np.ndarray:
        """Returns the total rates between the mmin and mmax

        Args:
            mmin: Minimum magnitude for rates
            mmax: Maximum magnitude for rates

        Returns:
            Sum of rates along the magnitude axis between mmin and mmax
        """
        idx = np.logical_and(self.mbins[:-1] >= mmin,
                             self.mbins[1:] < mmax)
        return np.sum(self.rates[:, :, idx], axis=2)

    def _check_grid_equality(self, rate_grid):
        """Checks that a second grid object has the same grid properties as the current one
        """
        assert type(rate_grid) is type(self)
        # Check that grids are of equal dimension
        assert np.allclose(rate_grid.lons, self.lons, 7)
        assert np.allclose(rate_grid.lats, self.lats, 7)
        assert np.allclose(rate_grid.mbins, self.mbins, 7)

    def get_diff_rates(self, igrid, absolute: bool = True) -> np.ndarray:
        """Returns the difference between the activity rates in the current grid and those
        in another grid of same dimension

        Args:
            igrid: Second grid as instance of `RateGrid`
            absolute: Returns the absolute difference betweem the two grids (True) or
                      the percentage change (False)

        Returns:
            Difference between the two rate grids as 3D numpy array
        """
        # Check that input grid has same dimensions
        self._check_grid_equality(igrid)
        if absolute:
            diff_grid = igrid.rates - self.rates
        else:
            # As percentage
            diff_grid = 100.0 * ((igrid.rates / self.rates) - 1.)
        return diff_grid

    def __iadd__(self, rates: Union[Union[int, float], RateGrid2D]):
        # Add a second rate grid in place
        if isinstance(rates, (int, float)):
            self.rates += rates
            return self
        # Check that input grid has same dimensions
        self._check_grid_equality(rates)
        self.rates += rates.rates
        return self

    def __isub__(self, rates: Union[Union[int, float], RateGrid2D]):
        # Subtract a second rate grid in place
        if isinstance(rates, (int, float)):
            self.rates -= rates
            return self
        self._check_grid_equality(rates)
        self.rates -= rates.rates
        return self

    def __imul__(self, rates: Union[Union[int, float], RateGrid2D]):
        # Multiple a second rate grid in place
        if isinstance(rates, (int, float)):
            self.rates *= rates
            return self
        self._check_grid_equality(rates)
        self.rates *= rates.rates
        return self

    def __itruediv__(self, rates: Union[Union[int, float], RateGrid2D]):
        # Multiple a second rate grid in place
        if isinstance(rates, (int, float)):
            self.rates /= rates
            return self
        self._check_grid_equality(rates)
        self.rates /= rates.rates
        return self

    def __ipow__(self, rates: Union[Union[int, float], RateGrid2D]):
        # Raise a grid to the power of a second grid in place
        if isinstance(rates, (int, float)):
            self.rates **= rates
            return self
        # Check that input grid has same dimensions
        self._check_grid_equality(rates)
        self.rates **= rates.rates
        return self

    def __add__(self, rates: Union[Union[int, float], RateGrid2D]) -> RateGrid2D:
        """Adds two rate grids together and returns a new `RateGrid2D` object
        """
        if isinstance(rates, self.__class__):
            self._check_grid_equality(rates)
        # Create a new grid by deepcopying the current one
        new_rates = deepcopy(self)
        # Update the rates
        new_rates += rates
        return new_rates

    def __sub__(self, rates: Union[Union[int, float], RateGrid2D]) -> RateGrid2D:
        """Takes the input rate grid from the current and returns a new `RateGrid2D`
        object
        """
        if isinstance(rates, self.__class__):
            self._check_grid_equality(rates)
        # Create a new grid by deepcopying the current one
        new_rates = deepcopy(self)
        # Update the rates
        new_rates -= rates
        return new_rates

    def __mul__(self, rates: Union[Union[int, float], RateGrid2D]) -> RateGrid2D:
        """Multiplies two rate grids together and returns a new `RateGrid2D` object
        """
        if isinstance(rates, self.__class__):
            self._check_grid_equality(rates)
        # Create a new grid by deepcopying the current one
        new_rates = deepcopy(self)
        # Update the rates
        new_rates *= rates
        return new_rates

    def __truediv__(self, rates: Union[Union[int, float], RateGrid2D]) -> RateGrid2D:
        """Divides the current rate grid by the input one and returns a new `RateGrid2d` object
        """
        if isinstance(rates, self.__class__):
            self._check_grid_equality(rates)
        # Create a new grid by deep copying the current one
        new_rates = deepcopy(self)
        # Update the rates
        new_rates /= rates
        return new_rates

    def __pow__(self, rates: Union[Union[int, float], RateGrid2D]) -> RateGrid2D:
        """Raises the current rate grid to the power of the input one and returns a new
        `RateGrid2D` object
        """
        if isinstance(rates, self.__class__):
            self._check_grid_equality(rates)
        # Create a new grid by deep copying the current one
        new_rates = deepcopy(self)
        # Update the rates
        new_rates **= rates
        return new_rates

    def to_csep_forecast(self, mmin: float = -np.inf, mmax: float = np.inf,
                         start_time: Optional[str] = None,
                         end_time: Optional[str] = None,
                         name: str = "") -> GriddedForecast:
        """Exports the grid to a PyCSEP gridded forecast object

        Args:
            mmin: Minimum magnitude of forecast to be exported
            mmax: Maximum magnitude of forecast to be exported
            start_time: Start time of forecast
            end_time: End time of forecast

        Returns:
             Grid as PyCSEP Gridded Forecast
        """
        if no_csep:
            raise OSError("CSEP function 'to_csep_forecast' is not available")
        # Get the reference grid
        gx, gy = np.meshgrid(self.lons[:-1], self.lats[:-1])
        cartesian_grid = CartesianGrid2D.from_origins(
            np.column_stack([gx.flatten(), gy.flatten()]),
            dh=self.spcy
        )
        # Get the rates
        idx = np.where(np.logical_and(self.mbins[:-1]  >= mmin,
                                      self.mbins[:-1] < mmax))[0]
        mags = self.mbins[:-1][idx]
        reshaped_rates = np.zeros([self.nx * self.ny, len(idx)])
        for i, loc in enumerate(idx):
            reshaped_rates[:, i] = self.rates[:, :, loc].flatten()
        # For CSEP purposes the start time and end time should correspond to
        # the duration to which the rates refer, which in this case is annual
        if end_time:
            end_time = datetime.fromisoformat(end_time)
        else:
            end_time - datetime.now()
        if start_time:
            start_time = datetime.fromisoformat(start_time)
        else:
            start_time = end_time - timedelta(hours=(365 * 24) + 6)
        if not name:
            name = self.name
        return GriddedForecast(
            start_time,
            end_time,
            magnitudes=mags,
            name=name,
            region=cartesian_grid,
            data=reshaped_rates)

    def plot(
            self,
            mmin: Optional[float] = None,
            mmax: Optional[float] = None,
            vmin: Optional[float] = None,
            vmax: Optional[float] = None,
            figure_size: Tuple[float, float] = (12.0, 12.0),
            ax: Optional[plt.Artist] = None,
            filename: str = "",
            dpi: int = 300,
            **kwargs
            ) -> plt.Artist:
        """Creates a plot of the total activity rate between a minimum and maximum magnitude

        Args:
            mmin: Minimum magnitude for total rate (will take mmin of the magnitude bins
                  if not specified)
            mmax: Maximum magnitude for total rate (will set no upper limit if not specified)
            vmin: Minimum value (rate) for the color scale
            vmax: Maximum value (rate) for the color scale
            figure_size: Size of the figure (if creating a new plot)
            ax: Artist to add the plot to
            filename: Name of file to export the plot
            dpi: Dots per inch for exported figure

        Returns:
            Current figure artist
        """
        mmin = self.mmin if mmin is None else mmin
        mmax = np.inf if mmax is None else mmax
        rates = self.get_rates(mmin, mmax)
        rates[rates == 0.0] = np.nan

        if vmin is None:
            vmin = 10.0 ** np.floor(np.nanmin(np.log10(rates)))
        if vmax is None:
            vmax = 10.0 ** np.ceil(np.nanmax(np.log10(rates)))
        rate_norm = LogNorm(vmin, vmax)
        if np.all(np.isnan(rates)):
            raise ValueError("No earthquakes with M > %.3f" % mmin)
        if not ax:
            fig = plt.figure(figsize=figure_size)
            ax = fig.add_subplot(111)

        cax = ax.pcolormesh(self.glons, self.glats, rates,
                            norm=rate_norm, **kwargs)
        cbar = plt.colorbar(cax, pad=0.02)
        cbar.ax.set_ylabel(
            "Annual Rate per %.1f x %.1f Cell" % (self.spcx, self.spcy),
            fontsize=18)
        cbar.ax.tick_params(labelsize=14)
        ax.set_xlabel("Longitude, deg", fontsize=18)
        ax.set_ylabel("Latitude, deg", fontsize=18)
        ax.grid(which="both")
        ax.set_xlim(self.llon, self.ulon)
        ax.set_ylim(self.llat, self.ulat)
        ax.tick_params(labelsize=14)
        ax.axis('equal')
        if filename:
            filetype = os.splitext(filename)[1]
            plt.savefig(filename, format=filetype, dpi=dpi, bbox_inches="tight")
        return ax

    def plot_diff(
            self,
            igrid,
            mmin: Optional[float] = None,
            mmax: Optional[float] = None,
            absolute: bool = True,
            vmin: Optional[float] = None,
            vmax: Optional[float] = None,
            log_color_scale: bool = False,
            figure_size: Tuple[float, float] = (12.0, 12.0),
            ax: Optional[plt.Artist] = None,
            filename: str = "",
            dpi: int = 300,
            **kwargs
            ) -> plt.Artist:
        """Plots the difference maps between the current model and another `RateGrid2D`

        Args:
            igrid: Grid for comparison as instance of :class:`RateGrid2D`
            mmin: Minimum magnitude for comparison (will take mmin of current grid if not
                  specified)
            mmax: Maximum magnitude for comparison (will be unbounded if not specified)
            absolute: Takes the absolute difference (True) or relative change as a percentage
                      (False)
            vmin: Minimum value of color scale
            vmax: Maximum value of color scale
            log_color_scale: Will use a logarithmic color scale (True) or linear (False)
            figure_size: Size of the figure (if creating a new plot)
            ax: Artist to add the plot to
            filename: Name of file to export the plot
            dpi: Dots per inch for exported figure

        Returns:
            Current figure artist

        """
        self._check_grid_equality(igrid)
        mmin = self.mmin if mmin is None else mmin
        mmax = np.inf if mmax is None else mmax
        idx = np.logical_and(self.mbins[:-1] >= mmin,
                             self.mbins[1:] <= mmax)
        # Get the total rates from the two grids
        orig_rates = np.sum(self.rates[:, :, idx], axis=2)
        new_rates = np.sum(igrid.rates[:, :, idx], axis=2)
        if absolute:
            # Absolute difference
            diff_rates = new_rates - orig_rates
        else:
            # As percentage change
            diff_rates = 100.0 * ((new_rates / orig_rates) - 1.)
        vmin = np.nanmin(diff_rates) if vmin is None else vmin
        vmax = np.nanmax(diff_rates) if vmax is None else vmax

        if log_color_scale:
            color_norm = LogNorm(vmin, vmax)
        else:
            color_norm = Normalize(vmin, vmax)
        if not ax:
            fig = plt.figure(figsize=figure_size)
            ax = fig.add_subplot(111)
        cax = ax.pcolormesh(self.glons, self.glats, diff_rates,
                            norm=color_norm, **kwargs)
        cbar = plt.colorbar(cax, pad=0.02)
        cbar.ax.tick_params(labelsize=14)
        ax.set_xlabel("Longitude, deg", fontsize=18)
        ax.set_ylabel("Latitude, deg", fontsize=18)
        ax.grid(which="both")
        ax.set_xlim(self.llon, self.ulon)
        ax.set_ylim(self.llat, self.ulat)
        ax.tick_params(labelsize=14)
        ax.axis('equal')
        if filename:
            filetype = os.splitext(filename)[1]
            plt.savefig(filename, format=filetype, dpi=dpi,
                        bbox_inches="tight")
        return ax


def extract_source_model_logic_tree_branches(
        source_model_logic_tree_file: str,
        source_folder: str = ".") ->\
        Tuple[List, List, List]:
    """Retreives the source model files, IDs and weights from an OpenQuake source model logic
    tree file in xml format

    Args:
        source_model_logic_tree_file: Path to OpenQuake source model logic tree file

    Returns:
        source_model_files: List of source model files
        source_model_ids: List of source model IDs
        weights: List of source model weights
    """
    source_model_files = []
    source_model_ids = []
    weights = []
    smlt = nrml.read(source_model_logic_tree_file)
    for branch_set in smlt.logicTree:
        for branch in branch_set:
            branch_id = branch["branchID"]
            # Replace whitespace in the branch IDs with underscores
            branch_id = branch_id.replace(" ", "_")
            source_model_ids.append(branch_id)
            # Get the source file and the weight from the uncertainty model
            branch_file = branch.uncertaintyModel.text
            source_files = []
            for fname in branch_file.split():
                source_files.append(os.path.join(source_folder, fname))
            source_file = " ".join(source_files)
            weight = float(branch.uncertaintyWeight.text)
            source_model_files.append(source_file)
            weights.append(weight)
    return source_model_files, source_model_ids, weights


def build_datastore(datastore: str, grid: RateGrid2D, source_model_files: List,
                    source_model_ids: List, weights: List,
                    width_of_mfd_bin: str = 0.1):
    """Builds the datastore from the lists of files, IDs and weights

    Args:
        grid: Rate grid common to all the source files
        datastore: Path to the hdf5 datastore file
        source_model_files: List of source models files where each source model file can
                            be a single file path or a space delimited list of files
        source_model_ids: Identifiers for each source. This will identify each specific
                          source model in the data store
        weights: Weights of the source models (if not specified then each source will
                 be evenly weighted with 1 / num_source_models)
        width_of_mfd_bin: Width of the MFD bins for exponential parametric recurrence
                          models
    """
    # Ensure we start with an empty grid
    grid.reset()
    if os.path.exists(datastore):
        raise OSError("Datastore %s already exists" % datastore)
    # Open the datastore
    dstore = h5py.File(datastore, "a")
    for source_files, source_model_id, weight in\
            zip(source_model_files, source_model_ids, weights):
        subfiles = source_files.split()
        if len(subfiles) > 1:
            # When the source model is constructed from multiple files
            # then need to load one-by-one before persisting to datastore
            for source_file in subfiles:
                grid.parse_source_model(
                    subfiles[0],
                    source_model_id,
                    weight,
                    dstore=None,
                    width_of_mfd_bin=width_of_mfd_bin)
            grid.to_hdf5(dstore, source_model_id)
        else:
            # If there is just one source model file then this is persisted
            # directly to datastore
            source_file = subfiles[0]
            grid.parse_source_model(
                source_file,
                source_model_id,
                weight,
                dstore,
                width_of_mfd_bin)

        if len(source_file) > 50:
            print("Source file %s ... %s added to datastore (%s)"
                  % (source_file[:20], source_file[-20:], datastore))
        else:
            print("Source file %s added to datastore (%s)"
                  % (source_file, datastore))
        # Reset the grid
        grid.reset()
        grid.weight = 1.0
    dstore.close()
    return


class RateGrid2DSet():
    """Class to handle a set of activity rate grids and their corresponding weights

    Attributes:
        datastore: Path to the datastore file
        models: List of model IDs
        weights: List of model weights

    """
    def __init__(
            self,
            datastore: str,
            models: List,
            weights: List,
            geodetic_crs: str = "EPSG:4326",
            cartesian_crs: str = "EPSG:3035",
            ):
        """
        Args:
            datastore: Path to the datastore file
            models: List of model IDs
            weights: List of model weights
        """
        self.datastore = datastore
        self.models = models
        self.weights = np.array(weights)
        self.geodetic_crs = geodetic_crs
        self.cartesian_crs = cartesian_crs
        self.geo_to_cart = Transformer.from_crs(geodetic_crs, cartesian_crs, always_xy=True)
        self.cart_to_geo = Transformer.from_crs(cartesian_crs, geodetic_crs, always_xy=True)
        self._geo_grid = None

    def __len__(self):
        return len(self.models)

    def __getitem__(self, key) -> RateGrid2D:
        """Return a specific model as a RateGrid2D object
        """
        if key not in self.models:
            return
        db = h5py.File(self.datastore, "r")
        grid = RateGrid2D.from_hdf5(db[key], self.geodetic_crs, self.cartesian_crs)
        db.close()
        return grid

    def __iter__(self):
        # Return a tuple of model and weight
        for model, weight in zip(self.models, self.weights):
            yield model, weight

    @property
    def grid(self):
        """Returns a basic underlying grid for the set
        """
        if self._geo_grid is None:
            self._geo_grid = self.get_null_grid()
        return self._geo_grid

    @classmethod
    def from_OQ_source_models(cls, grid: RateGrid2D, datastore: str, source_model_files: List,
                              source_model_ids: List, weights: Optional[List] = None,
                              width_of_mfd_bin: float = 0.1):
        """Instantiate the class from a list of source model files in xml format and their
        corresponding weights

        Args:
            grid: Rate grid common to all the source files
            datastore: Path to the hdf5 datastore file
            source_model_files: List of source models files where each source model file can
                                be a single file path or a space delimited list of files
            source_model_ids: Identifiers for each source. This will identify each specific
                              source model in the data store
            weights: Weights of the source models (if not specified then each source will
                     be evenly weighted with 1 / num_source_models)
            width_of_mfd_bin: Width of the MFD bins for exponential parametric recurrence
                              models
        """
        # Verify the list of source model files, IDs and weights have the same length
        num_models = len(source_model_files)
        assert num_models == len(source_model_ids)
        if weights is None:
            weight = 1.0 / num_models
            weights = [weight] * num_models
        assert len(weights) == num_models
        # Build the data strore
        build_datastore(
            datastore,
            grid,
            source_model_files,
            source_model_ids,
            weights,
            width_of_mfd_bin
        )
        return cls(datastore, source_model_ids, weights)

    @classmethod
    def from_OQ_logic_tree_file(cls, grid: RateGrid2D, datastore: str,
                                source_model_logic_tree_file: str,
                                source_folder: Optional[str] = None,
                                width_of_mfd_bin: float = 0.1):
        """Instantiate the class from an OpenQuake source model logic tree file

        Args:
            grid: Rate grid common to all the source files
            datastore: Path to the hdf5 datastore file
            source_model_logic_tree_file: Path to OpenQuake source model logic tree file
            source_folder: Relative path to source model files (if not in current directory)
            width_of_mfd_bin: Width of the MFD bins for exponential parametric recurrence
                              models
        """
        if not source_folder:
            source_folder = os.path.split(source_model_logic_tree_file)[0]
        # Get the source branches, IDs and weights
        source_model_files, source_model_ids, weights = \
            extract_source_model_logic_tree_branches(source_model_logic_tree_file,
                                                     source_folder)
        total_weight = sum(weights)

        print("Source model logic tree %s contains %g branches (total weight = %.6f)"
              % (source_model_logic_tree_file, len(source_model_ids), total_weight))
        # Build the data store
        build_datastore(
            datastore,
            grid,
            source_model_files,
            source_model_ids,
            weights,
            width_of_mfd_bin
        )
        return cls(datastore, source_model_ids, weights)

    def _verify_grid_similarity(self):
        """Verifies that all of the rate model data sets in the database share the same
        basic grid attributes (i.e. they are comparable)
        """
        grid_attrs = ["llon", "llat", "ulon", "ulat", "spcx", "spcy", "lm", "um", "dm"]
        db = h5py.File(self.datastore, "r")
        target_attrs = {}
        for model in self.models:
            if not target_attrs:
                for key in grid_attrs:
                    target_attrs[key] = db[model].attrs[key]
            else:
                for key in grid_attrs:
                    val = db[model].attrs[key]
                    assert np.isclose(val, target_attrs[key]),\
                        "Inconsistent grid attribute for %s and model %s (expected %.3f,"\
                        "found %.3f)" % (key, model, val, target_attrs[key])
        return

    def get_null_grid(self) -> RateGrid2D:
        """Returns an empty :class:`RateGrid2D` based on the attributes found in the datasets

        Returns:
            grid: An instance of the general rate grid with zero rates
        """
        self._verify_grid_similarity()
        key = self.models[0]
        db = h5py.File(self.datastore, "r")
        bbox = (db[key].attrs["llon"], db[key].attrs["llat"],
                db[key].attrs["ulon"], db[key].attrs["ulat"])

        grid = RateGrid2D(
            bbox,
            db[key].attrs["spcx"],
            db[key].attrs["spcy"],
            db[key].attrs["lm"],
            db[key].attrs["um"],
            db[key].attrs["dm"],
            self.geodetic_crs,
            self.cartesian_crs
            )
        db.close()
        return grid

    def get_rate_map_set(self, mmin: float, mmax: float = np.inf) -> np.ndarray:
        """Returns a set of rate maps constrained by the minimum and maximum magnitude

        Args:
            mmin: Minimum magnitude for the rate maps
            mmax: Maximum magnitude for the rate maps

        Returns:
            rate_maps: 3D array containing a rate map for each source model [nlat, nlon, nmodel]
        """
        rate_maps = []
        db = h5py.File(self.datastore, "r")
        for model in self.models:
            low_m = db[model].attrs["lm"]
            high_m = db[model].attrs["um"]
            d_m = db[model].attrs["dm"]
            mbins = np.arange(low_m, high_m + d_m, d_m)
            idx = np.logical_and(mbins[:-1] >= mmin, mbins[1:] <= mmax)
            rates = db[model][:]
            rate_maps.append(np.sum(rates[:, :, idx], axis=2))
        rate_maps = np.dstack(rate_maps)
        db.close()
        return rate_maps

    def get_mean_rates(
            self,
            mmin: float = -np.inf,
            mmax: float = np.inf,
            rate_maps: Optional[np.ndarray] = None,
            ) -> np.ndarray:
        """Gets the weighted mean of an array of rate maps. If the rate maps have been
        pre-computed then these can be input directly via thr `rate_maps`, otherwise they
        will be calculated according to the `mmin` and `mmax`

        Args:
            mmin: Minimum magnitude for rate bins
            mmax: Maximum magnitude for rate bins
            rate_maps: Pre-computed set of rate maps

        Returns:
            mean_rates: Weighted mean rate map
        """
        if rate_maps is None:
            rate_maps = self.get_rate_map_set(mmin, mmax)
        nx, ny, nmod = rate_maps.shape
        assert nmod == len(self.weights)
        mean_rates = np.zeros([nx, ny])
        for i in range(nmod):
            mean_rates += (rate_maps[:, :, i] * self.weights[i])
        return mean_rates

    def get_quantile_rates(
            self,
            quantiles: List,
            mmin: float = -np.inf,
            mmax: float = np.inf,
            rate_maps: Optional[np.ndarray] = None,
            ) -> Dict:
        """Calculates the quantiles of a set of rates. If the rate maps have been
        pre-computed then these can be input directly via thr `rate_maps`, otherwise they
        will be calculated according to the `mmin` and `mmax`


        Args:
            quantiles: List of quantiles as fractions in the range [0, 1]

        Returns:
            quantile_rates: A dictionary of rate maps for each quantile
        """
        if rate_maps is None:
            rate_maps = self.get_rate_map_set(mmin, mmax)
        ny, nx, nmod = rate_maps.shape
        assert nmod == len(self.weights)

        # Pre-allocate arrays and quantile dictionary
        quantile_keys = []
        quantile_rates = {}
        for qntl in quantiles:
            quantile_key = "{:.4f}".format(qntl)
            quantile_keys.append(quantile_key)
            quantile_rates[quantile_key] = np.zeros([ny, nx])
        # Get the quantile curves
        for i in range(ny):
            for j in range(nx):
                for key, qntl in zip(quantile_keys, quantiles):
                    quantile_rates[key][i, j] = quantile_curve(qntl,
                                                               rate_maps[i, j, :],
                                                               self.weights)
        return quantile_rates

    def get_statistical_rate_set(
            self,
            mmin: float,
            mmax: float = np.inf,
            ) -> Dict:
        """Returns a complete set of maps containing basic statistics of the weighted rate
        distributions

        Args:
            mmin: Minimum magnitude for rate bins
            mmax: Maximum magnitude for rate bins

        Returns:
            rate_map_statistics: Dictionary containing maps of the statistics of the rates
                                 including, mean, quantiles [0.05, 0.16, 0.25, 0.5, 0.75, 0.84,
                                 0.95], IQR, min and max
        """
        quantiles = [0.05, 0.16, 0.25, 0.5, 0.75, 0.84, 0.95]
        rate_maps = self.get_rate_map_set(mmin, mmax)
        rate_map_statistics = self.get_quantile_rates(quantiles, rate_maps=rate_maps)
        # Get mean, IQR, minimum and maximum rate map
        rate_map_statistics["mean"] = self.get_mean_rates(rate_maps=rate_maps)
        rate_map_statistics["IQR"] = rate_map_statistics["0.7500"] -\
            rate_map_statistics["0.2500"]
        rate_map_statistics["min"] = np.min(rate_maps, axis=2)
        rate_map_statistics["max"] = np.max(rate_maps, axis=2)
        return rate_map_statistics

    def get_ks_distance(
            self,
            rate_set: RateGrid2DSet,
            mmin: float,
            mmax: float = np.inf,
            ) -> Tuple[np.ndarray, np.ndarray]:
        """Returns the spatial maps Kolmogorov-Smirnoff distance between the current set of
        rates and their corresponding weights and another set of rates and corresponding
        weights

        Args:
            rate_set: Model for comparison as :class:`RateGrid2DSet`
            mmin: Minimum magnitude for rate bins
            mmax: Maximum magnitude for rate bins

        Returns:
            ks_dist: Map of Kolmogorov-Smirnov distance betwee, the current and target models
            ks_prob: Probabilities from a 2-sided weighted KS-Test
        """
        rate_maps1 = self.get_rate_map_set(mmin, mmax)
        rate_maps2 = rate_set.get_rate_map_set(mmin, mmax)
        ny1, nx1, nmod1 = rate_maps1.shape
        ny2, nx2, nmod2 = rate_maps2.shape
        assert (nx1, ny1) == (nx2, ny2)
        ks_dist = np.nan + np.zeros([ny1, nx1])
        ks_prob = np.nan + np.zeros([ny1, nx1])
        idx = np.logical_and(
            np.sum(rate_maps1, axis=2) > 0.0,
            np.sum(rate_maps2, axis=2) > 0.0
        )
        for i in range(ny1):
            if not np.any(idx[i, :]):
                # No non-zero values in the row - skip the row
                continue
            for j in range(nx1):
                if not idx[i, j]:
                    # Empty cell - skip
                    continue
                ks_dist[i, j], ks_prob[i, j] = ks_weighted(rate_maps1[i, j],
                                                           rate_maps2[i, j],
                                                           self.weights,
                                                           rate_set.weights)
        return ks_dist, ks_prob

    def get_wasserstein_distance(
            self,
            rate_set: RateGrid2DSet,
            mmin: float,
            mmax: float = np.inf,
            ) -> np.ndarray:
        """Returns the spatial maps of Wasserstein distance between the current set of
        rates and their corresponding weights and another set of rates and corresponding
        weights

        Args:
            rate_set: Model for comparison as :class:`RateGrid2DSet`
            mmin: Minimum magnitude for rate bins
            mmax: Maximum magnitude for rate bins

        Returns:
            wasserstein_dist: Map of Wasserstein Distance between the current model and the
                              target model
        """
        rate_maps1 = self.get_rate_map_set(mmin, mmax)
        rate_maps2 = rate_set.get_rate_map_set(mmin, mmax)
        ny1, nx1, nmod1 = rate_maps1.shape
        ny2, nx2, nmod2 = rate_maps2.shape
        assert (nx1, ny1) == (nx2, ny2)
        wasserstein_dist = np.nan + np.zeros([ny1, nx1])
        idx = np.logical_and(
            np.sum(rate_maps1, axis=2) > 0.0,
            np.sum(rate_maps2, axis=2) > 0.0
        )
        for i in range(ny1):
            if not np.any(idx[i, :]):
                # No non-zero values in the row - skip the row
                continue
            for j in range(nx1):
                if not idx[i, j]:
                    # Empty cell - skip
                    continue
                wasserstein_dist[i, j] = wasserstein_distance(rate_maps1[i, j],
                                                              rate_maps2[i, j],
                                                              self.weights,
                                                              rate_set.weights)
        return wasserstein_dist

    def to_csep_forecast_set(
            self,
            mmin: float,
            mmax: np.inf,
            start_time: str,
            end_time: str) -> Tuple[List, List, List]:
        """Exports the grid to a list of PyCSEP gridded forecast objects

        Args:
            mmin: Minimum magnitude of forecast to be exported
            mmax: Maximum magnitude of forecast to be exported
            start_time: Start time of forecast
            end_time: End time of forecast

        Returns:
             forecasts: List of PyCSEP Gridded Forecasts for each source model
             model_ids: Names of the models
             model_weights = weights of the models
        """
        model_ids = []
        model_weights = []
        forecasts = []

        db = h5py.File(self.datastore, "r")
        for key, weight in zip(self.models, self.weights):
            # Load the grids directly from hdf5 but don't build the RTree
            # index and grid polygons
            grid = RateGrid2D.from_hdf5(db[key], self.geodetic_crs,
                                         self.cartesian_crs,
                                         build_tree_polygons=False)
            model_ids.append(key)
            model_weights.append(weight)
            forecasts.append(grid.to_csep_forecast(mmin, mmax, start_time, end_time,
                                                   name=key))
        db.close()
        return forecasts, model_ids, model_weights


def parse_simple_csv_to_csep(
        cat_file: str,
        start_time: str,
        end_time: str,
        mmin: float = -np.inf,
        mmax: float = np.inf,
        min_depth: float = 0.0,
        max_depth: float = np.inf,
        region: Optional[CartesianGrid2D] = None,
        name: Optional[str] = None,
        name_map: Optional[Dict] = None,
    ) -> CSEPCatalog:
    """Converts a catalogue from basic csv

    Args:
        cat_file: Path to csv catalogue
        start_time: Start time to filter the catalogue (in ISO datetime format)
        end_time: End time to filter the catalogue (in ISO datetime format)
        mmin: Minimum magnitude
        mmax: Maximum magnitude
        min_depth: Minimum depth (km)
        max_depth: Maximum depth (km)
        region: Geographical region as a CartesianGrid2D object
        name: Catalogue name
        name_map: Dictionary to map the columns of the original
                  catalogue file to the required reference

    Returns:
        Filtered earthquake catalogue in CSEPCatalog format
    """
    start_time = np.datetime64(start_time)
    end_time = np.datetime64(end_time)
    input_catalogue = pd.read_csv(cat_file, ",")
    if "flagvector" in input_catalogue:
        # Can keep just the mainshocks if a declustering algorithm was applied
        input_catalogue["flagvector"] =\
            input_catalogue["flagvector"].astype(int)
        input_catalogue = input_catalogue[input_catalogue["flagvector"] == 0]
    # If a name mapping is provided then apply this
    if name_map:
        input_catalogue.rename(columns=name_map, inplace=True,)
    
    for key in ["id", "origin_time", "longitude",
                "latitude", "depth", "magnitude"]:
        assert key in input_catalogue.columns,\
            "Required key %s not found in file" % key
    
    input_catalogue["origin_time"] =\
        input_catalogue["origin_time"].astype(np.datetime64)
    if region:
        west, east, south, north = region.get_bbox()
    else:
        west, east, south, north = (-180.0, 180.0, -90.0, 90.0)
    idx = (input_catalogue["origin_time"] >= start_time) &\
        (input_catalogue["origin_time"] <= end_time) &\
        (input_catalogue["magnitude"] >= mmin) &\
        (input_catalogue["magnitude"] <= mmax) &\
        (input_catalogue["longitude"] >= west) &\
        (input_catalogue["longitude"] <= east) &\
        (input_catalogue["latitude"] >= south) &\
        (input_catalogue["latitude"] <= north) &\
        (input_catalogue["depth"] >= min_depth) &\
        (input_catalogue["depth"] <= max_depth)
    input_catalogue = input_catalogue[idx]
    n = input_catalogue.shape[0]
    catalog = np.zeros(n, dtype=CSEPCatalog.dtype)
    catalog["id"] = input_catalogue["id"].to_numpy()
    catalog["longitude"] = input_catalogue["longitude"].to_numpy()
    catalog["latitude"] = input_catalogue["latitude"].to_numpy()
    catalog["depth"] = input_catalogue["depth"].to_numpy()
    catalog["magnitude"] = input_catalogue["magnitude"].to_numpy()
    # Define the reference epoch
    origin_times = []
    epoch = datetime(1970, 1, 1, 0, 0, 0, 0).replace(tzinfo=timezone.utc)
    for dt in input_catalogue["origin_time"].dt.to_pydatetime():
        epoch_eq = dt.replace(tzinfo=timezone.utc)
        epoch_time_seconds = (epoch_eq - epoch).total_seconds()
        origin_times.append(int(1000 * epoch_time_seconds))
    catalog["origin_time"] = np.array(origin_times, dtype=int)
    
    return CSEPCatalog(data=catalog, region=region, name=name)
