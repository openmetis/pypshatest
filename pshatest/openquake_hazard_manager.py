"""
Module to process and explore OpenQuake outputs independently of the station database
"""

import json
import argparse
import logging
from multiprocessing import Pool  # cpu_count
import h5py
import numpy as np
from scipy.stats import wasserstein_distance
from typing import Union, Dict, Tuple, Optional, List
from openquake.commonlib import datastore
from openquake.calculators.extract import extract, extract_
from openquake.hazardlib.stats import mean_curve, quantile_curve


logging.basicConfig(level=logging.INFO)


try:
    from pshatest.utils import ks_weighted, overlap_index
except ImportError:
    logging.info("PSHA Testing utilities not available here")


SITE_DTYPES = {
    "sids": np.uint32,
    "stations": (np.string_, 10),
    "lon": np.float64,
    "lat": np.float64,
    "depth": np.float64,
    "region": np.uint32,
    "vs30": np.float64,
    "vs30measured": bool,
    "xvf": np.float64,
    "z1pt0": np.float64,
    "z2pt5": np.float64,
    "custom_site_id": (np.string_, 10),
    "geology": (np.string_, 20),
}


SUPPORTED_METRICS = ["overlap", "wasserstein", "kolmogorov-smirnov",
                     "OS", "WS", "KS"]


def load_split_calculation_info(split_calculation_file: str) -> List:
    """Split calculation information (calculation ID, branch range, weight) can be retreived
    from a json file

    Args:
        split_calculation_file: Path to the json file containing the split calculation
                                arguments

    Returns:
        calc_properties: Calculation properties as list of tuples of (Calc. ID, Branch Indices,
                         calculation weight)
    """
    with open(split_calculation_file, "r") as f:
        calc_info = json.load(f)
    calc_properties = []
    for key, val in calc_info.items():
        idx = np.arange(val["range"][0], val["range"][1])
        try:
            oq_key = int(key)
        except ValueError:
            oq_key = key
        calc_properties.append((oq_key, idx, val["weight"]))
    return calc_properties


def fast_compute_hazard_map(A: Tuple) -> np.ndarray:
    """Rapid calculation of seismic hazard maps (for use in parallel process)
    Args:
        A: Input (curves, imls, poes): Curves = [Nsites, Nimls]

    Returns:
        hmaps: Hazard maps [Nsites, NPoEs]
    """
    curves, imls, poes = A
    nsites, nimls = curves.shape
    assert nimls == len(imls)
    log_curves = np.log(np.clip(curves[:, ::-1], 1.0E-30, 1.0))
    log_imls = np.log(imls[::-1])
    log_poes = np.log(poes)
    hmaps = np.zeros([nsites, len(poes)])
    for i in range(nsites):
        hmaps[i, :] = np.exp(np.interp(log_poes, log_curves[i, :], log_imls))
    return hmaps


def fast_get_mean_quantiles(
        hcurves: np.ndarray,
        weights: np.ndarray,
        quantiles: Optional[np.ndarray] = None,
        ) -> np.ndarray:
    """Rapid calculation of means and quantiles from a set of seismic hazard curves (for use
    in a parallel process

    Args:
        hcurves: The complete set of hazard curves with dimension [Num. Branches, Num. IMTs,
                 Num. IMLs]
        weights: The array of weights
        quantiles: The required target quantiles if specified, otherwise just the mean is
                   returns

    Returns:
        hcurves_stats: Returns the hazard curves for the given mean and quantiles
    """
    nbranches, nimts, nimls = hcurves.shape
    assert len(weights) == nbranches
    if quantiles is None:
        # Just going to return the mean
        nqntl = 1
        quantiles = []
    else:
        nqntl = len(quantiles) + 1
    hcurves_stats = np.zeros([nqntl, nimts, nimls])
    hcurves_stats[0, :, :] = mean_curve(hcurves, weights)
    for i, qntl in enumerate(quantiles):
        hcurves_stats[i + 1, :, :] = quantile_curve(qntl, hcurves, weights)
    return hcurves_stats


def mean_difference(
        values_a: np.ndarray,
        values_b: np.ndarray,
        weights_a: np.ndarray,
        weights_b: np.ndarray) -> np.ndarray:
    """Returns the difference in weighted mean values between the two datasets

    Args:
        values_a: Values of seismic hazard for model A
        values_b: Values of seismic hazard for model B
        weights_a: Weights corresponding to seismic hazard values of model A
        weights_b: Weights corresponding to seismic hazard values of model B

    Returns:
        Percent increase/decrease in hazard values from A to B
    """
    return 100.0 * ((mean_curve(values_b, weights_b) / mean_curve(values_a, weights_a)) - 1.0)


def quantile_difference(
        values_a: np.ndarray,
        values_b: np.ndarray,
        weights_a: np.ndarray,
        weights_b: np.ndarray,
        qntls: List) -> List:
    """Returns the difference in the weighted quantiles between the two datasets

    Args:
        values_a: Values of seismic hazard for model A
        values_b: Values of seismic hazard for model B
        weights_a: Weights corresponding to seismic hazard values of model A
        weights_b: Weights corresponding to seismic hazard values of model B
        qntls: List of quantile values (between 0 and 1)

    Returns:
        Percent change in hazard quantile from A to B for each quantile
    """
    qntl_diffs = []
    for qntl in qntls:
        qntl_diffs.append(quantile_curve(qntl, values_b, weights_b) /
                          quantile_curve(qntl, values_a, weights_a))
    return qntl_diffs


def iqr_ratio(
        values_a: np.ndarray,
        values_b: np.ndarray,
        weights_a: np.ndarray,
        weights_b: np.ndarray) -> np.ndarray:
    """Returns the Inter-quartile range ratio of Model B to Model A

    Args:
        values_a: Values of seismic hazard for model A
        values_b: Values of seismic hazard for model B
        weights_a: Weights corresponding to seismic hazard values of model A
        weights_b: Weights corresponding to seismic hazard values of model B
    """
    iqr_a = quantile_curve(0.75, values_a, weights_a) -\
        quantile_curve(0.25, values_a, weights_a)
    iqr_b = quantile_curve(0.75, values_b, weights_b) -\
        quantile_curve(0.25, values_b, weights_b)
    return iqr_b / iqr_a


def get_full_lt(rlzs: Union[List, int], gsim_keymap: Optional[Dict] = None,
                str_len: int = 200,):
    """Builds the full logic tree information as a numpy array

    Args:
        rlzs: List of realisations or number of realisaions
        gsim_keymap:
    """
    dtypes = np.dtype([("ordinal", int),
                       ("smlt_id", (np.string_, str_len)),
                       ("gsim_id", (np.string_, str_len)),
                       ("weight", np.float64)])
    if isinstance(rlzs, int):
        # This is a pre-allocation - so return the array of zeros
        return np.zeros(rlzs, dtype=dtypes)
    nrlzs = len(rlzs)
    arr = np.zeros(nrlzs, dtype=dtypes)
    for i, rlz in enumerate(rlzs):
        arr["ordinal"][i] = i
        arr["smlt_id"][i] = "~".join(rlz.sm_lt_path)
        arr["weight"][i] = rlz.weight["weight"]
        gsim = str(rlz.gsim_rlz.value)[1:-1].rstrip(",")
        if "\n" in gsim:
            gsim = gsim.replace("\n", ";")
        if gsim_keymap:
            arr["gsim_id"][i] = gsim_keymap[gsim]
        else:
            arr["gsim_id"][i] = gsim
    return arr


def extract_information_from_openquake_datastore(calc_id: Union[int, str],
                                                 grp: Union[h5py.Group, h5py.File],
                                                 str_len: int = 200,
                                                 gsim_map: Optional[Dict] = None):
    """Extracts relevant seismic hazard results from an OpenQuake datastore adds them to
    an h5py.Group

    Args:
        calc_id: OpenQuake calculation results either as the calculation ID (integer) or the
                 path to the hdf5 datastore file for that calculation
        grp: h5py.Group to place the hazard results
        str_len: Number of characters to use as the max. string length for the GSIM ID
        gsim_map: Mapping of the GSIM IDs to simpler string identifiers
    """
    # Connect to the OpenQuake data store
    dstore = datastore.read(calc_id)
    # Extract the OQ input parameters, especially the IMTLs
    oqparam = extract(dstore, "oqparam")
    vars(oqparam).update(json.loads(oqparam.json))
    logging.info("... adding metadata")
    # Add the group and initial metadata
    grp.attrs["description"] = oqparam.description
    grp.attrs["gsim_logic_tree"] = oqparam.inputs["gsim_logic_tree"]
    grp.attrs["job_ini"] = oqparam.inputs["job_ini"]
    grp.attrs["source_model_logic_tree"] = oqparam.inputs["source_model_logic_tree"]
    grp.attrs["site_model"] = oqparam.inputs["site_model"]
    grp.attrs["investigation_time"] = oqparam["investigation_time"]
    # Add IMTLs
    imtls_grp = grp.create_group("IMTLs")
    for imtl in oqparam.hazard_imtls:
        imtl_dset = imtls_grp.create_dataset(imtl,
                                             (len(oqparam.hazard_imtls[imtl]),),
                                             dtype="f")
        imtl_dset[:] = np.array(oqparam.hazard_imtls[imtl])
    # Add the poes and quantiles
    if oqparam.poes is not None:
        poes_dset = grp.create_dataset("poes", (len(oqparam.poes),), dtype="f")
        poes_dset[:] = np.array(oqparam.poes)
    if oqparam.quantiles is not None:
        qntls_dset = grp.create_dataset("quantiles", (len(oqparam.quantiles),), dtype="f")
        qntls_dset[:] = np.array(oqparam.quantiles)
    # Store the realizations and weights
    logging.info("... adding realizations data")
    full_lt = extract(dstore, "full_lt").get_realizations()
    rlz_array = get_full_lt(full_lt, gsim_map, str_len)
    rlz_dset = grp.create_dataset('realizations', rlz_array.shape, rlz_array.dtype)
    rlz_dset[:] = rlz_array
    if gsim_map:
        # Add the GSIM Map as an attribute to the dataset
        rlz_dset.attrs["gsim_map"] = str(gsim_map)

    # Get the sites information and add on the station keys

    logging.info("... adding sites data")
    sites = extract(dstore, "sitecol").to_dframe()
    if "custom_site_id" in sites.columns:
        sites["stations"] = sites["custom_site_id"].copy()
    # Convert to array
    site_dtypes = []
    for key in sites.columns:
        site_dtypes.append((key, SITE_DTYPES[key]))
    site_array = np.zeros(sites.shape[0], dtype=site_dtypes)
    for key in sites.columns:
        site_array[key] = sites[key].to_numpy()
    site_dset = grp.create_dataset("sites", site_array.shape, site_dtypes)
    site_dset[:] = site_array
    logging.info("... adding curves")
    # Add the curves and stats
    h_results = {"hcurves": extract(dstore, "hcurves-rlzs")[:],
                 "hstats": extract(dstore, "hcurves-stats")[:]}

    for key in h_results:
        if not len(h_results[key]):
            logging.info("%s not present in model" % key)
            continue
        dset = grp.create_dataset(key,
                                  h_results[key].shape,
                                  h_results[key].dtype)
        dset[:] = h_results[key]
    # Add the weights
    weights = rlz_array["weight"]
    weight_dset = grp.create_dataset("weights", weights.shape, dtype="f")
    weight_dset[:] = weights
    logging.info("... Done!")
    return


def extract_information_from_openquake_ses_calculation(
        calc_id: Union[int, str],
        grp: Union[h5py.Group, h5py.File],
        str_len: int = 200,
        gsim_map: Optional[Dict] = None
        ):
    """
    Extracts relevant stochastic event set seismic hazard results from an OpenQuake datastore
    adds them to an h5py.Group

    Args:
        calc_id: OpenQuake calculation results either as the calculation ID (integer) or the
                 path to the hdf5 datastore file for that calculation
        grp: h5py.Group to place the hazard results
        str_len: Number of characters to use as the max. string length for the GSIM ID
        gsim_map: Mapping of the GSIM IDs to simpler string identifiers

    """
    # All of the hazard curve information can be retreived from the datastore in the
    # same way as for the classical PSHA calculations
    extract_information_from_openquake_datastore(calc_id, grp, str_len, gsim_map)
    # Extract GMF information - re-open datastore
    dstore = datastore.read(calc_id)
    oqparam = extract(dstore, "oqparam")
    vars(oqparam).update(json.loads(oqparam.json))
    # Get the event information
    logging.info("... retreiving event-table")
    events = extract(dstore, "events").array
    # Add events to the hdf5
    events_dset = grp.create_dataset("events", events.shape, dtype=events.dtype)
    events_dset[:] = events
    logging.info("... retreiving GMFs")
    # Get the ground motion fields
    eid = extract_(dstore, "gmf_data/eid").array
    sid = extract_(dstore, "gmf_data/sid").array
    # Setup data types
    imts = list(oqparam.hazard_imtls)
    gmf_dtypes = [("eid", eid.dtype), ("sid", sid.dtype)]
    for imt in imts:
        logging.info("... ... Retreiving field %s" % imt)
        gmf_dtypes.append((imt, np.float64))
    gmfs = np.zeros(len(eid), dtype=np.dtype(gmf_dtypes))
    # Get event and site IDs
    gmfs["eid"] = eid
    gmfs["sid"] = sid
    # Get the GMFs
    for i, imt in enumerate(imts):
        gmfs[imt] = extract_(dstore, "gmf_data/gmv_{:g}".format(i)).array
    # Store to hdf5
    logging.info("... storing GMFs to file")
    gmfs_dset = grp.create_dataset("gmf_data", gmfs.shape, dtype=gmfs.dtype)
    gmfs_dset[:] = gmfs
    logging.info("... Done")
    return


def get_gsim_map_from_json(gsim_map_filename: str) -> Dict:
    """Extract a GSIM mapping from a json file
    Args:
        gsim_map_filename: Path to JSON file containing the mapping
    """
    with open(gsim_map_filename, "r") as f:
        gsim_map = json.load(f)
    return gsim_map


class OpenQuakeHazardOutputManager():
    """

    HDF5 file attributes:

    hcurves = Array of hazard curves with dimension [nsites, nbranches, nimts, nimls]
    hstats: = Array of mean, quantiles curves with dimensions [nsites, 1 + nqntl, nimts, nimls]
    sitecol = Site collection (numpy array - custom dtype)
    weights = Branch weights (numpy array)
    realizations = Numpy array with the logic tree branch IDs and weights

    Attributes:
        dbfile: Path to the database file
        num_proc: Number of processors to use for parallel job
    """
    def __init__(self, dbfile: str, num_proc: int = 8):
        self.dbfile = dbfile
        self.num_proc = num_proc

    def add_hazard_results(
            self,
            calc_id: int,
            calc_type: str,
            model_name: str,
            model_attrs: Optional[Dict] = None,
            gsim_map: Optional[Dict] = None,
            str_len: int = 200):
        """Add results from a single seismic hazard calculation

        Args:
            calc_id: OpenQuake calculation ID
            calc_type: Calculation type (must be either 'classical' for classical PSHA or
                       'ses' for event-based PSHA)
            model_name: Identifier for the model
            model_attrs: Dictionary of additional model information attributes
            gsim_map: A dictionary mapping the OpenQuake GSIM string to a user-specified name
            str_len: Maximum number of characters to represented the GSIM string
        """
        assert calc_type in ("classical", "ses"),\
            "Calculation type must be 'classical' or 'ses' (%s not recognised)" % calc_type
        db = h5py.File(self.dbfile, "a")
        model_grp = db.create_group(model_name)
        model_grp.attrs["software"] = "OpenQuake"
        if model_attrs is not None:
            # Add some optional attributes
            for key in model_attrs:
                model_grp.attrs[key] = model_attrs[key]
        if calc_type == "ses":
            extract_information_from_openquake_ses_calculation(
                calc_id,
                model_grp,
                str_len=str_len,
                gsim_map=gsim_map
                )
        else:
            extract_information_from_openquake_datastore(
                calc_id,
                model_grp,
                str_len=str_len,
                gsim_map=gsim_map)
        db.close()
        return

    def add_split_classical_logic_tree_results(
            self,
            calc_properties: List,
            model_name: str,
            model_attrs: Optional[Dict] = None,
            gsim_map: Optional[Dict] = None,
            str_len: int = 200,
            ):
        """In some cases the full logic tree cannot be run in a single calculation, so this
        adds a split logic tree model to the database by recombining the branches in the
        process

        Args:
            calc_properties: List of tuples containing the calculation properties for each
                             sub branch (OQ Calculation ID, Index of branches in main LT,
                             Total Weight of Subset of Branches)
            model_name: Identifier for the model
            model_attrs: Dictionary of additional model information attributes
            gsim_map: A dictionary mapping the OpenQuake GSIM string to a user-specified name
            str_len: Maximum number of characters to represented the GSIM string
        """
        # Get total number of branches
        nbranches = 0
        total_weight = 0.0
        for calc_id, calc_index, calc_weight in calc_properties:
            nbranches += len(calc_index)
            total_weight += calc_weight
        assert np.allclose(total_weight, 1.0),\
            "Total weight %.7f not equal to 1" % total_weight
        # Need to setup the group with information from initial model
        logging.info("Adding hazard from split calculation %s (%g branches)"
                     % (model_name, nbranches))
        dstore = datastore.read(calc_properties[0][0])
        oqparam = extract(dstore, "oqparam")
        vars(oqparam).update(json.loads(oqparam.json))
        sites = extract(dstore, "sitecol").to_dframe()
        nsites = sites.shape[0]
        dstore.close()
        imtls = oqparam["hazard_imtls"]
        nimts = len(imtls)
        nimls = 0
        for imt in imtls:
            if nimls:
                assert nimls == len(imtls[imt]),\
                    "Unequal number of IMLs for imt %s (expected %g)" % (imt, nimls)
            nimls = len(imtls[imt])
        # Create the group
        db = h5py.File(self.dbfile, "a")
        model_grp = db.create_group(model_name)
        model_grp.attrs["software"] = "OpenQuake"
        model_grp.attrs["job_ini"] = oqparam.inputs["job_ini"]
        model_grp.attrs["site_model"] = oqparam.inputs["site_model"]
        model_grp.attrs["investigation_time"] = oqparam["investigation_time"]
        # Add other attributes if specified
        if model_attrs:
            for key in model_attrs:
                model_grp.attrs[key] = model_attrs[key]
        # Set the IMTLs
        imtls_grp = model_grp.create_group("IMTLs")
        for imt in imtls:
            imt_dset = imtls_grp.create_dataset(imt, (nimls,), dtype="f")
            imt_dset[:] = np.array(imtls[imt])
        # Set the site collection
        if "custom_site_id" in sites.columns:
            sites["stations"] = sites["custom_site_id"].copy()
        # Convert to array
        site_dtypes = []
        for key in sites.columns:
            site_dtypes.append((key, SITE_DTYPES[key]))
        site_array = np.zeros(sites.shape[0], dtype=site_dtypes)
        for key in sites.columns:
            site_array[key] = sites[key].to_numpy()
        sites_dset = model_grp.create_dataset("sites",
                                              site_array.shape,
                                              dtype=site_array.dtype)
        sites_dset[:] = site_array
        # Add the hazard curves
        model_grp.create_dataset("hcurves", (nsites, nbranches, nimts, nimls), dtype="f")

        # IF the poes are specified then add these
        if oqparam.poes is not None:
            poes_dset = model_grp.create_dataset("poes", (len(oqparam.poes),), dtype="f")
            poes_dset[:] = np.array(oqparam.poes)

        # If quantiles are specified then add these
        if oqparam.quantiles is not None:
            qntls_dset = model_grp.create_dataset("quantiles",
                                                  (len(oqparam.quantiles),), dtype="f")
            qntls_dset[:] = np.array(oqparam.quantiles)
        # Initialise the logic tree realizations
        lt_dtypes = np.dtype([("ordinal", np.int),
                              ("smlt_id", (np.string_, str_len)),
                              ("gsim_id", (np.string_, str_len)),
                              ("weight", np.float64)])
        model_grp.create_dataset("realizations", (nbranches, ), dtype=lt_dtypes)
        self._get_split_hazard_results(model_grp, calc_properties, gsim_map, str_len)
        weights = model_grp["realizations"][:]["weight"]
        weight_dset = model_grp.create_dataset("weights", weights.shape, dtype="f")
        weight_dset[:] = weights
        db.close()
        return

    def _get_split_hazard_results(
            self,
            model_grp: Union[h5py.Group, h5py.File],
            calc_properties: List,
            gsim_map: Optional[Dict] = None,
            str_len: int = 200,
            ):
        """Appends the sets of split hazard results to the database

        Args:
            model_grp: h5py.Group correspending to the specific model
        """
        description = []
        gsim_logic_tree = []
        source_model_logic_tree = []
        for calc_id, idx, calc_weight in calc_properties:
            logging.info("---- Adding results from calculation ID %s" % str(calc_id))
            dstore = datastore.read(calc_id)
            oqparam = extract(dstore, "oqparam")
            vars(oqparam).update(json.loads(oqparam.json))
            # Extract the hazard curves
            hcurves_rlzs = extract(dstore, "hcurves-rlzs")[:]
            model_grp["hcurves"][:, idx, :, :] = hcurves_rlzs
            del hcurves_rlzs
            # Extract the logic tree realizations
            full_lt = extract(dstore, "full_lt").get_realizations()
            rlz_array = get_full_lt(full_lt, gsim_map, str_len)
            # Re-adjust weights
            rlz_array["weight"] *= calc_weight
            model_grp["realizations"][idx] = rlz_array
            # Append model specific description, gsim_logic_tree, source_model_logic_tree
            description.append(oqparam.description)
            gsim_logic_tree.append(oqparam.inputs["gsim_logic_tree"])
            source_model_logic_tree.append(oqparam.inputs["source_model_logic_tree"])
            dstore.close()
        # Add description and logic tree info as attributes
        model_grp.attrs["description"] = "|".join(description)
        model_grp.attrs["gsim_logic_tree"] = "|".join(gsim_logic_tree)
        model_grp.attrs["source_model_logic_tree"] = "|".join(source_model_logic_tree)
        # Now need to recalculate mean and quantiles
        weights = model_grp["realizations"]["weight"]
        assert np.isclose(np.sum(weights), 1.0),\
            "Weights sum to %.7f (not 1)" % np.sum(weights)
        logging.info("---- Re-calculating mean and quantiles")
        # Extract the curves and the required quantiles
        hcurves = model_grp["hcurves"][:]
        nsites, nbranches, nimts, nimls = hcurves.shape
        quantiles = model_grp["quantiles"][:]
        nqntl = len(quantiles)
        hstats = np.zeros([nsites, nqntl + 1, nimts, nimls])
        # Execute retreival of mean and quantiles in parallel
        pool = Pool(self.num_proc)
        A = pool.starmap(fast_get_mean_quantiles,
                         [(hcurves[i, :, :, :], weights, quantiles) for i in range(nsites)])
        pool.close()
        for i in range(nsites):
            hstats[i, :, :, :] = A[i]
        # Store to database
        hstats_dset = model_grp.create_dataset("hstats", hstats.shape, dtype="f")
        hstats_dset[:] = hstats
        return

    def compare_hazard_distributions(
            self,
            model1: Union[str, h5py.Group],
            model2: Union[str, h5py.Group],
            poes: Union[List, np.ndarray],
            imts: List,
            metrics: Union[List, str]) -> Tuple[Dict, List, Union[List, np.ndarray]]:
        """Undertake quantitative comparisons of two distributions of seismic hazard results

        Args:
            model1: Hazard model 1 (as either name of model in current database or h5py.Group
                                    of model in a separate database)
            model2: Hazard model 2 (as either name of model in current database or h5py.Group
                                    of model in a separate database)
            poes: Probabilities of exceedance for comparisons
            imts: List of intensity measure types (as strings i.e.
                  ["PGA", "SA(0.1)", "SA(1.0)"])
            metrics: Choice of metrics for comparison (either string or list of strings)

        Returns:
            results: Dictionary of numpy arrays with result for each selected metric. The
                     result array has dimension [nsites, npoes, nimts]
            imts: IMTs used for the metrics (may be different from those originally input
            poes: Probabilities of exceedance used for the metrics
        """
        if isinstance(metrics, str):
            metrics = [metrics]
        for metric in metrics:
            if metric not in SUPPORTED_METRICS:
                raise ValueError("Comparison metric %s not supported" % metric)
        db = h5py.File(self.dbfile, "r")
        if isinstance(model1, str):
            model1 = db[model1]
        if isinstance(model2, str):
            model2 = db[model2]
        # Find common sites
        sites1 = model1["sites"][:]
        sites2 = model2["sites"][:]
        if not (np.allclose(sites1["lon"], sites2["lon"]) and
                np.allclose(sites1["lat"], sites2["lat"])):
            raise ValueError('Site models are different, cannot run comparisons')
        rlzs1 = model1["rlzs"][:]
        rlzs2 = model2["rlzs"][:]
        imts1 = list(model1["IMTLs"])
        imts2 = list(model2["IMTLs"])
        imt_pos1 = []
        imt_pos2 = []
        imtls1 = {}
        imtls2 = {}
        sel_imts = []
        for imt in imts:
            if (imt not in imts1) or (imt not in imts2):
                logging.info("IMT %s not common to both models" % imt)
                continue
            imt_pos1 = imts1.index(imt)
            imtls1[imt] = model1["IMTLs/{:s}".format(imt)][:]
            imt_pos2 = imts2.index(imt)
            imtls2[imt] = model2["IMTLs/{:s}".format(imt)][:]
            sel_imts.append(imt)
        logging.info("Extracting hazard curves from databases ...")
        curves1 = model1["hcurves"][:, :, np.array(imt_pos1), :]
        curves2 = model2["hcurves"][:, :, np.array(imt_pos2), :]
        # Can now close the database
        db.close()
        # Get the hazard maps
        logging.info("Calculating hazard maps for model 1")
        hmaps1 = self._get_hmaps(curves1, imtls1, poes, self.num_proc)
        logging.info("Calculating hazard maps for model 2")
        hmaps2 = self._get_hmaps(curves2, imtls2, poes, self.num_proc)
        return self.extract_metrics(hmaps1, hmaps2, rlzs1["weight"], rlzs2["weight"],
                                    sel_imts, poes)

    def extract_metrics(
            self,
            hmaps1: np.ndarray,
            hmaps2: np.ndarray,
            weights1: np.ndarray,
            weights2: np.ndarray,
            metrics: List,
            imts: List,
            poes: Union[List, np.ndarray]) -> Tuple[Dict, List, Union[List, np.ndarray]]:
        """Retreive the comparison metrics for the set of hazard maps and weights

        Args:
            hmaps1: Hazard maps for the first hazard model, with shape
                    [nsites, nbranches, npoes, nimts]
            hmaps2: Hazard maps for the second hazard model, with shape
                    [nsites, nbranches, npoes, nimts]
            weights1: Weights of the branches for the first hazard model
            weights2: Weights of the branches for the second hazard model
            metrics: List of required metrics
            imts: List of intensity measure types
            poes: Chosen probabilities of exceedance

        Results:
            results: Dictionary of numpy arrays with result for each selected metric. The
                     result array has dimension [nsites, npoes, nimts]
            imts: IMTs used for the metrics (may be different from those originally input
            poes: Probabilities of exceedance used for the metrics
        """
        results = {}
        nsites = hmaps1.shape[0]
        nimts = len(imts)
        npoes = len(poes)
        for metric in metrics:
            if metric == "KS":
                results["KS-DIST"] = np.zeros([nsites, npoes, nimts], dtype=float) + np.nan
                results["KS-PROB"] = np.zeros([nsites, npoes, nimts], dtype=float) + np.nan
            else:
                results[metric] = np.zeros([nsites, npoes, nimts], dtype=float) + np.nan

        pool = Pool(self.num_proc)
        for i, imt in enumerate(imts):
            for j, poe in enumerate(poes):
                values1 = hmaps1[:, :, j, i]
                values2 = hmaps2[:, :, j, i]
                for metric in list(results):

                    if metric in ("overlap", "OI"):
                        o_i = pool.starmap(
                            overlap_index,
                            [(values1[k, :], values2[k, :], weights1, weights2, self.oi_bins)
                             for k in np.arange(0, nsites)]
                        )
                        results[metric][:, j, i] = np.array(o_i)
                    elif metric in ("Wasserstein", "WS"):
                        w_s = pool.starmap(
                            wasserstein_distance,
                            [(values1[k, :], values2[k, :], weights1, weights2)
                             for k in np.arange(0, nsites)]
                        )
                        results[metric][:, j, i] = np.array(w_s)
                    elif metric in ("Kolmogorov-Smirnov", "KS"):
                        k_s = pool.starmap(
                            ks_weighted,
                            [(values1[k, :], values2[k, :], weights1, weights2)
                             for k in np.arange(0, nsites)]
                        )
                        k_s = np.array(k_s)
                        results["KS-DIST"] = k_s[:, 0]
                        results["KS-PROP"] = k_s[:, 1]
                    elif metric in ("mean difference",):
                        results[metric] = mean_difference(values1, values2, weights1, weights2)
                    elif metric.startswith("quantile difference"):
                        quantiles = np.array(
                            map(float, metric.split("|")[1].split("-"))
                            ) / 100.
                        results[metric] = np.array(quantile_difference(values1, values2,
                                                                       weights1, weights2,
                                                                       quantiles))
                    elif metric in ("iqr_ratio",):
                        results["metric"] = iqr_ratio(values1, values2, weights1, weights2)
                    else:
                        pass
            pool.close()
            return results, imts, poes

    @staticmethod
    def _get_hmaps(
            curves: np.ndarray,
            imtls: Dict,
            poes: Union[List, np.ndarray],
            num_proc: int = 8) -> np.ndarray:
        """Retreive the hazard maps from the curves

        Args:
            curves: Hazard curves with shape [nsites, nbranches, nimts, nimls]
            imtls: Intensity measure types and levels (dictionary of numpy arrays)
            poes: Target probabilities of exceedance for hazard maps

        Returns:
            hmaps: Hazard maps with shape [nsites, nbranches, npoes, nimts]
        """
        imts = list(imtls)
        nimts = len(imts)
        npoes = len(poes)
        nsites, nbranches, nimts1, nimls = curves.shape
        assert nimts == nimts1,\
            "Mismatch between number of IMTs in IMTLs and curves shape (%g != %g)"\
            % (nimts, nimts1)
        hmaps = np.zeros([nsites, nbranches, npoes, nimts])
        for i, (imt, imls) in enumerate(imtls.items()):
            logging.info(" ... Retreiving hazard maps for IMT %s (%g of %g)"
                         % (imt, i + 1, nimts))
            pool = Pool(num_proc)
            A = pool.map(fast_compute_hazard_map,
                         [(curves[:, j, :, i], imls, poes)
                          for j in range(nbranches)])
            pool.close()
            for j in range(nbranches):
                hmaps[:, j, :, i] = A[j]
        return hmaps


def setup_arg_parser():
    description = ("Extract/explore OpenQuake hazard results")
    parser = argparse.ArgumentParser(description=description,
                                     add_help=True,
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("-db", "--dbfile",
                        help="Path to hdf5 database for storing results",
                        required=True)

    parser.add_argument("-id", "--calc-id",
                        help="OpenQuake Calculation ID",
                        required=True)

    parser.add_argument("-n", "--name",
                        help="Model Name (to identify in group)",
                        required=True)

    parser.add_argument("-t", "--calc-type",
                        choices=["classical", "ses"],
                        help="Calculation type ('classical' or 'ses')",
                        required=True)

    parser.add_argument("-gs", "--gsim-map",
                        help="./path/to/gsim_mapping_file.json",
                        required=False,
                        default=None)

    parser.add_argument("-sl", "--string-length",
                        help="Maximum string length (characters) for GSIM representation",
                        required=False,
                        default=200)
    return parser


if __name__ == "__main__":
    parser = setup_arg_parser()
    args = parser.parse_args()
    if args.gsim_map:
        with open(args.gsim_map, "r") as f:
            gsim_map = json.load(f)
    else:
        gsim_map = None
    logging.info("Opening Database %s" % args.dbfile)
    manager = OpenQuakeHazardOutputManager(
        args.dbfile
    )
    logging.info("Adding model %s" % args.name)
    calc_id = int(args.calc_id) if not args.calc_id.endswith(".hdf") else args.calc_id
    manager.add_hazard_results(
        calc_id,
        args.calc_type,
        args.name,
        gsim_map=gsim_map,
        str_len=int(args.string_length)
        )
