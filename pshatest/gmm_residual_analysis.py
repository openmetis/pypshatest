"""
General Code for a flatfile against GMPEs

Note that in order for this to work the R installation requires lme4 and lmerTest

In the R environment run:
```
install.packages("nloptr")
install.packages("lme4")
install.packages("lmerTest")
```
It may also be necessry to install cmake via the command line
"""
import os
from typing import Dict, List, Tuple, Union, Optional
import h5py
import numpy as np
import pandas as pd
import logging
from openquake.hazardlib.imt import SA, from_string
from openquake.hazardlib.gsim import get_available_gsims
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm, Normalize
# Requires R2py
from rpy2.robjects.packages import importr
from rpy2.robjects.conversion import localconverter
from rpy2.robjects import pandas2ri, numpy2ri
import rpy2.robjects as robjects
import rpy2.rinterface as rinterface
logging.basicConfig(level=logging.INFO)


ALL_GSIMS = get_available_gsims()


# Import R libraries we need
base = importr("base")
stats = importr("stats")
numpy2ri.activate()


def pandas2R(df):
    """Local conversion of pandas dataframe to R dataframe as recommended by rpy2"""
    with localconverter(robjects.default_converter + pandas2ri.converter):
        data = robjects.conversion.py2rpy(df)
    return data


def R2pandas(df):
    """Local conversion of R dataframe to pandas dataframe as recommended by rpy2"""
    with localconverter(robjects.default_converter + pandas2ri.converter):
        data = robjects.conversion.rpy2py(df)
    return data


class LmerFit(object):
    """
    General class to attempt to fit a linear mixed effects regression
    model by wrapping the R Lmer code using r2py.
    """

    def __init__(self, formula, data, family="gaussian"):
        """
        """
        self.family = family
        self.formula = formula.replace(" ", "")
        self.data = data.copy()
        self.grps = None
        self.AIC = None
        self.logLike = None
        self.ranef_var = None
        self.ranef_corr = None
        self.ranef = None
        self.fixef = None
        self.residuals = None
        self.coeffs = None
        self.model_obj = None
        self.design_matrix = None
        self.grps = None
        self.warnings = []

    def get_mixed_effect_residuals(self, summary=True, REML=True, control="bobyqa",
                                   no_warnings=False):
        """Fits a model using lmer
        """
        control = "optimizer='{:s}'".format(control)
        data = pandas2R(self.data)
        lmer = importr("lmerTest")
        lmc = robjects.r(f"lmerControl({control})")
        contrasts = rinterface.NULL
        self.model_obj = lmer.lmer(self.formula, data=data, REML=REML,
                                   control=lmc, contrasts=contrasts)
        summary = base.summary(self.model_obj)
        unsum = base.unclass(summary)
        self.AIC = unsum.rx2("AICtab")[0]
        self.logLike = unsum.rx2("logLik")[0]
        # Print the warnings, if there are any
        # First check for lme4 printed messages
        # (e.g. convergence info is usually here instead of in warnings)
        fit_messages = unsum.rx2("optinfo").rx2("conv").rx2("lme4").rx2("messages")
        # Then check warnings for additional stuff
        fit_warnings = unsum.rx2("optinfo").rx2("warnings")

        try:
            fit_warnings = [fw for fw in fit_warnings]
        except TypeError:
            fit_warnings = []
        try:
            fit_messages = [fm for fm in fit_messages]
        except TypeError:
            fit_messages = []

        fit_messages_warnings = fit_warnings + fit_messages
        if fit_messages_warnings:
            self.warnings.extend(fit_messages_warnings)
            if not no_warnings:
                for warning in self.warnings:
                    if isinstance(warning, list) | isinstance(warning, np.ndarray):
                        for w in warning:
                            logging.info(w + " \n")
                    else:
                        logging.info(warning + " \n")
        else:
            self.warnings = []
        # Extract the fixed effects
        rstring = """
            function(model){
            getIndex <- function(df){
                    orignames <- names(df)
                    df <- transform(df, index=row.names(df))
                    names(df) <- append(orignames, c("index"))
                    df
                    }
            out <- coef(model)
            out <- lapply(out, getIndex)
            out
            }
         """
        fixef_func = robjects.r(rstring)
        fixefs = fixef_func(self.model_obj)
        fixefs = [R2pandas(e).drop(columns=["index"]) for e in fixefs]

        # Extract the random effects
        rstring = """
            function(model){
            uniquify <- function(df){
            colnames(df) <- make.unique(colnames(df))
            df
            }
            getIndex <- function(df){
            df <- transform(df, index=row.names(df))
            df
            }
            out <- lapply(ranef(model),uniquify)
            out <- lapply(out, getIndex)
            out
            }
        """
        ranef_func = robjects.r(rstring)
        ranefs = ranef_func(self.model_obj)
        if len(ranefs) > 1:
            ranefs = [
                R2pandas(e).drop(columns=["index"]) for e in ranefs
            ]
        else:
            ranefs = R2pandas(ranefs[0]).drop(
                columns=["index"]
            )

        # Model residuals
        rstring = """
            function(model){
            out <- resid(model)
            out
            }
        """
        resid_func = robjects.r(rstring)
        residuals = np.array(resid_func(self.model_obj))
        unsum = self.get_variance_covariance(unsum)
        return ranefs, fixefs, residuals, unsum

    def get_variance_covariance(self, unsum):
        """"""
        df = R2pandas(base.data_frame(unsum.rx2("varcor")))
        return df

    def _get_ngrps(self, unsum, base):
        """Get the groups information from the model as a dictionary"""
        # This works for 2 grouping factors
        ns = unsum.rx2("ngrps")
        names = base.names(self.model_obj.slots["flist"])
        self.grps = dict(zip(names, ns))


def check_infs_and_nans(resid):
    """Checks the residuals for invalid floats (i.e. inf and nan)

    Args:
        resid: Outputs of the residual calculations
    """
    logging.info("Checking residuals for infs and nans")
    for key in resid.predictions:
        if np.any(np.isinf(resid.predictions[key])):
            for i, imt in enumerate(resid.imts):
                idx = np.where(np.isinf(resid.predictions[key][:, i]))[0]
                if len(idx):
                    logging.info(key, resid.imts[i], resid.metadata.iloc[idx, :])
        elif np.any(np.isnan(resid.predictions[key])):
            for i, imt in enumerate(resid.imts):
                idx = np.where(np.isnan(resid.predictions[key][:, i]))[0]
                if len(idx):
                    logging.info(key, resid.imts[i], resid.metadata.iloc[idx, :])
    logging.info("Checks completed")
    return


class Residuals(object):
    """Class to calculate the residuals of the data with respect to the selected GMPEs

    Attributes:
        imts: List of intensity measure types for calculating residuals (not necessarily the
              same as the list found in the flatfile (though cannot accept values outside the
              spectral period range)
        gmpes: The list of GMPEs as instantiated OpenQuake GMPE classes
        labels: List of labels for the GMPEs (used to identify the GMPE in the output)

    Args:
        gmdata: Ground motion data and metadata as output from
                `pshatest.flatfile_handler.FlatfileFormatter.setup_gmvs()`
        gmpes_labels: The set of GMPEs and their respective labels in the form of a list
                      of tuples of GMPE Label and OpenQuake GMPE object, e.g.::

                          gmpe_lables = [("ASK14", AbrahamsonEtAl2014()),
                                         ("BSSA14", BooreEtAl2014()]

        imts: List of intensity measures as OpenQuake Intensity Measure Type objects, e.g.::

                  imts = [PGA(), SA(0.2), SA(1.0), ...]

    """
    def __init__(self, gmdata: Dict, gmpes_labels: List, imts: List):
        """Instantiate Residuals object
        """
        self.imts = imts
        self.labels, self.gmpes = zip(*gmpes_labels)
        self.indices = []
        self.event_ids = gmdata["metadata"]["ev_id"]
        self.station_ids = gmdata["metadata"]["sta"]
        self.record_ids = gmdata["metadata"]["gmid"]
        self.num_recs = gmdata["metadata"].shape[0]
        self.num_events = len(self.event_ids.unique())
        self.num_stations = len(self.station_ids.unique())
        self.metadata = gmdata["metadata"]
        self.flatfile = pd.concat([gmdata["metadata"], gmdata["gmvs"]], axis=1)
        self.ctx = gmdata["ctx"]
        self.observations = None
        self.obs_imts = gmdata["imts"]
        self._get_obs_imts(gmdata)
        self.predictions = {}
        self.stddevs = {"total": {},
                        "between_event": {},
                        "within_event": {}}
        self.ref_residuals = {"between_event": {},
                              "between_station": {},
                              "within_event": {}}
        for gmpe_label in self.labels:
            # In this case the arrays must be [nimts, 1, num_recs] in order
            # for them to be [1, num_recs] in the GMM
            self.predictions[gmpe_label] = np.zeros([len(self.imts), 1, self.num_recs])
            self.stddevs["total"][gmpe_label] = np.zeros([len(self.imts), 1, self.num_recs])
            self.stddevs["between_event"][gmpe_label] =\
                np.zeros([len(self.imts), 1, self.num_recs])
            self.stddevs["within_event"][gmpe_label] =\
                np.zeros([len(self.imts), 1, self.num_recs])

    def _get_obs_imts(self, gmdata: Dict):
        """Get the observed ground motions for the selected target IMTs, interpolating
        to new periods if necessary and returning NaNs if the target IMT is outside the
        observed period range
        """

        target_gmvs = {}
        nobs = gmdata["gmvs"].shape[0]

        obs_periods = np.array([np.nan if str(imt) in ("PGA", "PGV") else imt.period
                                for imt in self.obs_imts])
        low_t_obs = np.nanmin(obs_periods)
        high_t_obs = np.nanmax(obs_periods)

        for i, imt in enumerate(self.imts):
            if str(imt) in ("PGV", "PGA") and str(imt) in gmdata["gmvs"].columns:
                # IMT is PGA or PGV and this is available in the data
                target_gmvs[str(imt)] = np.log(gmdata["gmvs"][str(imt)].to_numpy())
            elif str(imt).startswith("SA("):
                # Is an SA
                target_period = imt.period
                if target_period < low_t_obs:
                    # Required SA is below the observed period range - assign NaNs
                    logging.info(
                        "Cannot get residuals for %s (below obs. period range [%.3f - %.3f])"
                        % (str(imt), low_t_obs, high_t_obs))
                    target_gmvs[str(imt)] = np.nan * np.ones(nobs)
                elif target_period > high_t_obs:
                    # Required SA is above the observed period range - assign NaNs
                    logging.info(
                        "Cannot get residuals for %s (above obs. period range [%.3f - %.3f])"
                        % (str(imt), low_t_obs, high_t_obs))
                    target_gmvs[str(imt)] = np.nan * np.ones(nobs)
                elif "pSA_{:.3f}".format(target_period) in gmdata["gmvs"].columns:
                    # IMT is in the data columns - no need to interpolate
                    target_gmvs[str(imt)] = np.log(
                        gmdata["gmvs"]["pSA_{:.3f}".format(imt.period)].to_numpy()
                        )
                else:
                    # Interpolate to period
                    idx = np.searchsorted(obs_periods, target_period)
                    gmvs_low = np.log(gmdata["gmvs"].iloc[:, idx - 1])
                    gmvs_high = np.log(gmdata["gmvs"].iloc[:, idx])
                    per_low, per_high = np.log(obs_periods[idx - 1]), np.log(obs_periods[idx])
                    target_gmvs[str(imt)] = gmvs_low + (np.log(target_period) - per_low) *\
                        ((gmvs_high - gmvs_low) / (per_high - per_low))
        self.observations = pd.DataFrame(target_gmvs, index=gmdata["gmvs"].index)
        return

    def run(self):
        """Execute retrieval of predictions and calculation of residuals
        """
        self.get_predictions()
        self.get_random_effects_residuals()
        return

    def get_predictions(self):
        """Gets the expected ground motions from the GMPEs
        """
        if self.ctx is None:
            raise ValueError(
                "No context object provided (possibly created from stored results file)"
                )
        logging.info("Building scenario observations ...")
        for gmpe_label, gmpe in zip(self.labels, self.gmpes):
            logging.info("GMPE: %s" % gmpe_label)
            for i, imt in enumerate(self.imts):
                try:
                    gmpe.compute(
                        self.ctx,
                        [imt],
                        self.predictions[gmpe_label][i, :, :],
                        self.stddevs["total"][gmpe_label][i, :, :],
                        self.stddevs["between_event"][gmpe_label][i, :, :],
                        self.stddevs["within_event"][gmpe_label][i, :, :]
                        )
                except (KeyError, AttributeError):
                    logging.info("-- No predictions for IMT: %s" % str(imt))
                    continue
        return

    def get_random_effects_residuals(self):
        """Calculates the between-event, within-event and between station residuals from Lme4
        """
        for gmpe_label, gmpe in zip(self.labels, self.gmpes):
            logging.info("Calculating residuals for GMPE %s" % gmpe_label)
            self.ref_residuals["between_event"][gmpe_label] = {}
            self.ref_residuals["between_station"][gmpe_label] = {}
            self.ref_residuals["within_event"][gmpe_label] = {}
            for i, imt in enumerate(self.imts):
                imt_str = str(imt)
                logging.info("---- %s" % imt_str)
                if np.all(pd.isna(self.observations[imt_str])):
                    logging.info("No GM observations for %s - skipping" % imt_str)
                    continue
                if np.allclose(np.fabs(self.predictions[gmpe_label][i, 0, :]), 0.0):
                    logging.info("No IMT %s available for %s - skipping"
                                 % (imt_str, gmpe_label))
                    self.ref_residuals["between_event"][gmpe_label][imt_str] =\
                        np.zeros(self.num_events)
                    self.ref_residuals["between_station"][gmpe_label][imt_str] =\
                        np.zeros(self.num_stations)
                    continue
                df = self._get_dataframe_model_imt(gmpe_label, i)
                # save_stdout = sys.stdout
                # sys.stdout = open(os.devnull, 'w')  # Suppress Lmer print functionality
                model = LmerFit(
                    "obs ~ 0 + offset(model) + (1|event_number) + (1|site_number)",
                    data=df,
                )
                ranef, fixef, residuals, unsum = model.get_mixed_effect_residuals()
                # sys.stdout = save_stdout  # Enable print functionality
                if ranef[0].shape[0] == self.num_events:
                    # Between event residuals in model.ranef[0] position
                    self.ref_residuals["between_event"][gmpe_label][imt_str] =\
                        ranef[0].to_numpy()[:, 0]
                    # Between station residuals in model.ranef[1] position
                    self.ref_residuals["between_station"][gmpe_label][imt_str] =\
                        ranef[1].to_numpy()[:, 0]
                elif ranef[0].shape[0] == self.num_stations:
                    # Between event residuals in model.ranef[1] position
                    self.ref_residuals["between_event"][gmpe_label][imt_str] =\
                        ranef[1].to_numpy()[:, 0]
                    # Between station residuals in model.ranef[0] position
                    self.ref_residuals["between_station"][gmpe_label][imt_str] =\
                        ranef[0].to_numpy()[:, 0]
                self.ref_residuals["within_event"][gmpe_label][imt_str] = residuals
            # For between event and between station terms, concatenate the dataframes
            if ranef[0].shape[0] == self.num_events:
                index_dbe = ranef[0].index
                index_ds2s = ranef[1].index
            else:
                index_dbe = ranef[1].index
                index_ds2s = ranef[0].index
            self.ref_residuals["between_event"][gmpe_label] = pd.DataFrame(
                self.ref_residuals["between_event"][gmpe_label], index=index_dbe)
            self.ref_residuals["between_station"][gmpe_label] = pd.DataFrame(
                self.ref_residuals["between_station"][gmpe_label], index=index_ds2s)
            self.ref_residuals["within_event"][gmpe_label] = pd.DataFrame(
                self.ref_residuals["within_event"][gmpe_label], index=self.record_ids)
        return

    def get_random_effects_residuals_abrahamson_youngs(self):
        """Calculates the between and within event residuals using the method of
        Abrahamson & Youngs (1992). Does not return station-to-station residual
        """
        for gmpe_label, gmpe in zip(self.labels, self.gmpes):
            logging.info("Calculating residuals for GMPE %s" % gmpe_label)
            self.ref_residuals["between_event"][gmpe_label] = {}
            self.ref_residuals["between_station"][gmpe_label] = {}
            self.ref_residuals["within_event"][gmpe_label] = {}
            for i, imt in enumerate(self.imts):
                imt_str = str(imt)
                logging.info("---- %s" % imt_str)
                if np.all(pd.isna(self.observations[imt_str])):
                    logging.info("No GM observations for %s - skipping" % imt_str)
                    continue
                if np.allclose(np.fabs(self.predictions[gmpe_label][i, 0, :]), 0.0):
                    logging.info("No IMT %s available for %s - skipping"
                                 % (imt_str, gmpe_label))
                    self.ref_residuals["between_event"][gmpe_label][imt_str] =\
                        np.zeros(self.num_recs)
                    self.ref_residuals["between_station"][gmpe_label][imt_str] =\
                        np.zeros(self.num_recs)
                    self.ref_residuals["within_event"][gmpe_label][imt_str] =\
                        np.zeros(self.num_recs)
                    continue
                dbe, dwes, _ = self.abrahamson_youngs_1992_mef(
                    self.event_ids, self.observations.iloc[:, i],
                    self.predictions[gmpe_label][i, 0, :],
                    self.stddevs["between_event"][gmpe_label][i, 0, :],
                    self.stddevs["within_event"][gmpe_label][i, 0, :],
                    self.stddevs["total"][gmpe_label][i, 0, :],
                    normalize=False)
                self.ref_residuals["between_event"][gmpe_label][imt_str] = dbe
                self.ref_residuals["within_event"][gmpe_label][imt_str] = dwes
            self.ref_residuals["between_event"][gmpe_label] = pd.DataFrame(
                self.ref_residuals["between_event"][gmpe_label], index=self.record_ids)
            self.ref_residuals["within_event"][gmpe_label] = pd.DataFrame(
                self.ref_residuals["within_event"][gmpe_label], index=self.record_ids)
        return

    @staticmethod
    def abrahamson_youngs_1992_mef(
            ev_ids: np.ndarray, obs: np.ndarray, median: np.ndarray,
            inter: np.ndarray, intra: np.ndarray, total: Optional[np.ndarray] = None,
            normalize: bool = False,
            ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Implements the derivation of mixed effects residuals according to the
        algorithm of Abrahamson & Youngs (1992)

        Args:
            ev_ids: Unique IDs of the events
            obs: Natural logarithm of the observed ground motions
            median: Natural logarithm of the predicted ground motion
            inter: Inter-event standard deviation of the predicted ground motion
            intra: Intra-event standard deviation of the predicted ground motion
            total: Total standard deviation of the predicted ground motion
            normalize: Return the residuals normalized with respect to the model standard
                       deviation

        Returns:
            inter_res: Between-event residuals
            intra_res: Within-event residuals
            total_res: Total residuals
        """

        if total is None:
            total = np.sqrt(inter ** 2.0 + intra ** 2.0)
        total_res = obs - median
        inter_res = np.zeros(obs.shape[0])
        intra_res = np.zeros(obs.shape[0])
        for ev_id in pd.unique(ev_ids):
            idx = ev_ids == ev_id
            nvals = np.sum(idx)
            inter_res[idx] = ((inter[idx] ** 2.) * sum(total_res[idx])) /\
                (nvals * (inter[idx] ** 2.) + (intra[idx] ** 2.))
            intra_res[idx] = obs[idx] - (median[idx] + inter_res[idx])
            if normalize:
                inter_res[idx] /= inter[idx]
                intra_res[idx] /= intra[idx]
                total_res[idx] /= total[idx]
        return inter_res, intra_res, total_res

    def _get_dataframe_model_imt(self, gmpe_label: str, i: Union[str, int]) -> pd.DataFrame:
        """For running the mixed effects residuals the model, observation and event and
        station IDs need to be input into a single dataframe with labels `obs`, `model`
        `event_number` and `site_number`

        Args:
            gmpe_label: Indicates which GMPE this corresponds to
            i: Either the IMT or the position of the IMT in the list

        Returns:
            Simple dataframe with observation, model, event ID and site ID
        """
        if isinstance(i, str):
            i = [str(imt) for imt in self.imts].index(i)
        df = {
            "obs": self.observations.iloc[:, i],
            "event_number": self.metadata["ev_id"].astype(str),
            "site_number": self.metadata["sta"],
            "model": self.predictions[gmpe_label][i, 0, :]
        }
        return pd.DataFrame(df)

    def to_hdf5(self, dbfile: str):
        """Export the results to an hdf5 file

        Args:
            dbfile: Path to hdf5 file for storing the data (the file should not currently
                    exist)
        """
        if os.path.exists(dbfile):
            raise IOError("DB file %s should not exist" % dbfile)
        fle = h5py.File(dbfile, "w-")
        fle.attrs["IMT"] = [str(imt) for imt in self.imts]
        fle.attrs["num_events"] = self.num_events
        fle.attrs["num_stations"] = self.num_stations
        fle.attrs["num_recs"] = self.num_recs
        idx_dset = fle.create_dataset("indices",
                                      (len(self.indices),),
                                      dtype=h5py.vlen_dtype(np.dtype("int32")))
        for i, idx in enumerate(self.indices):
            idx_dset[i] = idx

        # Observations
        obs = fle.create_dataset("Observations", self.observations.shape, dtype="f")
        obs[:] = self.observations
        # Predictions
        pred = fle.create_group("Predictions")
        for gmpe_str in self.labels:
            gmpe_grp = pred.create_group(gmpe_str)
            # Mean
            mean_dset = gmpe_grp.create_dataset("Mean",
                                                self.predictions[gmpe_str].shape,
                                                dtype="f")
            mean_dset[:] = self.predictions[gmpe_str]
            # Total
            total_stddev_dset = gmpe_grp.create_dataset("total_stddev",
                                                        self.stddevs["total"][gmpe_str].shape,
                                                        dtype="f")
            total_stddev_dset[:] = self.stddevs["total"][gmpe_str]
            # Inter-event
            inter_stddev_dset = gmpe_grp.create_dataset(
                "between_event_stddev",
                self.stddevs["between_event"][gmpe_str].shape,
                dtype="f")
            inter_stddev_dset[:] = self.stddevs["between_event"][gmpe_str]
            # Intra-event
            intra_stddev_dset = gmpe_grp.create_dataset(
                "within_event_stddev",
                self.stddevs["within_event"][gmpe_str].shape,
                dtype="f")
            intra_stddev_dset[:] = self.stddevs["within_event"][gmpe_str]
        fle.close()
        # Now store the Pandas dataframes
        store = pd.HDFStore(dbfile, "a")
        store.put("metadata", self.metadata)
        store.put("flatfile", self.flatfile)
        for gmpe_str in self.labels:
            store.put("RandomEffects/{:s}/between_event/".format(gmpe_str),
                      self.ref_residuals["between_event"][gmpe_str])
            store.put("RandomEffects/{:s}/between_station/".format(gmpe_str),
                      self.ref_residuals["between_station"][gmpe_str])
            store.put("RandomEffects/{:s}/within_event".format(gmpe_str),
                      self.ref_residuals["within_event"][gmpe_str])
        store.close()
        return

    @classmethod
    def from_hdf5(cls, dbfile: str, gmpes_labels: List):
        """Instantiate the class from an existing database file with stored results. Mostly
        this is intended for visualization and/or post-processing previous results rather than
        running new analyses.

        Args:
            dbfile: Path to existing database file (.hdf5)
            gmpes_labels: List of tuples of ("GMPE_LABEL", GMPE) where "GMPE_LABEL" is a simple
                          label and GMPE an instantiated OpenQuake GMM object
        """
        # Get the data stored as pandas Dataframrs
        gmdata = {}
        gmdata["metadata"] = pd.read_hdf(dbfile, "metadata")
        gmdata["ctx"] = None
        flatfile = pd.read_hdf(dbfile, "flatfile")
        gmv_cols = []
        gmdata["imts"] = []
        # Need to separate out the metadata from the flatfile to retreive the GMVs
        # Also need the IMTs of the original data
        for col in flatfile.columns:
            if col not in gmdata["metadata"].columns:
                gmv_cols.append(col)
            if col.startswith("pSA"):
                gmdata["imts"].append(SA(float(col.replace("pSA_", ""))))
            elif col in ("PGA", "PGV"):
                gmdata["imts"].append(from_string(col))
        gmdata["gmvs"] = flatfile[gmv_cols]
        # Can now open the database
        fle = h5py.File(dbfile, "r")
        imts = [from_string(imt) for imt in fle.attrs["IMT"]]
        # Instantiate class
        resids = cls(gmdata, gmpes_labels, imts)
        assert resids.num_events == fle.attrs["num_events"]
        assert resids.num_stations == fle.attrs["num_stations"]
        assert resids.num_recs == fle.attrs["num_recs"]
        resids.observations = fle["Observations"][:]
        resids.indices = fle["indices"][:]
        # Extract the predictions and standard deviations
        for key in list(fle["Predictions"]):
            if key not in resids.labels:
                raise ValueError("GMM %s from file not found in GMM labels" % key)
            resids.predictions[key] = fle["Predictions/{:s}/Mean".format(key)][:]
            resids.stddevs["total"][key] = fle["Predictions/{:s}/total_stddev".format(key)][:]
            resids.stddevs["between_event"][key] =\
                fle["Predictions/{:s}/between_event_stddev".format(key)][:]
            resids.stddevs["within_event"][key] =\
                fle["Predictions/{:s}/within_event_stddev".format(key)][:]
        fle.close()
        # Extract the residuals
        for key in resids.labels:
            resids.ref_residuals["between_event"][key] = pd.read_hdf(
                dbfile,
                "RandomEffects/{:s}/between_event".format(key))
            resids.ref_residuals["between_station"][key] = pd.read_hdf(
                dbfile,
                "RandomEffects/{:s}/between_station".format(key))
            resids.ref_residuals["within_event"][key] = pd.read_hdf(
                dbfile,
                "RandomEffects/{:s}/within_event".format(key))
        return resids


class ResidualVisualizations():
    """
    Class to handle visualizations of the residuals with respect to different
    predictor variables

    Attributes:
        residuals: Calculation residuals
    """
    def __init__(self, residuals: Residuals):
        """
        """
        self.residuals = residuals

    def between_event(self, gmm: str, imt: str, pred_var: str, col_var: Optional[str] = None,
                      pred_var_units: str = "", col_var_units: str = "",
                      xlim: Tuple = (), ylim: Tuple = (), clim: Tuple = (),
                      xscale: str = "lin", yscale: str = "lin", cscale: str = "lin",
                      cmap: str = "magma", figsize: Tuple = (10, 5),
                      filename: str = "", filetype: str = "png", dpi: int = 300,
                      bins: Optional[np.ndarray] = None,):
        """Creates a plot of between-event residuals with respect to a given
        predictor variable
        """
        dbe = self.residuals.ref_residuals["between_event"][gmm][imt]
        if np.allclose(dbe.to_numpy(), 0.0):
            print("No between-event residuals for GMM %s and IMT %s" % (gmm, imt))
            return
        keys = ["ev_id", pred_var]
        if col_var:
            keys.append(col_var)
        # Sort the predictors to get event terms
        predictors = self.residuals.metadata[keys]
        predictors.drop_duplicates("ev_id", inplace=True, ignore_index=True)
        predictors.set_index("ev_id", inplace=True)
        predictors = predictors.loc[dbe.index]

        # Create the figure
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)
        if col_var:
            # Get the values and label for colour scaling
            cvals = predictors[col_var]
            clabel = "%s (%s)" % (col_var, col_var_units)
        else:
            cvals = None
            clabel = ""
        # Get x-, and y- labels then plot
        ylabel = r"$\delta B_e$ %s" % imt
        xlabel = "%s %s" % (pred_var, pred_var_units)
        ax, cbar = self._plot(fig, ax, dbe, predictors[pred_var], ylabel, xlabel, cvals,
                              clabel, xlim, ylim, clim, xscale, yscale, cscale, cmap, bins)
        ax.set_title(gmm, fontsize=20)
        # Tidy figure and save
        fig.tight_layout()
        if filename:
            plt.savefig(filename, format=filetype, dpi=dpi, bbox_inches="tight")
        return

    def between_station(self, gmm: str, imt: str, pred_var: str, col_var: Optional[str] = None,
                        pred_var_units: str = "", col_var_units: str = "",
                        xlim: Tuple = (), ylim: Tuple = (), clim: Tuple = (),
                        xscale: str = "lin", yscale: str = "lin", cscale: str = "lin",
                        cmap: str = "magma", figsize: Tuple = (10, 5),
                        filename: str = "", filetype: str = "png", dpi: int = 300,
                        bins: Optional[np.ndarray] = None,):
        """Creates a plot of between-station residuals with respect to a given
        predictor variable
        """
        ds2s = self.residuals.ref_residuals["between_station"][gmm][imt]
        if np.allclose(ds2s.to_numpy(), 0.0):
            print("No between-event residuals for GMM %s and IMT %s" % (gmm, imt))
            return
        keys = ["sta", pred_var]
        if col_var:
            keys.append(col_var)
        predictors = self.residuals.metadata[keys]
        predictors.drop_duplicates("sta", inplace=True, ignore_index=True)
        predictors.set_index("sta", inplace=True)
        predictors = predictors.loc[ds2s.index]

        # Create the figure
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)
        if col_var:
            # Get the values and label for colour scaling
            cvals = predictors[col_var]
            clabel = "%s (%s)" % (col_var, col_var_units)
        else:
            cvals = None
            clabel = ""
        # Get x-, and y- labels then plot
        ylabel = r"$\delta S2S_s$ %s" % imt
        xlabel = "%s %s" % (pred_var, pred_var_units)
        ax, cbar = self._plot(fig, ax, ds2s, predictors[pred_var], ylabel, xlabel, cvals,
                              clabel, xlim, ylim, clim, xscale, yscale, cscale, cmap, bins)
        ax.set_title(gmm, fontsize=20)
        # Tidy figure and save
        fig.tight_layout()
        if filename:
            plt.savefig(filename, format=filetype, dpi=dpi, bbox_inches="tight")
        return

    def within_event(self, gmm: str, imt: str, pred_var: str, col_var: Optional[str] = None,
                     pred_var_units: str = "", col_var_units: str = "",
                     xlim: Tuple = (), ylim: Tuple = (), clim: Tuple = (),
                     xscale: str = "lin", yscale: str = "lin", cscale: str = "lin",
                     cmap: str = "magma", figsize: Tuple = (10, 5),
                     filename: str = "", filetype: str = "png", dpi: int = 300,
                     bins: Optional[np.ndarray] = None,):
        """Creates a plot of within-event residuals with respect to a given
        predictor variable
        """
        dwes = self.residuals.ref_residuals["within_event"][gmm][imt]
        if np.allclose(dwes.to_numpy(), 0.0):
            print("No within-event residuals for GMM %s and IMT %s" % (gmm, imt))
            return
        keys = ["gmid", pred_var]
        if col_var:
            keys.append(col_var)
        predictors = self.residuals.metadata[keys].reset_index(inplace=False, drop=True)
        predictors.drop_duplicates("gmid", inplace=True,)
        predictors.set_index("gmid", inplace=True, drop=True)
        predictors = predictors.loc[dwes.index]
        # Create the figure
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)
        if col_var:
            # Get the values and label for colour scaling
            cvals = predictors[col_var]
            clabel = "%s (%s)" % (col_var, col_var_units)
        else:
            cvals = None
            clabel = ""
        # Get x-, and y- labels then plot
        ylabel = r"$\delta W_{es}$ %s" % imt
        xlabel = "%s %s" % (pred_var, pred_var_units)
        ax, cbar = self._plot(fig, ax, dwes, predictors[pred_var], ylabel, xlabel, cvals,
                              clabel, xlim, ylim, clim, xscale, yscale, cscale, cmap, bins)
        ax.set_title(gmm, fontsize=20)
        # Tidy figure and save
        fig.tight_layout()
        if filename:
            plt.savefig(filename, format=filetype, dpi=dpi, bbox_inches="tight")
        return

    def _plot(self, fig, ax, yvals, xvals, ylabel: str, xlabel: str,
              cvals: Optional[pd.Series] = None, clabel: Optional[str] = "",
              xlim: Tuple = (), ylim: Tuple = (), clim: Tuple = (),
              xscale: str = "lin", yscale: str = "lin", cscale: str = "lin",
              cmap: str = "magma", bins: Optional[np.ndarray] = None,):
        """Common plotting functionality
        """
        if cvals is not None:
            if len(clim):
                norm = LogNorm(*clim) if cscale == "log" else Normalize(*clim)
            else:
                norm = None
            cax = ax.scatter(xvals, yvals, c=cvals, marker="o", cmap=cmap, norm=norm,
                             edgecolor="darkgrey", linewidth=0.5, zorder=1)
            cbar = fig.colorbar(cax, pad=0.03)
            cbar.ax.set_ylabel(clabel, fontsize=16)
            cbar.ax.tick_params(labelsize=12)
        else:
            ax.plot(xvals, yvals, "o", mec="k", mfc="gold", zorder=1)
            cbar = None
        if bins is not None:
            # Show the distribution of results per bins in terms of a violin plot
            midpoints = (bins[:-1] + bins[1:]) / 2.0
            data = []
            quantiles = []
            for i in range(len(bins) - 1):
                vals = yvals[np.logical_and(xvals >= bins[i],
                                            xvals < bins[i + 1])]
                data.append(vals.to_numpy())
                quantiles.append([0.16, 0.84])
            parts = ax.violinplot(data, positions=midpoints,
                                  widths=np.diff(bins) / 2.0,
                                  showmedians=True,
                                  showextrema=False,
                                  quantiles=quantiles,
                                  points=25)
            parts["cmedians"].set_edgecolor("blue")
            parts["cquantiles"].set_edgecolor("blue")
            for pc in parts["bodies"]:
                pc.set_facecolor('cornflowerblue')
                pc.set_edgecolor('blue')
                pc.set_alpha(0.6)

        if len(xlim):
            ax.set_xlim(*xlim)
        if len(ylim):
            ax.set_ylim(*ylim)
        if xscale == "log":
            ax.set_xscale("log")
        if yscale == "log":
            ax.set_yscale("log")
        ax.set_xlabel(xlabel, fontsize=18)
        ax.set_ylabel(ylabel, fontsize=18)
        ax.tick_params(labelsize=12)
        ax.grid(which="both")
        return ax, cbar
