"""
Simple window-based declustering algorithms
"""
import numpy as np
import pandas as pd
from typing import Optional, Union, Tuple


def prepare_catalogue(catalogue_file: str, mmin: Optional[float] = None,
                      start_time: Optional[str] = None,
                      end_time: Optional[str] = None,
                      max_depth: Optional[float] = None) -> pd.DataFrame:
    """
    Prepares a catalogue by reading in from csv, parsing to a pandas Dataframe and
    applying filters by magnitude, depth and time

    Args:
        catalogue_file: Path to the earthquake catalogue file
        mmin: Minimum magnitude
        start_time: Beginning time to select events (will take earliest event if not specified)
        end_time: End time to select events (will take latest event if not specified)
        max_depth: Maximum depth (in km)

    Returns:
        Selected events as a pandas DataFrame
    """
    catalogue = pd.read_csv(catalogue_file, sep=",")
    mmin = -np.inf if mmin is None else mmin
    max_depth = np.inf if max_depth is None else max_depth
    catalogue = catalogue[(catalogue["M"] >= mmin) & (catalogue["depth"] <= max_depth)]
    catalogue["ev_time"] = catalogue["ev_time"].astype("datetime64")
    start_time = np.datetime64(start_time) if start_time else catalogue["ev_time"].min()
    end_time = np.datetime64(end_time) if end_time else catalogue["ev_time"].max()
    catalogue = catalogue[(catalogue["ev_time"] >= start_time) &
                          (catalogue["ev_time"] <= end_time)]
    return catalogue


def _haversine_formula(tlon: Union[float, np.ndarray], tlat: Union[float, np.ndarray],
                       lons: np.ndarray, lats: np.ndarray,
                       earth_rad: float = 6371.224) -> np.ndarray:
    """Implementation of the Haversine formula for a single target location
    and a vector of locations

    Args:
        tlon: Target longitude (decimal degrees)
        tlat: Target latitude (decimal degrees)
        lons: Longitudes for calculating distance (decimal degrees)
        lats: Latitudes for calculating distance (decimal degrees)
        earth_rad: Assumed Earth radius (in km)
    Returns:
        Distances in km from target location to vector of locations
    """
    dlat = lats - tlat
    dlon = lons - tlon
    aval = (np.sin(dlat / 2.) ** 2.) + (np.cos(tlat) * np.cos(lats) *
                                        (np.sin(dlon / 2.) ** 2.))
    return 2.0 * earth_rad * np.arctan2(np.sqrt(aval), np.sqrt(1.0 - aval))


def haversine(lons1: np.ndarray, lats1: np.ndarray, lons2: np.ndarray, lats2: np.ndarray,
              earth_rad: float = 6371.224) -> np.ndarray:
    """Calculates distance between sets of longitudes and latitudes using the Haversine
    formula.

    Args:
        lons1: Longitudes of first vector of sites (decimal degrees) - can be singleton vector
        lats1: Latitudes of first vector of sites (decimal degrees) - can be singleton vector
        lons2: Longitudes of second vector of sites (decimal degrees) - can be singleton vector
        lats2: Latitudes of second vector of sites (decimal degrees) - can be singleton vector
        earth_rad: Assumed average radius of the Earth (in km)
    Returns:
        Distances from (lons1, lats1) to (lons2, lats2) in km
    """
    lons1 = np.radians(lons1)
    lats1 = np.radians(lats1)
    assert lons1.shape == lats1.shape
    lons2 = np.radians(lons2)
    lats2 = np.radians(lats2)
    assert lons2.shape == lats2.shape
    if not len(lons1.shape) and not len(lats1.shape):
        # Simple distance vector
        return _haversine_formula(lons1, lats1, lons2, lats2, earth_rad)
    else:
        # Distance array
        distance = np.zeros([lons1.shape[0], lons2.shape[0]])
        for i, (tlon, tlat) in enumerate(zip(lons1, lats1)):
            distance[i, :] = _haversine_formula(tlon, tlat, lons2, lats2, earth_rad)
        return distance


def distance_window(mag: Union[float, np.ndarray]) -> Union[float, np.ndarray]:
    """Returns the distance window (in km) for the Gruenthal methodology

    Args:
        mag: Magnitude

    Returns:
        distance window in days
    """
    return np.exp(1.77474 + np.sqrt(0.03656 + 1.02237 * mag))


def foreshock_time_window(mag: np.ndarray) -> np.ndarray:
    """Returns the foreshock time window (days) for the Gruenthal methodology

    Args:
        mag: Magnitude

    Returns:
        Foreshock time window in days
    """
    window = np.zeros_like(mag)
    idx = mag < 7.784
    window[idx] = np.exp(-4.76590 + np.sqrt(0.61937 + 17.32149 * mag[idx]))
    idx = np.logical_not(idx)
    window[idx] = np.exp(6.44307 + 0.05515 * mag[idx])
    return window


def aftershock_time_window(mag: np.ndarray) -> np.ndarray:
    """Returns the aftershock time window (days) for the Gruenthal methodology

    Args:
        mag: Magnitude

    Returns:
        Aftershock time window in days
    """
    window = np.zeros_like(mag)
    idx = mag < 6.643
    window[idx] = np.exp(-3.94655 + np.sqrt(0.61937 + 17.32149 * mag[idx]))
    idx = np.logical_not(idx)
    window[idx] = np.exp(6.44307 + 0.05515 * mag[idx])
    return window


def simple_declustering(catalogue: pd.DataFrame) -> Tuple[np.ndarray, np.ndarray]:
    """Applies simple declustering algorithm using the Gardner & Knopoff method adapted
    by Gottfried Gruenthal

    Args:
        catalogue: Earthquake catalogue containing `NEQs` in the form of a dictionary
                   (as prepared using the function `prepare_catalogue`)
    Returns:
        vcl: NEQs length vector indicating the ID of the cluster to which each event is assigned
        flag_vector: NEQs length vector indicating for each event whether it is classified as
                     a foreshock (-1), mainshock (0) or aftershock (1). If unassigned to a
                     cluster then an event is a mainshock. Otherwise the mainshock is the
                     largest event in each cluster and foreshocks/aftershocks are classified
                     with respect to this event.
    """
    # Create numerical ids with original event order
    nids = np.arange(0, len(catalogue["master_id"]))
    # Sort into order of descending magnitude
    sidx = np.argsort(catalogue["M"])[::-1]
    lons, lats, depths, mags, dtimes, ids = (
        catalogue["lon"].to_numpy()[sidx], catalogue["lat"].to_numpy()[sidx],
        catalogue["depth"].to_numpy()[sidx], catalogue["M"].to_numpy()[sidx],
        catalogue["ev_time"].to_numpy()[sidx], nids[sidx]
    )
    # Get time and distance windows
    rwin = distance_window(mags)
    # Time windows returned in terms of days, round up to nearest integer
    fswin = np.ceil(foreshock_time_window(mags)).astype(int)
    fswin = fswin.astype("timedelta64[D]")
    aswin = np.ceil(aftershock_time_window(mags)).astype(int)
    aswin = aswin.astype("timedelta64[D]")
    neqs = len(mags)  # Number of events
    vcl = np.zeros(neqs, dtype=int)  # Set cluster indices to 0
    cluster_counter = 0
    marker = neqs // 10
    for i in range(neqs):
        if i and (i % marker) == 0:
            print("-- %g %% done --" % int(100 * (i / neqs)))
        d_t = (dtimes - dtimes[i]).astype("timedelta64[D]")
        # Find events within forshock and aftershock time window
        t_idx = np.logical_and(d_t >= -fswin[i],
                               d_t <= aswin[i])
        if np.sum(t_idx) <= 1:
            # No events within fore- or aftershock window of this event
            continue
        # Check distance
        rval = haversine(lons[i], lats[i], lons[t_idx], lats[t_idx]).flatten()
        rval = np.sqrt(rval ** 2. + (depths[t_idx] - depths[i]) ** 2.)
        r_idx = rval <= rwin[i]
        if np.sum(r_idx) <= 1:
            # No events within distance window of this event
            continue
        t_idx[t_idx] = np.copy(r_idx)
        if vcl[i]:
            # Considered event was already in a cluster, so new
            # events extend the existing cluster
            vcl[np.logical_or(t_idx, vcl == vcl[i])] = vcl[i]
        else:
            # Is a new cluster
            cluster_counter += 1
            vcl[t_idx] = cluster_counter

    # Sort vcl back into original catalogue order
    vcl = vcl[np.argsort(ids)]
    new_vcl = np.zeros_like(vcl, dtype=int)
    flag_vector = np.zeros(neqs, dtype=int)
    # Loop through clusters and separate foreshock/mainshock/aftershock
    cluster_counter = 1
    for j in range(1, np.max(vcl)):
        idx = np.where(vcl == j)[0]
        if len(idx) <= 1:
            continue
        new_vcl[idx] = cluster_counter
        local_flag = np.zeros(len(idx), dtype=int)
        local_time = catalogue["ev_time"].iloc[idx]
        local_mag = catalogue["M"].iloc[idx]
        max_m = np.argmax(local_mag)
        local_dtime = (local_time - local_time.iloc[max_m]).astype(int)
        local_flag[local_dtime > 0] = 1  # Aftershock
        local_flag[local_dtime < 0] = -1  # Mainshock
        flag_vector[idx] = local_flag
        cluster_counter += 1
    print("-- 100 % done --")
    return vcl, flag_vector


def export_catalogue_dataseries(filename: str, data: pd.DataFrame,
                                vcl: np.ndarray, flagvector: np.ndarray,
                                purge: bool = False):
    """Exports the catalogue to a file including the cluster flags

    Args:
        filename: Path to the filename for the exported catalogue
        data: Earthquake catalogue in pandas dataframe format
        vcl: Vector indicating the cluster assignment for each event (output from declustering
             algorithm)
        flagvector: Vector indicating if the event is a foreshock, mainshock or aftershock (as
                    output from declustering algorith)
        purge: Export only the mainshocks to file (True) or keep all (False)
    If purge is True then returns only the mainshocks
    """
    # Add vcl and flagvector
    data["vcl"] = pd.Series(vcl)
    data["flagvector"] = pd.Series(flagvector)
    if purge:
        # Keep only mainshocks
        data = data[flagvector == 0]
    data.to_csv(filename, sep=",", index=False)
    print("Exported to file %s" % filename)
    return
