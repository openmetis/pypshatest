"""
Ground Motion Predictor metadata validator and fill missing values
"""
import math
import numpy as np
from typing import Optional, List, Dict, Tuple, Union
from openquake.hazardlib.scalerel import WC1994
from openquake.hazardlib.geo import geodetic, Mesh, PlanarSurface, Point
from openquake.hazardlib.geo.surface.base import BaseSurface
from openquake.hazardlib.contexts import RuptureContext
from pshatest.utils import vs30_to_z1pt0_cy14, vs30_to_z2pt5_cb14


def build_rupture(usd, lsd, mag, dims, strike, dip, rake, clon, clat, cdep):
    """
    Function to build the rupture from the available dimension and orientation
    information.

    Copied from the OpenQuake-engine codebase
    """
    # from the rupture center we can now compute the coordinates of the
    # four coorners by moving along the diagonals of the plane. This seems
    # to be better then moving along the perimeter, because in this case
    # errors are accumulated that induce distorsions in the shape with
    # consequent raise of exceptions when creating PlanarSurface objects
    # theta is the angle between the diagonal of the surface projection
    # and the line passing through the rupture center and parallel to the
    # top and bottom edges. Theta is zero for vertical ruptures (because
    # rup_proj_width is zero)
    half_length, half_width, half_height = dims / 2.
    rdip = math.radians(dip)

    # precalculated azimuth values for horizontal-only and vertical-only
    # moves from one point to another on the plane defined by strike
    # and dip:
    azimuth_right = strike
    azimuth_down = azimuth_right + 90
    azimuth_left = azimuth_down + 90
    azimuth_up = azimuth_left + 90

    # half height of the vertical component of rupture width
    # is the vertical distance between the rupture geometrical
    # center and it's upper and lower borders:
    # calculate how much shallower the upper border of the rupture
    # is than the upper seismogenic depth:
    vshift = usd - cdep + half_height
    # if it is shallower (vshift > 0) than we need to move the rupture
    # by that value vertically.
    if vshift < 0:
        # the top edge is below upper seismogenic depth. now we need
        # to check that we do not cross the lower border.
        vshift = lsd - cdep - half_height
        if vshift > 0:
            # the bottom edge of the rupture is above the lower seismo
            # depth; that means that we don't need to move the rupture
            # as it fits inside seismogenic layer.
            vshift = 0
        # if vshift < 0 than we need to move the rupture up.

    # now we need to find the position of rupture's geometrical center.
    # in any case the hypocenter point must lie on the surface, however
    # the rupture center might be off (below or above) along the dip.
    if vshift != 0:
        # we need to move the rupture center to make the rupture fit
        # inside the seismogenic layer.
        hshift = abs(vshift / math.tan(rdip))
        clon, clat = geodetic.fast_point_at(
            clon, clat, azimuth_up if vshift < 0 else azimuth_down,
            hshift)
        cdep += vshift
    theta = math.degrees(math.atan(half_width / half_length))
    hor_dist = math.sqrt(half_length ** 2 + half_width ** 2)
    top_left = np.array([0., 0., cdep - half_height])
    top_right = np.array([0., 0., cdep - half_height])
    bottom_left = np.array([0., 0., cdep + half_height])
    bottom_right = np.array([0., 0., cdep + half_height])
    top_left[:2] = geodetic.fast_point_at(
        clon, clat, strike + 180 + theta, hor_dist)
    top_right[:2] = geodetic.fast_point_at(
        clon, clat, strike - theta, hor_dist)
    bottom_left[:2] = geodetic.fast_point_at(
        clon, clat, strike + 180 - theta, hor_dist)
    bottom_right[:2] = geodetic.fast_point_at(
        clon, clat, strike + theta, hor_dist)
    return top_left, top_right, bottom_left, bottom_right


def get_rupdims(area: float, dip: float, width: float, rar: float) -> np.ndarray:
    """
    Calculate and return the rupture length and width
    for given magnitude surface parameters.

    Args:
        area: Rupture area (km ^ 2)
        dip: Rupture dip (degrees from horizontal)
        width: Rupture width (km)
        rar: Rupture aspect ratio
    Returns:
        array of shape (1, 3) with rupture lengths, widths and heights

    The rupture area is calculated using method
    :meth:`~openquake.hazardlib.scalerel.base.BaseMSR.get_median_area`
    of source's
    magnitude-scaling relationship. In any case the returned
    dimensions multiplication is equal to that value. Than
    the area is decomposed to length and width with respect
    to source's rupture aspect ratio.
    If calculated rupture width being inclined by nodal plane's
    dip angle would not fit in between upper and lower seismogenic
    depth, the rupture width is shrunken to a maximum possible
    and rupture length is extended to preserve the same area.
    """
    out = np.zeros(3)
    rup_length = np.sqrt(area * rar)
    rup_width = area / rup_length
    rdip = math.radians(dip)
    max_width = width / math.sin(rdip)
    if rup_width > max_width:
        rup_width = max_width
        rup_length = area / rup_width
    out[0] = rup_length
    out[1] = rup_width * math.cos(rdip)
    out[2] = rup_width * math.sin(rdip)
    return out


class ScenarioProperties():
    """Class to manage the set of attributes associated with a given earthquake
    and set of stations, including the ability to infer missing attributes
    from available data

    Attributes:
        site_lon: Longitudes of the sites (decimal degrees)
        site_lat: Latitudes of the sites (decimal degrees)
        site_vs30: Vs30 at the locations of the sites (m/s)
        nsites: Number of sites associated with this event
        ev_lon: Earthquake longitude (decimal degrees)
        ev_lat: Earthquake latitude (decimal degrees)
        ev_depth: Earthquake depth (km)
        m: Magnitude
        lamda: Rake of the earthquake (Aki & Richards convention)
        reference_vs30: Vs30 of a reference rock material (m/s) (default = 800 m/s)
        site_z1pt0: Depth (m) to 1.0 km/s Vs layer
        reference_z1pt0: Depth (m) to 1.0 km/s Vs layer for reference rock site
        region: ESHM20 attenuation region (for Kotha et al., 2020 model) (default 0)
        basin_region: For Vs30 to basin depth conversion, defines the region ("japan" or
                      "california")
        aspect: Aspect ratio (rupture length / width)
        repi: Source to site epicentral distance (km)
        rhypo: Source to site hypocentral distance (km)

    """
    def __init__(self, ev_lon: float, ev_lat: float, ev_depth: float,
                 mag: float, site_lon: np.ndarray, site_lat: np.ndarray,
                 site_vs30: Optional[np.ndarray] = None,
                 site_z1pt0: Optional[np.ndarray] = None,
                 site_z2pt5: Optional[np.ndarray] = None,
                 strike: Optional[float] = None, dip: Optional[float] = None,
                 rake: Optional[float] = None, reference_vs30: float = 800.0,
                 region: Union[int, np.ndarray] = 0,
                 vs30measured: Union[bool, np.ndarray] = True,
                 reference_z1pt0: Optional[float] = None,
                 reference_z2pt5: Optional[float] = None,
                 basin_region: str = "japan", aspect: float = 1.25,
                 upper_seismo_depth: float = 0.0,
                 lower_seismo_depth: float = 1000.0,
                 rupture: Optional[BaseSurface] = None):
        """
        """
        self.site_lon = site_lon
        self.site_lat = site_lat
        self.nsites = len(self.site_lon)
        self._vs30 = site_vs30
        self.reference_vs30 = reference_vs30
        self.ev_lon = ev_lon * np.ones(self.nsites)
        self.ev_lat = ev_lat * np.ones(self.nsites)
        self.hypo_depth = ev_depth * np.ones(self.nsites)
        self.m = mag
        self._rake = rake
        self._dip = dip
        self._strike = strike
        self._z1pt0 = site_z1pt0
        self._z2pt5 = site_z2pt5
        self.reference_z1pt0 = reference_z1pt0
        self.reference_z2pt5 = reference_z2pt5
        self.basin_region = basin_region
        self.aspect = aspect
        self.region = region + np.zeros(self.nsites, dtype=int)
        if isinstance(vs30measured, np.ndarray):
            self.vs30measured = vs30measured
        else:
            if vs30measured:
                self.vs30measured = np.ones(self.nsites, dtype=bool)
            else:
                self.vs30measured = np.zeros(self.nsites, dtype=bool)
        self.repi = geodetic.geodetic_distance(self.ev_lon, self.ev_lat,
                                               self.site_lon, self.site_lat)
        self.rhypo = np.sqrt(self.repi ** 2.0 + self.hypo_depth ** 2.0)
        self.usd = upper_seismo_depth
        self.lsd = lower_seismo_depth
        self._rupture = None

    @property
    def rupture(self):
        """
        Returns the rupture if provided, builds otherwise or returns None
        """
        if self._rupture:
            return self._rupture

        # No rupture defined, so try to build one from the available information
        if (self._strike is not None) and (self._dip is not None) and (self._rake is not None):
            # A rupture nodal plane is available, so can build a rupture from this

            rup_dims = get_rupdims(self.area[0], self.dip[0], self.width[0], self.aspect)
            t_l, t_r, b_l, b_r = build_rupture(
                self.usd, self.lsd, self.m, rup_dims, self._strike, self._dip, self._rake,
                self.ev_lon[0], self.ev_lat[0], self.hypo_depth[0]
                )
            self._rupture = PlanarSurface.from_corner_points(
                Point(t_l[0], t_l[1], t_l[2]),
                Point(t_r[0], t_r[1], t_r[2]),
                Point(b_r[0], b_r[1], b_r[2]),
                Point(b_l[0], b_l[1], b_l[2])
                )
            return self._rupture
        return None

    @property
    def rake(self):
        # Returns the rake of the rupture
        if self._rake is not None:
            return self._rake * np.ones(self.nsites)
        else:
            # Defaults to strike-slip rake (0.0 degrees)
            return np.zeros(self.nsites)

    @property
    def area(self):
        # Returns the area using Wells & Coppersmith (1994)
        return WC1994().get_median_area(self.m, self.rake[0]) *\
            np.ones(self.nsites)

    @property
    def mag(self):
        # Returns the magnitude as a numpy array
        return self.m * np.ones(self.nsites)

    @property
    def length(self):
        """
        Rupture length (calculated from area and aspect ratio)

        A = L * W
        R = L / W
        A(L) = L * (L / R) = (L ** 2) / R
        A(W) = (W * R) * W = (W ** 2) * R

        L = sqrt(A * R)
        W = sqrt(A / R)
        """
        return np.sqrt(self.area * self.aspect)

    @property
    def width(self):
        # Rupture width (calculated from area and aspect ratio
        return np.sqrt(self.area / self.aspect)

    @property
    def dip(self):
        """Returns the earthquake dip (if specified) or a characteristic
        dip value dependent on the rake

        45.0 < rake <= 135.0 = (Reverse) dip = 45.0 degrees

        -45 <= rake <= -135.0 = (Normal) dip = 60.0 degrees

        abs(rake) < 45.0 = (Strike-slip) dip = 90.0 degrees

        abs(rake) > 135.0 = (Strike-slip) dip = 90.0 degrees
        """
        if self._dip:
            return self._dip * np.ones(self.nsites)
        # Returns a characteristic dip value depending on rake
        dip = np.ones(self.nsites)
        if self._rake is None:
            # Assume vertical rupture
            return 90.0 * np.ones(self.nsites)
        elif (self._rake >= 45.0) & (self._rake <= 135.0):
            # Reverse fault
            return 45.0 * dip
        elif (self._rake >= -135.0) & (self._rake <= -45.0):
            return 60.0 * dip
        else:
            return 90.0 * dip

    @property
    def rjb(self):
        # Returns the Joyner-Boore distance
        if self._rupture is not None:
            return self.rupture.get_joyner_boore_distance(
                Mesh(self.site_lon, self.site_lat)
                )
        return self.repi

    @property
    def rrup(self):
        # Returns the Rupture distance
        if self._rupture is not None:
            return self.rupture.get_min_distance(
                Mesh(self.site_lon, self.site_lat)
                )
        return self.rhypo

    @property
    def rx(self):
        # Returns the Rx distance
        if self._rupture is not None:
            return self.rupture.get_rx_distance(
                Mesh(self.site_lon, self.site_lat)
                )
        return self.repi

    @property
    def ry0(self):
        # Returns the Ry0 distance
        if self._rupture is not None:
            return self.rupture.get_ry0_distance(
                Mesh(self.site_lon, self.site_lat)
                )
        return self.repi

    @property
    def vs30(self):
        # Returns the vs30
        if self._vs30 is not None:
            return self._vs30
        else:
            return self.reference_vs30 * np.ones(self.nsites)

    @property
    def z1pt0(self):
        """Returns the Z1.0 according to different configurations

        1. If site Z 1.0 values were provided then use these but fill in nans
        with values taken from Vs30 conversion (Chiou & Youngs, 2014)
        2. Otherwise if a reference Z1.0 is given then take that
        3. Otherwise retreive a reference Z1.0 from the reference Vs30
        """
        z10 = np.zeros(self.nsites)
        if self._z1pt0 is not None:
            # Have defined z1pt0 - take all the nan values as they are
            z1_nan = np.isnan(self._z1pt0)
            idx = np.logical_not(z1_nan)
            z10[idx] = self._z1pt0[idx]
            # For nan values take a conversion from vs30
            z10[z1_nan] = vs30_to_z1pt0_cy14(self.vs30[z1_nan], self.basin_region)
        elif self.reference_z1pt0:
            # Use the reference z1.0
            z10 += self.reference_z1pt0
        else:
            # Take reference Vs30 and get z1pt0 from that
            z10 += vs30_to_z1pt0_cy14(self.reference_vs30, self.basin_region)
        return z10

    @property
    def z2pt5(self):
        """Returns the Z2.5 according to different configurations

        1. If site Z 2.5 values were provided then use these but fill in nans
        with values taken from Vs30 conversion (Campbell & Bozorgnia, 2014)
        2. Otherwise if a reference Z2.5 is given then take that
        3. Otherwise retreive a reference Z2.5 from the reference Vs30
        """
        z25 = np.zeros(self.nsites)
        if self._z2pt5 is not None:
            # Have defined z1pt0 - take all the nan values as they are
            z25_nan = np.isnan(self._z2pt5)
            idx = np.logical_not(z25_nan)
            z25[idx] = self._z2pt5[idx]
            # For nan values take a conversion from vs30
            z25[z25_nan] = vs30_to_z2pt5_cb14(self.vs30[z25_nan], self.basin_region)
        elif self.reference_z2pt5:
            # Use the reference z1.0
            z25 += self.reference_z2pt5
        else:
            # Take reference Vs30 and get z1pt0 from that
            z25 += vs30_to_z2pt5_cb14(self.reference_vs30, self.basin_region)
        return z25

    @property
    def ztor(self):
        if self._rupture:
            return self.rupture.get_top_edge_depth() * np.ones(self.nsites)
        else:
            return np.clip(self.hypo_depth - (self.width / 2.0), 0.0, np.inf)

    @property
    def zbor(self):
        if self._rupture:
            if isinstance(self.rupture, PlanarSurface):
                return self.corner_depths[-1] * np.ones(self.nsites)
            else:
                raise NotImplementedError("zbor not defined for surface type %s"
                                          % self.rupture.__class__.__name__)
        return np.clip(self.hypo_depth + (self.width / 2.0), 0.0, np.inf)

    def build_context(self, gmms: List):
        """
        Returns a complete context object
        """
        ctx = RuptureContext()
        for gmm in gmms:
            for key in gmm.REQUIRES_RUPTURE_PARAMETERS:
                if not hasattr(ctx, key):
                    setattr(ctx, key, getattr(self, key))
            for key in gmm.REQUIRES_SITES_PARAMETERS:
                if not hasattr(ctx, key):
                    setattr(ctx, key, getattr(self, key))
            for key in gmm.REQUIRES_DISTANCES:
                if not hasattr(ctx, key):
                    setattr(ctx, key, getattr(self, key))
        return ctx
