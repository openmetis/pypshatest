"""
Tools for handling the occurrence and exceedance of ground motions at a set
of stations including data imputation by mixed effects regression
"""
import logging
from typing import Optional, List, Dict, Union, Tuple
import numpy as np
from scipy.interpolate import interp1d
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from shapely.geometry import Polygon
from openquake.hazardlib.stats import mean_curve, quantile_curve
from openquake.hazardlib.geo import geodetic
from openquake.hazardlib.imt import PGA, SA
from openquake.hazardlib.gsim.base import CoeffsTable
import pshatest.scenario_validator as sv
from pshatest.station_database import StationDatabase
from pshatest.utils import best_subplot_dimensions


dateaxis_formatter = {
    (0, 1): (mdates.MonthLocator(interval=1), mdates.DateFormatter("%d/%m/%Y")),
    (1, 3): (mdates.MonthLocator(interval=3), mdates.DateFormatter("%m/%Y")),
    (3, 10): (mdates.MonthLocator(interval=6), mdates.DateFormatter("%m/%Y")),
    (10, 20): (mdates.YearLocator(), mdates.DateFormatter("%Y")),
    (20, 10000): (mdates.YearLocator(base=5), mdates.DateFormatter("%Y")),
}


class StationSetResults():
    """
    General class for handling a result set of imputed station curves

    Attributes:
        stations: Station data (as pandas Dataframe)

        periods: Spectral periods used for calculation of the hazard

        accelerations: "Observed" accelerations at the stations (including direct and
                       imputed values)

        sigmas: Uncertainty of the accelerations at the stations

        events: Catalogue of events considered in the imputation

        neq: Number of events considered in the station set

        nsta: Number of stations

        nper: Number of spectral periods

        start_date: Start date (as date string: YYYY-mm-DD) of observation period

        end_date: End date (as date string: YYYY-mm-DD) of observation period
    """

    def __init__(
            self,
            stations: pd.DataFrame,
            periods: np.ndarray,
            accelerations: np.ndarray,
            sigmas: np.ndarray,
            start_date: str,
            end_date: str,
            events: pd.DataFrame,
            relevant_events: Optional[np.ndarray] = None
            ):
        """
        Instantiate class with outputs of the data imputation process

        Args:
            relevant_events: Boolean vector indicating whether each event in the events
                             is considered relevant for the application or not
        """
        self.stations = stations
        self.periods = periods
        self.neq = events.shape[0]
        self.nsta = self.stations.shape[0]
        self.nper = len(self.periods)
        assert (self.neq, self.nsta, self.nper) == accelerations.shape,\
            "Expected acceleration size %s does not match acceleration dimension %s"\
            % (str((self.neq, self.nsta, self.nper)), str(accelerations.shape))
        if relevant_events is None:
            relevant_events = np.ones(self.neq, dtype=bool)
        self.events = events.iloc[relevant_events, :]
        self.accelerations = accelerations[relevant_events, :, :]
        self.sigmas = sigmas[relevant_events, :, :]
        self.neq = self.events.shape[0]
        self.start_date = start_date
        self.end_date = end_date

    def __repr__(self):
        # Returns basic information about data set
        return "Ground Motion Station Set for period %s to %s"\
            "(%g sites, %g events, %g periods)"\
            % (self.start_date, self.end_date, self.nsta, self.neq, self.nper)

    def plot_station_pseudo_history(
            self, station_name: str,
            period: float,
            ylim: Tuple = (),
            start_date: str = "2000-01-01",
            end_date: str = "2020-12-31",
            figsize: Tuple = (12, 7),
            filename: str = "",
            figtype: str = "png",
            dpi: int = 300
            ):
        """Creates a plot of the observation pseudo-history for each station

        Args:
            station_name: Name of station
            period: Spectral period
            ylim: Limits of the y-axis
            start_date: Start date as limit of the x-axis (in format YYYY-mm-DD)
            end_date: End date as limit of the x-axis (in format YYYY-mm-DD)
            figsize: Figure size (as matplotlib)
            filename: Name of file for exporting the figure
            figtype: File type for figure export
            dpi: Figure resolution (dots per inch)

        """
        loc = self.stations.index.get_loc(station_name)
        station_data = self.stations.iloc[loc]
        if period < np.min(self.periods) or period > np.max(self.periods):
            raise ValueError("Period %.5f outside the data range (%.5f to %.5f)"
                             % (period, np.min(self.periods), np.max(self.periods)))
        # Get the accelerations for the stations
        accels, sigmas, idx = self._get_accelerations_sigmas(loc, np.array([period]))
        asymmetric_error = [accels[:, 0] - np.exp(np.log(accels[:, 0]) - sigmas[:, 0]),
                            np.exp(np.log(accels[:, 0]) + sigmas[:, 0]) - accels[:, 0]]
        # Get datetimes of events
        dtimes = self.events["ev_time"].iloc[idx].astype(np.datetime64)
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)
        ax.errorbar(dtimes, accels[:, 0], yerr=asymmetric_error,
                    fmt="o", capsize=6, linewidth=1)

        has_obs = np.isclose(sigmas[:, 0], 0.0)
        ax.plot(dtimes[has_obs], accels[has_obs, 0], "ro", label="Observation", zorder=3)
        ax.grid(which="both")
        ax.set_yscale("log")
        ax.set_ylabel("Sa ({:.3f}) (g)".format(period), fontsize=16)
        ax.set_xlabel("Date", fontsize=16)
        if len(ylim):
            ax.set_ylim(*ylim)

        start_date = np.datetime64(start_date) if start_date\
            else np.datetime64(self.start_date)
        end_date = np.datetime64(end_date) if end_date else np.datetime64(self.end_date)
        tdiff = end_date - start_date

        for (dlow, dhigh), (dint, dfmt) in dateaxis_formatter.items():
            if (tdiff >= (366 * dlow)) & (tdiff <= 366 * dhigh):
                break
        ax.set_xlim(np.datetime64(start_date), np.datetime64(end_date))

        ax.xaxis.set_major_locator(dint)
        ax.xaxis.set_major_formatter(dfmt)
        plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
        ax.set_title("%s (%.5fE, %.5fN)" % (station_data.name, station_data.lons,
                                            station_data.lats), fontsize=18)
        ax.tick_params(labelsize=12)
        if filename:
            plt.savefig(filename, format=figtype, dpi=dpi, bbox_inches="tight")
        return

    def get_hazard_curves_site(
            self,
            site_name: str,
            periods: np.ndarray,
            target_accel: Optional[np.ndarray] = None,
            nsamples: int = 0
            ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Retreive the full set of observed hazard curves for a site (in terms of rates of
        exceedance)

        Args:
            site_name: Name (ID string) of the site
            periods: Spectral periods for the hazard curves
            target_accel: Target accelerations for computing the rates of exceedance. If not
                          specified then a default set of 200 values logarithmically spaced
                          between 1.0E-5 and 10 will be used.
            nsamples: Number of samples to use if sampling the uncertainty

        Returns:
            target_accel: The target accelerations for calculating the rates of exceedance
            obs_rates: Observed rates of exceedance (without imputation) normalised by
                       station data availability
            sample_rates: Rates of exceedance sampling the uncertainty in the imputed data,
                          normalised to annual rates

        """
        if target_accel is None:
            target_accel = np.logspace(-5, 1, 200)
        loc = self.stations.index.get_loc(site_name)
        station_data = self.stations.iloc[loc]
        if np.any(periods < np.min(self.periods)) or np.any(periods > np.max(self.periods)):
            raise ValueError("Requested periods outside the data range (%.5f to %.5f)"
                             % (np.min(self.periods), np.max(self.periods)))
        accels, sigmas, idx = self._get_accelerations_sigmas(loc, periods)
        # Need to extract the actual observations first and normalise by implied
        # data availability
        ndays = station_data.ndays
        if np.isnan(ndays):
            start_date = max(np.datetime64(station_data.start_eida),
                             np.datetime64(self.start_date))
            end_date = min(np.datetime64(station_data.end_eida),
                           np.datetime64(self.end_date))
            ndays = (end_date - start_date).astype("timedelta64[D]").astype(float)
        time_fact = 365.25 / ndays
        ndays_all = (
            np.datetime64(self.end_date) -
            np.datetime64(self.start_date)).astype("timedelta64[D]").astype(float) + 1.0
        obs_accels = accels[np.isclose(sigmas[:, 0], 0.0), :]
        nobs, nper = obs_accels.shape
        obs_rates = np.zeros([len(target_accel), nper])
        for i, a in enumerate(target_accel):
            obs_rates[i, :] += np.sum(obs_accels >= a, axis=0)
        obs_rates *= time_fact

        if nsamples:
            sample_accels = np.zeros([accels.shape[0], nper, nsamples])
            for j in range(nsamples):
                sample_accels[:, :, j] = np.exp(
                    np.log(accels) +
                    np.random.normal(size=[accels.shape[0], nper]) * sigmas)
        else:
            sample_accels = accels.copy().reshape([accels.shape[0], nper, 1])
        # Get sample curves
        sample_rates = np.zeros([len(target_accel), nper, nsamples])
        for i, a in enumerate(target_accel):
            sample_rates[i, :, :] += np.sum(sample_accels >= a, axis=0)
        sample_rates *= (365.25 / ndays_all)
        return target_accel, obs_rates, sample_rates

    def _get_accelerations_sigmas(
            self,
            locs: np.ndarray,
            periods: np.ndarray
            ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Get the acceelerations and sigmas for a set of locations and periods,
        interpolating where necessary

        Args:
            locs: Index vector indicating positions of stations in the arrays
            periods: Periods for calculating the accelerations
        """
        # Get the accelerations for the stations
        accels = self.accelerations[:, locs, :]
        sigmas = self.sigmas[:, locs, :]
        # Interpolate to desired periods
        ipl = interp1d(np.log10(self.periods), accels)
        accels = ipl(np.log10(periods))
        ipl = interp1d(np.log10(self.periods), sigmas)
        sigmas = ipl(np.log10(periods))
        assert accels.shape[0] == self.neq
        # Plot only the accelerations greater than 0
        idx = np.max(accels, axis=1) > 0.0
        accels = accels[idx, :]
        sigmas = sigmas[idx, :]
        return accels, sigmas, idx

    def plot_station_sampled_curves(
            self,
            site_name: str,
            periods: np.ndarray,
            target_accel: Optional[np.ndarray] = None,
            nsamples: int = 0,
            nrow: Optional[int] = None,
            ncol: Optional[int] = None,
            xlim: Tuple = (),
            ylim: Tuple = (),
            figsize: Tuple = (12, 12),
            quantiles: Optional[List] = None,
            filename: str = "",
            figtype: str = "png",
            dpi: int = 300):
        """Creates a plot of the seismic hazard curves at a site with uncertainty included
        by sampling

        Args:
            site_name: Name of the station (station ID)
            periods: Spectral periods for creating the plots
            target_accel: Target accelerations (intensity measure levels) for determining the
                          observed exceedance
            nsamples: Number of samples for sampling the uncertainty
            nrow: Number of rows in the figure
            ncol: Number of columns in the figure
            xlim: Limits of the x-axis (applies to all sub-plots)
            ylim: Limits of the y-axis (applies to all sub-plots)
            figsize: Matplotlib figure size
            quantiles: List of quantiles (between 0 and 1) for showing uncertainty in observed
                       seismic hazard curves
            filename: Name of file for exporting the figure
            figtype: File type for figure export
            dpi: Figure resolution (dots per inch)
        """
        nper = len(periods)
        if (nrow is None) and (ncol is None):
            nrow, ncol = best_subplot_dimensions(nper)
        elif ncol:
            nrow = nper // ncol
            if (nrow * ncol) < nper:
                nrow += 1
        elif nrow:
            ncol = nper // ncol
            if (nrow * ncol) < nper:
                ncol += 1
        imls, obs_curves, sample_curves = self.get_hazard_curves_site(site_name,
                                                                      periods,
                                                                      target_accel,
                                                                      nsamples)
        site_info = self.stations.iloc[self.stations.index.get_loc(site_name)]
        fig = plt.figure(figsize=figsize)
        axs = [fig.add_subplot(nrow, ncol, i + 1) for i in range(nper)]
        if nsamples <= 1:
            weights = 1.0
        else:
            weights = (1.0 / nsamples) * np.ones(nsamples)
        for i, (per, ax) in enumerate(zip(periods, axs)):
            mean_obs_curve = np.exp(mean_curve(np.log(sample_curves[:, i, :]).T, weights))
            for j in range(nsamples):
                ax.loglog(imls, sample_curves[:, i, j], "-",
                          color="cornflowerblue", alpha=0.5)
            if quantiles is not None:
                for qntl in quantiles:
                    qntl_curve = quantile_curve(qntl,
                                                sample_curves[:, i, :].T,
                                                weights)
                    ax.loglog(imls, qntl_curve, "--", color="gold")
                qntl_label = "QNTL:[{:s}]".format(
                    ",".join(["{:.1f}".format(qntl) in quantiles])
                )
                ax.loglog(imls, qntl_curve, "--", color="gold", label=qntl_label)
            ax.loglog(imls, mean_obs_curve, "k-", lw=2, label="Mean Curve (Imp.)")
            ax.loglog(imls, obs_curves[:, i], "r-", lw=2, label="Obs. Curve")
            ax.set_xlabel("Sa (%.4f) (g)" % per, fontsize=16)
            ax.set_ylabel("Annual Rate of Exceed.", fontsize=16)
            ax.grid(which="both")
            if len(xlim):
                ax.set_xlim(*xlim)
            if len(ylim):
                ax.set_ylim(*ylim)
            ax.tick_params(labelsize=12)
        ax.legend(loc="upper right", fontsize=14)
        plt.suptitle("%s (%.4fE, %.4fN)" % (site_name, site_info.lons, site_info.lats),
                     fontsize=20)
        fig.tight_layout(rect=[0, 0, 1, 0.95])
        if filename:
            plt.savefig(filename, format=figtype, dpi=dpi, bbox_inches="tight")
        return


class ObservedHazardDataImputation():
    """General class to handle the imputation of data from the observed database
    and build "virtual" hazard curves for set of sites

    Attributes:

        db: Station Database for a given ground motion dataset as instance of
            pshatest.station_database.StationDatabase

        mrange: Range of magnitudes (mmin, mmax) for consideration of an event

        rmax: Maximum source to site distance for consideration of a record

        max_depth: Maximum depth (km) for consideration of an earthquake

        region: Region for application of the imputation (as bounding box (west, south, east,
                north) or Shapely geometry Polygon

        vs30ref: Reference Vs30 (m/s)

        basin_region: If retreiving Z1.0 or Z2.5 from Vs30 then this defines the basin region
                      to be used for conversion

        z1pt0ref: Reference depth to the Vs 1.0 km/s layer if not measured

        events: Events from catalogue to be used for the imputation

        neq: Number of events used in the imputation

        declustering: Results of the declustering being used (if selected)

        metadata: Metadata of the ground motion database

    """

    # Get Phi0 for Europe from SWUS (central) model
    PHI_0 = CoeffsTable(sa_damping=5, table="""\
       imt                 phi_0
       pgv     0.446525247049620
       pga     0.467151252053241
       0.010   0.467206938011971
       0.025   0.468698397037258
       0.040   0.473730661220076
       0.050   0.479898166019243
       0.070   0.487060899687138
       0.100   0.496152397155402
       0.150   0.497805936702476
       0.200   0.494075956910651
       0.250   0.488950837091220
       0.300   0.482157450259502
       0.350   0.480254056040507
       0.400   0.475193646646757
       0.450   0.469672671050266
       0.500   0.463165027132890
       0.600   0.451206692163190
       0.700   0.446704739768374
       0.750   0.444311331912854
       0.800   0.439300059540554
       0.900   0.433043490235851
       1.000   0.426703815544157
       1.200   0.416539877732589
       1.400   0.409641113489270
       1.600   0.404985162254916
       1.800   0.399057812399511
       2.000   0.396502973620567
       2.500   0.393288023064441
       3.000   0.390859427279163
       3.500   0.388102875218375
       4.000   0.386956009422453
       4.500   0.372827866334900
       5.000   0.377458812369736
       6.000   0.384862538848542
       7.000   0.385850838707000
       8.000   0.387633769846605
       10.00   0.386000000000000
    """)

    def __init__(self, dbname: str,
                 mmin: float = 3.5, mmax: float = 10.0,
                 rmax: float = 350.0, max_depth: float = 50.0,
                 region: Optional[Union[List, Polygon]] = None,
                 vs30ref: float = 800.0, basin_region: str = "japan",
                 z1pt0ref: Optional[float] = None,
                 decluster_method: str = None):
        """
        Args:
            decluster_method: Use declustering results from those found in the database
        """
        self.db = StationDatabase(dbname)
        self.mrange = (mmin, mmax)
        self.rmax = rmax
        self.region = region
        self.basin_region = basin_region
        self.vs30ref = vs30ref
        self.z1pt0ref = z1pt0ref
        logging.info("Reading and selecting catalogue")
        catalogue = pd.read_hdf(dbname, "catalogue")
        catalogue["ev_time"] = catalogue["ev_time"].astype(np.datetime64)
        # Events are only those in the catalogue between the minimum
        # and maximum
        self.events = catalogue[
            (catalogue["M"] >= self.mrange[0]) &
            (catalogue["M"] <= self.mrange[1]) &
            (catalogue["depth"] <= max_depth)
        ]
        self.neq = self.events.shape[0]
        logging.info(
            "Identified %g events with %.1f < M < %.1f (from %g in database)"
            % (self.events.shape[0], self.mrange[0], self.mrange[1], catalogue.shape[0])
        )
        # Apply the declustering if specified
        self.declustering = None
        if decluster_method:
            self._apply_declustering(decluster_method)
            logging.info("After declustering: retain %g mainshocks" % self.neq)
        logging.info("Loading metadata")
        self.metadata = pd.read_hdf(dbname, "metadata")
        self.metadata_event_ids = self.metadata["M_CAT_ID"].unique()
        logging.info("ready")

    def _apply_declustering(self, decluster_method: str):
        """
        If the results of a particular declustering outcome are selected then load in the
        results and retain only the mainshocks
        """
        logging.info("Extracting declustering results for method %s" % decluster_method)
        self.db.open()
        if "declustering" not in list(self.db.db):
            self.db.close()
            raise ValueError("No declustering information in database")
        elif decluster_method not in list(self.db.db["declustering"]):
            self.db.close()
            raise ValueError("Declustering method %s not found in declustering results"
                             % decluster_method)
        else:
            declust = self.db.db['declustering/{:s}'.format(decluster_method)][:]
            declust = pd.DataFrame(declust[["vcl", "flagvector"]],
                                   index=declust["master_id"].astype(str))
            self.declustering = declust.loc[self.events.master_id]
            self.db.close()
        n_fore = (self.declustering["flagvector"] == -1).sum()
        n_after = (self.declustering["flagvector"] == 1).sum()
        n_main = (self.declustering["flagvector"] == 0).sum()
        logging.info(
            "Declustering by %s returns %g mainshocks, %g foreshocks and %g aftershocks"
            % (decluster_method, n_fore, n_after, n_main))
        idx = self.declustering["flagvector"] == 0
        self.events = self.events.iloc[idx.to_numpy()]
        self.neq = self.events.shape[0]
        return

    @staticmethod
    def prepare_geodataframe(
            region: Union[Tuple, Polygon],
            crs: str = "EPSG:4326") -> gpd.GeoDataFrame:
        """Returns a simple geodataframe representing a single region
        from either a bounding box or a shapely Polygon

        Args:
            region: Region for selecting sites (either bounding box or shapely
                    geometry polygon)
            crs: Coordinate reference system for use with shapely polygon
        """
        if isinstance(region, tuple):
            west, south, east, north = region
            # Build a simple polygon from bounding box
            # corners clockwise from northwest
            region = Polygon([(west, north),
                              (east, north),
                              (east, south),
                              (west, south)])
        return gpd.GeoDataFrame(
            {"REGIONID": [1], "geometry": gpd.GeoSeries([region])}, crs=crs
        )

    def _select_sites(self, cartesian_ref: str = "EPSG:3035"):
        """Select sites according to the defined region

        Args:
            cartesian_ref: Coordinate reference system for cartesian point in polygon selection
        """
        self.db.open()
        stations = self.db.stations.copy()
        self.db.close()
        if self.region is None:
            return stations
        if isinstance(self.region, list):
            # Select a list of sites
            return stations.loc[self.region]
        # geospatial selection
        region = self.prepare_geodataframe(self.region)
        region_cart = region.to_crs(cartesian_ref)
        sites_cart = stations.to_crs(cartesian_ref)
        sites_cart = gpd.sjoin(sites_cart, region_cart, how="left")
        return stations[sites_cart["REGIONID"] == 1]

    def get_site_observations(self, sites: List, periods: np.ndarray, channels: List) -> Dict:
        """Retrieves the observed accelerations and associated metadata
        for a list of stations and channels

        Args:
            sites: The selected sites for finding the observations
            periods: The selected periods of the obsevations
            channels: List of channels to consider for observatios

        Returns:
            Observed accelerations per site and channel
        """
        observed_accelerations = {}
        for site in sites:
            obs_accel = self.db.get_observed_accelerations(site,
                                                           periods=periods)[1]
            for channel in obs_accel:
                obs_accel[channel]["metadata"] =\
                    self.metadata.loc[obs_accel[channel]["indexes"]]
                obs_accel[channel]["metadata"] = obs_accel[channel]["metadata"][
                    ["ev_id", "sta", "gmid", "sta_lon", "sta_lat", "M_CAT_ID",
                     "mag", "r_epi", "r_hyp", "r_jb", "r_rup"]
                    ]
            observed_accelerations[site] = obs_accel
        return observed_accelerations

    def get_all_site_specific_information(self, sites, gmm, periods, channels):
        """Retreives all of the site specific information
        """
        observed_accelerations = self.get_site_observations(sites, periods, channels)
        raw_ds2s = self.db.get_ds2s_for_sites(sites, [gmm],
                                              channels=channels,
                                              periods=periods)[0]
        for i, site in enumerate(sites):
            nobs = {}
            ds2s_chan = {}
            for channel in channels:
                if channel not in observed_accelerations[site]:
                    continue
                nobs[channel] = len(observed_accelerations[site][channel]["indexes"])

                ds2s_chan[channel] = raw_ds2s[gmm][channel].loc[site].to_numpy()
            ds2s = None
            ds2s_channel = None
            ds2s_nobs = None
            # See if there exist some well-constrained ds2s
            for chn in ["HN", "HG", "HN", "EH"]:
                if chn in nobs:
                    if nobs[chn] >= 10:
                        # Well constrained ds2s
                        ds2s = ds2s_chan[chn]
                        ds2s_channel = chn
                        ds2s_nobs = nobs[chn]
                        break
            # No well constained ds2s, but if poorly constrained by 3 records
            # on a strong motion channel then use that
            if ds2s is None:
                for chn in ["HN", "HG"]:
                    if (chn in nobs) and nobs[chn] >= 3:
                        # If a less well-constrained strong motion channel ds2s
                        ds2s = ds2s_chan[chn]
                        ds2s_channel = chn
                        ds2s_nobs = nobs[chn]
                        break

            # No well constrained ds2s, but if poorly constrained by >= 5 records
            # on a weak motion channel then use that
            if ds2s is None:
                for chn in ["HH", "EH"]:
                    if (chn in nobs) and nobs[chn] >= 5:
                        # Use a less well-constrained weak motion channel ds2s
                        ds2s = ds2s_chan[chn]
                        ds2s_channel = chn
                        ds2s_nobs = nobs[chn]
                        break
            observed_accelerations[site]["ds2s"] = ds2s
            observed_accelerations[site]["ds2s_channel"] = ds2s_channel
            observed_accelerations[site]["ds2s_nobs"] = ds2s_nobs
        return observed_accelerations

    def _get_dbe_for_event(self, master_id, dbe_strong, dbe_weak, gmm):
        """Returns the between event residual for each event

        """
        if master_id in self.metadata_event_ids:
            # This event has observations associated with it - get them
            # from the metadata
            sel_metadata = self.metadata[self.metadata["M_CAT_ID"] == master_id]
            ev_id = sel_metadata["ev_id"].iloc[0]
            if ev_id in dbe_strong[gmm].index:
                dbe_event_strong = dbe_strong[gmm].loc[ev_id].to_numpy()
            else:
                dbe_event_strong = None
            if ev_id in dbe_weak[gmm].index:
                dbe_event_weak = dbe_weak[gmm].loc[ev_id].to_numpy()
            else:
                dbe_event_weak = None
        else:
            dbe_event_strong = None
            dbe_event_weak = None
        return dbe_event_strong, dbe_event_weak

    def get_accelerations_for_stations(
            self,
            periods: np.ndarray,
            gmm: str,
            gmm_model,
            start_date: str,
            end_date: str,
            cartesian_ref: str = "EPSG:3035",
            channels: List = ["HN", "HG", "HH", "EH"]) -> StationSetResults:
        """Retrieves the accelerations and uncertainties for each station with
        missing observations imputed from the GMM with its between-event
        residual and/or between site residual where available
        """
        imts = [SA(per) for per in periods]
        nimts = len(imts)
        # First get the between event residuals while the database is closed
        dbe_strong, dbe_weak, periods = self.db.get_dbes([gmm], periods)
        # Get the sites
        sites = self._select_sites(cartesian_ref)
        nsites = sites.shape[0]
        logging.info("Found %g sites in region" % nsites)

        # Open the database
        self.db.open()
        # orig_periods = self.db.db["periods"][:]
        # Get phi0 with period
        phi0 = np.array([self.PHI_0[imt]["phi_0"] for imt in imts])
        # Get the observed accelerations for all channels and phi_s2s for the sites
        logging.info("Retreiving observed site data and ds2s")
        site_data = self.get_all_site_specific_information(sites.index,
                                                           gmm, periods,
                                                           channels)
        # Don't need the database anymore, so closing it
        self.db.close()
        acceleration = np.zeros([self.neq, nsites, nimts])
        sigma = np.zeros([self.neq, nsites, nimts])
        relevant_event = np.zeros(self.neq, dtype=bool)
        logging.info("Running events ...")
        start_time = np.datetime64(start_date)
        end_time = np.datetime64(end_date)
        for i in range(self.events.shape[0]):
            row = self.events.iloc[i]
            if row.ev_time < start_time or row.ev_time > end_time:
                # Event outside time window
                continue
            dbe_event_strong, dbe_event_weak = self._get_dbe_for_event(
                row.master_id, dbe_strong, dbe_weak, gmm)
            # Calculate the stations within the distance of the event
            distances = geodetic.geodetic_distance(row.lon, row.lat,
                                                   sites.lons.to_numpy(),
                                                   sites.lats.to_numpy())
            idx = distances <= self.rmax
            if not np.any(idx):
                logging.info(" ... No stations within %.2f km of event %s (%.3fE, %.3fN)"
                             % (self.rmax, row.master_id, row.lon, row.lat))
                # All stations outside of the event distance - so skip event
                continue
            relevant_event[i] = True
            logging.info("... Found %g stations with %.2f km of event %s (%.3fE, %.3fN)"
                         % (np.sum(idx), self.rmax, row.master_id, row.lon, row.lat))
            # Need to calculate the expected ground motions at the station
            sel_sites = sites[idx]
            scenario_gen = sv.ScenarioProperties(row.lon, row.lat, row.depth,
                                                 row.M, sel_sites.lons.to_numpy(),
                                                 sel_sites.lats.to_numpy(),
                                                 reference_vs30=self.vs30ref,
                                                 reference_z1pt0=self.z1pt0ref,
                                                 basin_region=self.basin_region)
            ctx = scenario_gen.build_context([gmm_model])
            mean = np.zeros([len(imts), sel_sites.shape[0]])
            tau = np.zeros_like(mean)
            phi = np.zeros_like(mean)
            sig = np.zeros_like(mean)
            # Execute GMPE
            gmm_model.compute(ctx, imts, mean, sig, tau, phi)
            # Add on the between event term if there and set tau to 0
            if dbe_event_strong is not None:
                logging.info("... dbe (strong) available")
                for j, dbe_s in enumerate(dbe_event_strong):
                    mean[j, :] += dbe_s
                    tau[j, :] = 0.0
            elif dbe_event_weak is not None:
                logging.info("... dbe (weak) available")
                for j, dbe_w in enumerate(dbe_event_weak):
                    mean[j, :] += dbe_w
                    tau[j, :] = 0.0
            else:
                # Assign no dbe and accept full
                # event to event variability
                logging.info("... no dbe available")
            logging.info("... checking for obs. accel and/or ds2s")
            obs_counter = 0
            ds2s_counter = 0
            for j, site in enumerate(sel_sites.index):
                has_obs = False
                # If the station has an observed acceleration here
                # then overwrite the prediction with the observation and
                # set all of the variability terms to 0
                for channel in channels:
                    if channel not in site_data[site]:
                        continue
                    m_cat_ids = site_data[site][channel]["metadata"]["M_CAT_ID"].to_list()
                    if row.master_id in m_cat_ids:
                        obs_loc = m_cat_ids.index(row.master_id)
                        # Replace the mean with the observed ground motion
                        mean[:, j] = np.log(site_data[site][channel]['values'][obs_loc, :])
                        # Set the uncertainties to 0.0
                        tau[:, j] = 0.0
                        phi[:, j] = 0.0
                        has_obs = True
                        obs_counter += 1
                        break
                if not has_obs and (site_data[site]["ds2s"] is not None):
                    # Didn't have an observation, but the site does have a ds2s
                    # Add this to the mean hazard and set the remaining variability
                    # to the European phi0
                    ds2s_counter += 1
                    mean[:, j] += site_data[site]["ds2s"]
                    phi[:, j] = phi0
            logging.info("... from %g sites: %g have obs., %g have ds2s"
                         % (sel_sites.shape[0], obs_counter, ds2s_counter))
            acceleration[i, idx, :] = np.exp(mean.T)
            sigma[i, idx, :] = np.sqrt(tau ** 2. + phi ** 2.).T
        return StationSetResults(sites, periods, acceleration, sigma, start_date, end_date,
                                 self.events.copy(), relevant_event)
