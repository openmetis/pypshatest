"""A set of general utility functions
"""
from math import ceil, sqrt
from typing import Union, Tuple
import numpy as np
import pandas as pd
from scipy.spatial.distance import cdist
from scipy.stats import distributions


#: Earth radius in km.
EARTH_RADIUS = 6371.0


def vs30_to_z1pt0_cy14(
        vs30: Union[float, np.ndarray],
        japan: bool = False
        ) -> Union[float, np.ndarray]:
    """Returns the estimate depth to the 1.0 km/s velocity layer based on Vs30
    from Chiou & Youngs (2014) California model

    Args:
        vs30: Vs30 values (numpy array or float)
        japan: Use Japan formula (True) or California formula (False)

    :returns:
        Z1.0 in m
    """
    if japan:
        c1 = 412. ** 2.
        c2 = 1360.0 ** 2.
        return np.exp((-5.23 / 2.0) * np.log((np.power(vs30, 2.) + c1) / (
            c2 + c1)))
    else:
        c1 = 571 ** 4.
        c2 = 1360.0 ** 4.
        return np.exp((-7.15 / 4.0) * np.log((vs30 ** 4. + c1) / (c2 + c1)))


def vs30_to_z2pt5_cb14(
        vs30: Union[float, np.ndarray],
        japan: bool = False
        ) -> Union[float, np.ndarray]:
    """Converts vs30 to depth to 2.5 km/s interface using model proposed by
    Campbell & Bozorgnia (2014)

    Args:
        vs30: Vs30 values (numpy array or float)
        japan: Use Japan formula (True) or California formula (False)

    :returns:
        Z2.5 in km
    """
    if japan:
        return np.exp(5.359 - 1.102 * np.log(vs30))
    else:
        return np.exp(7.089 - 1.144 * np.log(vs30))


def geodetic_distance(lons1: np.ndarray, lats1: np.ndarray,
                      lons2: np.ndarray, lats2: np.ndarray,
                      diameter: float = 2.0 * EARTH_RADIUS) -> np.ndarray:
    """
    Calculate the geodetic distance between two points or two collections
    of points.
    Parameters are coordinates in decimal degrees. They could be scalar
    float numbers or numpy arrays, in which case they should "broadcast
    together".
    Implements http://williams.best.vwh.net/avform.htm#Dist
    :returns:
        Distance in km, floating point scalar or numpy array of such.
    """
    lons1 = np.radians(lons1)
    lats1 = np.radians(lats1)
    assert lons1.shape == lats1.shape
    lons2 = np.radians(lons2)
    lats2 = np.radians(lats2)
    assert lons2.shape == lats2.shape
    distance = np.arcsin(np.sqrt(
        np.sin((lats1 - lats2) / 2.0) ** 2.0
        + np.cos(lats1) * np.cos(lats2)
        * np.sin((lons1 - lons2) / 2.0) ** 2.0
    ))
    return diameter * distance


def best_subplot_dimensions(nplots: int) -> Tuple[int, int]:
    """
    Returns the optimum arrangement of number of rows and number of
    columns given a total number of plots. Converted from the Matlab function
    "BestArrayDims"
    Args:
        nplots: Number of subplots
    Returns:
        num_row: Number of rows
        num_col: Number of columns
    """
    if nplots == 1:
        return 1, 1
    nplots = float(nplots)
    d_l = ceil(sqrt(nplots))
    wdth = np.arange(1., d_l + 1., 1.)
    hgt = np.ceil(nplots / wdth)
    waste = (wdth * hgt - nplots) / nplots
    savr = (2. * wdth + 2. * hgt) / (wdth * hgt)
    cost = 0.5 * savr + 0.5 * waste
    num_col = np.argmin(cost)
    num_row = int(hgt[num_col])
    return num_row, num_col + 1


def dbe_dframe_to_numpy(dbe: pd.DataFrame) -> np.ndarray:
    """
    Function to transform between event residuals from a dataframe
    to a named array for storage into hdf5

    Args:
        dbe: Between event residuals as dataframe (within event IDs as index

    Returns:
        dbe_array: Between event residuals as a numpy array
    """
    dbe_dtype = [("evid", (np.string_, 40))]
    for key in dbe.columns:
        dbe_dtype.append((key, np.float64))
    dbe_array = np.zeros(dbe.shape[0], dbe_dtype)
    dbe_array["evid"] = dbe.index.to_numpy()
    for key in dbe.columns:
        dbe_array[key] = dbe[key].to_numpy()
    return dbe_array


def dbe_numpy_to_dframe(dbe_array: np.ndarray) -> pd.DataFrame:
    """
    Reverse operation to convert a numpy array with named datatypes into
    a pandas dataframe

    Args:
        dbe_array: Numpy array with named datatypes

    Returns:
        dbe: Data in a data frame
    """
    dbe = pd.DataFrame(dbe_array, index=dbe_array["evid"].astype(str))
    del dbe["evid"]
    return dbe


def ks_weighted(
        data1: np.ndarray,
        data2: np.ndarray,
        wei1: np.ndarray,
        wei2: np.ndarray,
        two_sided: bool = True,
        ) -> Tuple[float, float]:
    """Calculates a weighted Kolmogorov-Smirnoff distance for two empirical distributions
    of data and their corresponding weights

    Args:
        data1: Vector of empirical observations for distribution 1
        data2: Vector of empirical observations for distribution 2
        wei1: Vector of weights for distribution 1
        wei2: Vector of weights for distribution 2
        two_sided: Calculates the KS distribution (True) or else use the Hodge
                   approximation for the 2-sided KS test

    Returns:
        d: Kolmogorov Smirnov distance
        prob: Probability for 2-sided KS-Test
    """
    ix1 = np.argsort(data1)
    ix2 = np.argsort(data2)
    data1 = data1[ix1]
    data2 = data2[ix2]
    wei1 = wei1[ix1]
    wei2 = wei2[ix2]
    data = np.concatenate([data1, data2])
    cwei1 = np.hstack([0, np.cumsum(wei1)/sum(wei1)])
    cwei2 = np.hstack([0, np.cumsum(wei2)/sum(wei2)])
    cdf1we = cwei1[np.searchsorted(data1, data, side='right')]
    cdf2we = cwei2[np.searchsorted(data2, data, side='right')]
    d = np.max(np.abs(cdf1we - cdf2we))
    # calculate p-value
    n1 = data1.shape[0]
    n2 = data2.shape[0]
    m, n = sorted([float(n1), float(n2)], reverse=True)
    en = m * n / (m + n)
    if two_sided:
        prob = distributions.kstwo.sf(d, np.round(en))
    else:
        z = np.sqrt(en) * d
        # Use Hodges' suggested approximation Eqn 5.3
        # Requires m to be the larger of (n1, n2)
        expt = -2 * z**2 - 2 * z * (m + 2*n)/np.sqrt(m*n*(m+n))/3.0
        prob = np.exp(expt)
    return d, prob


def overlap_index(values_a: np.ndarray, values_b: np.ndarray, weights_a: np.ndarray,
                  weights_b: np.ndarray, bins: np.ndarray) -> np.ndarray:
    """
    Calculates the overlap index between distributions of seismic hazard values
    Args:
        values_a: Values of seismic hazard for model A (e.g. PoE or IML)
        values_b: Values of seismic hazard for model B (e.g. PoE or IML)
        weights_a: Weights corresponding to seismic hazard values of model A
        weights_b: Weights corresponding to seismic hazard values of model B
        bins: Bins for constructing the histgrams

    Returns:
        Overlap index
    """
    density = np.zeros([len(bins) - 1, 2])
    density[:, 0] = np.histogram(values_a, bins=bins, density=True, weights=weights_a)[0]
    density[:, 1] = np.histogram(values_b, bins=bins, density=True, weights=weights_b)[0]
    overlap = np.min(density, axis=1)
    total = np.max(density, axis=1)
    return np.sum(overlap) / np.sum(total)


def sammon(x: np.ndarray, n: int = 2, display: int = 2, inputdist: str = 'raw',
           maxhalves: int = 20, maxiter: int = 500, tolfun: float = 1e-9,
           init: str = 'pca') -> Tuple[np.ndarray, float]:

    """Perform Sammon mapping on dataset x

    y = sammon(x) applies the Sammon nonlinear mapping procedure on
    multivariate data x, where each row represents a pattern and each column
    represents a feature.  On completion, y contains the corresponding
    co-ordinates of each point on the map.  By default, a two-dimensional
    map is created.  Note if x contains any duplicated rows, SAMMON will
    fail (ungracefully).

    [y, E] = sammon(x) also returns the value of the cost function in E (i.e.
    the stress of the mapping).

    An N-dimensional output map is generated by y = sammon(x,n) .

    A set of optimisation options can be specified using optional
    arguments, y = sammon(x,n,[OPTS]):

       maxiter        - maximum number of iterations
       tolfun         - relative tolerance on objective function
       maxhalves      - maximum number of step halvings
       input          - {'raw','distance'} if set to 'distance', X is
                        interpreted as a matrix of pairwise distances.
       display        - 0 to 2. 0 least verbose, 2 max verbose.
       init           - {'pca', 'random'}

    The default options are retrieved by calling sammon(x) with no
    parameters.

    File        : sammon.py
    Date        : 18 April 2014
    Authors     : Tom J. Pollard (tom.pollard.11@ucl.ac.uk)
                : Ported from MATLAB implementation by
                  Gavin C. Cawley and Nicola L. C. Talbot

    Description : Simple python implementation of Sammon's non-linear
                  mapping algorithm [1].

    References  : [1] Sammon, John W. Jr., "A Nonlinear Mapping for Data
                  Structure Analysis", IEEE Transactions on Computers,
                  vol. C-18, no. 5, pp 401-409, May 1969.

    Copyright   : (c) Dr Gavin C. Cawley, November 2007.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

    """
    X = x

    # Create distance matrix unless given by parameters
    if inputdist == 'distance':
        xD = X
    else:
        xD = cdist(X, X)

    # Remaining initialisation
    N = X.shape[0]  # hmmm, shape[1]?
    scale = 0.5 / xD.sum()

    if init == 'pca':
        [UU, DD, _] = np.linalg.svd(X)
        Y = UU[:, :n] * DD[:n]
    else:
        Y = np.random.normal(0.0, 1.0, [N, n])
    one = np.ones([N, n])

    xD = xD + np.eye(N)
    xDinv = 1 / xD  # Returns inf where D = 0.
    xDinv[np.isinf(xDinv)] = 0  # Fix by replacing inf with 0 (default Matlab behaviour).
    yD = cdist(Y, Y) + np.eye(N)
    yDinv = 1. / yD  # Returns inf where d = 0.

    np.fill_diagonal(xD, 1)
    np.fill_diagonal(yD, 1)
    np.fill_diagonal(xDinv, 0)
    np.fill_diagonal(yDinv, 0)

    xDinv[np.isnan(xDinv)] = 0
    yDinv[np.isnan(xDinv)] = 0
    xDinv[np.isinf(xDinv)] = 0
    yDinv[np.isinf(yDinv)] = 0  # Fix by replacing inf with 0 (default Matlab behaviour).

    delta = xD - yD
    E = ((delta ** 2) * xDinv).sum()

    # Get on with it
    for i in range(maxiter):
        # Compute gradient, Hessian and search direction (note it is actually
        # 1/4 of the gradient and Hessian, but the step size is just the ratio
        # of the gradient and the diagonal of the Hessian so it doesn't
        # matter).
        delta = yDinv - xDinv
        deltaone = np.dot(delta, one)
        g = np.dot(delta, Y) - (Y * deltaone)
        dinv3 = yDinv ** 3
        y2 = Y ** 2
        H = np.dot(dinv3, y2) - deltaone - np.dot(2, Y) * np.dot(dinv3, Y) +\
            y2 * np.dot(dinv3, one)
        s = -g.flatten(order='F') / np.abs(H.flatten(order='F'))
        y_old = Y

        # Use step-halving procedure to ensure progress is made
        for j in range(maxhalves):
            s_reshape = s.reshape(2, len(s) // 2).T
            y = y_old + s_reshape
            d = cdist(y, y) + np.eye(N)
            dinv = 1 / d  # Returns inf where D = 0.
            # Fix by replacing inf with 0 (default Matlab behaviour).
            dinv[np.isinf(dinv)] = 0
            delta = xD - d
            E_new = ((delta ** 2) * xDinv).sum()
            if E_new < E:
                break
            else:
                s = np.dot(0.5, s)

        # Bomb out if too many halving steps are required
        if j == maxhalves:
            print('Warning: maxhalves exceeded. Sammon mapping may not converge...')

        # Evaluate termination criterion
        if np.abs((E - E_new) / E) < tolfun:
            if display:
                print('TolFun exceeded: Optimisation terminated')
            break

        # Report progress
        E = E_new
        if display > 1:
            print('epoch = ' + str(i) + ': E = ' + str(E * scale))

    # Fiddle stress to match the original Sammon paper
    E = E * scale
    return y, E
