
from setuptools import setup, find_packages

test_requirements = ["pylint", "pytest"]

setup(
    name="PyPSHATest",
    version="0.1.0dev",
    description="Python Toolkit for Testing PSHA Models and Results",
    license="GPLv3",
    install_requires=[
        "openquake.engine>=3.13.0",
        "tables",
        "Rtree",
        "geopandas",
        "rasterio",
        "rpy2",
    ],
    extras_require = {
        "grid": [
            "pycsep",
            ],
        "test": [
            "pylint",
            "flake8",
            "pytest",
            "pytest-cov",
            ],
        "dev": [
            "pylint",
            "flake8",
            "pytest",
            "pytest-cov",
            "sphinx",
            ],
        "all": [
            "pycsep",
            "pylint",
            "flake8",
            "pytest",
            "pytest-cov",
            "sphinx",
        ]
    },
    packages=find_packages(),
    python_requires=">=3.8",
)

