.. pypshatest documentation master file, created by
   sphinx-quickstart on Wed Nov  2 14:43:26 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyPSHATest's documentation!
======================================

**PyPSHATest** is a Python library for exploring and testing components and outputs
of probabilistic seismic hazard models. It is largely built around the OpenQuake
PSHA calculation engine and can be used with its input models and hazard outputs
(though PSHA outputs from other software can also be adopted). 


Disclaimer
----------

The software is exploratory and developed for the purposes of academic research.
It is distributed WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. The developers and their respective organisations assume no
liability for use of the software.

.. note::

   This project is under active development

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   seismicity_rate
   psha_testing_tools
   flatfile_handler
   scenario_validator
   gmm_residual_analysis
   catalogue_tools
   station_database
   openquake_hazard_manager
   observed_hazard


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
